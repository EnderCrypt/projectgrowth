package endercrypt.projectgrowth.setting.environment.food;


import lombok.Getter;


@Getter
public class EnvironmentFood
{
	private int frequency;
	
	private MinMax energy;
	
	private MinMax body;
	
	@Getter
	public static class MinMax
	{
		private int min;
		
		private int max;
	}
}
