package endercrypt.projectgrowth.setting.environment;


import endercrypt.projectgrowth.setting.environment.arena.EnvironmentArena;
import endercrypt.projectgrowth.setting.environment.food.EnvironmentFood;

import lombok.Getter;


@Getter
public class EnvironmentConfiguration
{
	private EnvironmentArena arena;
	
	private EnvironmentFood food;
}
