package endercrypt.projectgrowth.setting.environment.arena;


import lombok.Getter;


@Getter
public class EnvironmentArena
{
	private int size;
	private String type;
}
