package endercrypt.projectgrowth.setting.autosave;


import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.Getter;


@Getter
public class AutosaveConfiguration
{
	private String filename;
	
	public Path getFilename()
	{
		return Paths.get(filename);
	}
	
	private int frequency;
}
