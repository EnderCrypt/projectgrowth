package endercrypt.projectgrowth.setting;


import endercrypt.library.commons.data.DataSource;
import endercrypt.library.framework.systems.configuration.Configuration;
import endercrypt.library.framework.systems.configuration.StandardConfigurations;
import endercrypt.projectgrowth.setting.autosave.AutosaveConfiguration;
import endercrypt.projectgrowth.setting.environment.EnvironmentConfiguration;
import endercrypt.projectgrowth.setting.mutation.MutationConfiguration;
import endercrypt.projectgrowth.setting.performance.PerformanceConfiguration;


public interface Configurations extends StandardConfigurations
{
	@Configuration(location = DataSource.DISK, path = "settings/autosave.yaml")
	public AutosaveConfiguration getAutosave();
	
	@Configuration(location = DataSource.DISK, path = "settings/performance.yaml")
	public PerformanceConfiguration getPerformance();
	
	@Configuration(location = DataSource.DISK, path = "settings/environment.yaml")
	public EnvironmentConfiguration getEnvironment();
	
	@Configuration(location = DataSource.DISK, path = "settings/mutation.yaml")
	public MutationConfiguration getMutation();
}
