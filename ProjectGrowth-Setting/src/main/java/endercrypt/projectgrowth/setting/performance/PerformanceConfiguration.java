package endercrypt.projectgrowth.setting.performance;


import lombok.Getter;


@Getter
public class PerformanceConfiguration
{
	private PerformanceThreads threads;
}
