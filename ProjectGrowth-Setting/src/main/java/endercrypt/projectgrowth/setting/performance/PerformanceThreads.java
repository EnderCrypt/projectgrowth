package endercrypt.projectgrowth.setting.performance;


import lombok.Getter;


@Getter
public class PerformanceThreads
{
	private int simulation;
}
