package endercrypt.projectgrowth.setting.mutation;


import lombok.Getter;


@Getter
public class MutationConfiguration
{
	private boolean enabled;
	
	private MutationGene gene;
	
	private MutationDna dna;
}
