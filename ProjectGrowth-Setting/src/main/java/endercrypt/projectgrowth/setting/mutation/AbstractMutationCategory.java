package endercrypt.projectgrowth.setting.mutation;


import lombok.Getter;


@Getter
public abstract class AbstractMutationCategory
{
	private double duplicate;
	
	private double swap;
	
	private double delete;
}
