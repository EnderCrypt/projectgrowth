package endercrypt.projectgrowth.main.utility;


import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DurationFormatUtils;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import lombok.NonNull;


public class Utility
{
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static String dateString()
	{
		return dateFormatter.format(new Date());
	}
	
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public static String dateString(ZonedDateTime time)
	{
		return dateTimeFormatter.format(time);
	}
	
	public static String smartSpaceText(String text)
	{
		StringBuilder sb = new StringBuilder();
		for (char c : text.toCharArray())
		{
			if (Character.isUpperCase(c) && sb.length() > 0 && sb.charAt(sb.length() - 1) != ' ')
			{
				sb.append(' ');
			}
			sb.append(c);
		}
		return sb.toString();
	}
	
	public static TextColor toTextColor(@NonNull Color color)
	{
		return new TextColor.RGB(color.getRed(), color.getGreen(), color.getBlue());
	}
	
	private static final double WIDTH_CUT_OFF = 0.75;
	
	public static List<String> lineDivider(String text, int width)
	{
		List<String> result = new ArrayList<>();
		
		while (text.length() > width)
		{
			int spaceIndex = text.lastIndexOf(' ', width);
			if (spaceIndex == -1)
			{
				spaceIndex = width;
			}
			if (spaceIndex <= width * WIDTH_CUT_OFF)
			{
				spaceIndex = width;
			}
			result.add(text.substring(0, spaceIndex));
			text = text.substring(spaceIndex + 1);
		}
		result.add(text);
		
		return result;
	}
	
	public static boolean isCtrlC(KeyStroke keyStroke)
	{
		return (keyStroke.getKeyType() == KeyType.Character)
			&& (keyStroke.isCtrlDown())
			&& (keyStroke.getCharacter() == 'c');
	}
	
	public static String execute(Path work, Path path, String... args) throws IOException, InterruptedException
	{
		List<String> processArguments = new ArrayList<>();
		processArguments.add(path.toString());
		processArguments.addAll(Arrays.asList(args));
		
		ProcessBuilder builder = new ProcessBuilder(processArguments);
		builder.directory(work.toFile());
		builder.redirectOutput(Redirect.PIPE);
		Process process = builder.start();
		int status = process.waitFor();
		if (status != 0)
		{
			throw new IOException("process exited with non-zero code: " + status);
		}
		InputStream inputStream = process.getInputStream();
		String processOutput = new String(inputStream.readAllBytes());
		return processOutput;
	}
	
	public static String tabLines(String text)
	{
		return text.lines()
			.map(s -> "  " + s)
			.collect(Collectors.joining("\n"));
	}
	
	public static String formatDuration(Duration duration)
	{
		return DurationFormatUtils.formatDuration(duration.toMillis(), "d'd' HH'h' mm'm' ss's'");
	}
}
