package endercrypt.projectgrowth.main.utility.thread;

public abstract class LoopedSmartRunnable extends SmartRunnable
{
	@Override
	protected final void execute() throws Exception
	{
		while (!Thread.currentThread().isInterrupted())
		{
			loop();
		}
	}

	protected abstract void loop() throws Exception;
}
