package endercrypt.projectgrowth.main.utility.thread;

public abstract class SmartRunnable implements Runnable
{
	@Override
	public final void run()
	{
		try
		{
			execute();
		}
		catch (InterruptedException e)
		{
			return;
		}
		catch (Exception e)
		{
			onException(e);
		}
	}

	protected abstract void execute() throws Exception;

	protected void onException(Exception e)
	{
		e.printStackTrace();
	}
}
