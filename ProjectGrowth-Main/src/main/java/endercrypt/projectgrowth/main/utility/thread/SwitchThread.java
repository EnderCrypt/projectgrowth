package endercrypt.projectgrowth.main.utility.thread;

public class SwitchThread
{
	private final String name;
	private final RunnableFactory runnableFactory;
	private Thread thread;

	public SwitchThread(String name, RunnableFactory runnableFactory)
	{
		this.name = name;
		this.runnableFactory = runnableFactory;
	}

	public String getName()
	{
		return name;
	}

	public synchronized boolean isRunning()
	{
		return thread != null && !thread.isInterrupted();
	}

	public synchronized void start()
	{
		if (isRunning())
		{
			throw new IllegalStateException(getName() + " already running");
		}
		thread = new Thread(runnableFactory.create());
		thread.setName(getName());
		thread.start();
	}

	public synchronized void stop()
	{
		if (!isRunning())
		{
			throw new IllegalStateException(getName() + " is not running");
		}
		thread.interrupt();
		thread = null;
	}

	public synchronized void toggle()
	{
		if (isRunning())
		{
			stop();
		}
		else
		{
			start();
		}
	}

	public interface RunnableFactory
	{
		public Runnable create();
	}
}
