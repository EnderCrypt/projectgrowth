package endercrypt.projectgrowth.main.application.modules;


import endercrypt.library.framework.systems.modules.StandardModules;
import endercrypt.projectgrowth.main.modules.commands.CommandModule;
import endercrypt.projectgrowth.main.modules.compiler.CompilerModule;
import endercrypt.projectgrowth.main.modules.gui.GuiModule;
import endercrypt.projectgrowth.main.modules.information.InformationModule;
import endercrypt.projectgrowth.main.modules.log.LogModule;
import endercrypt.projectgrowth.main.modules.save.SaveModule;
import endercrypt.projectgrowth.main.modules.simulation.SimulationModule;
import endercrypt.projectgrowth.main.modules.terminal.TerminalModule;
import endercrypt.projectgrowth.main.modules.thread.ThreadModule;
import endercrypt.projectgrowth.main.modules.tui.TuiModule;


public interface Modules extends StandardModules
{
	public CommandModule command();
	
	public TerminalModule terminal();
	
	public TuiModule tui();
	
	public SimulationModule simulation();
	
	public LogModule log();
	
	public CompilerModule compiler();
	
	public GuiModule gui();
	
	public ThreadModule thread();
	
	public SaveModule save();
	
	public InformationModule information();
}
