package endercrypt.projectgrowth.main.application.exits;


import endercrypt.library.framework.systems.exits.ExitCode;
import endercrypt.library.framework.systems.exits.StandardExits;


public interface Exits extends StandardExits
{
	@ExitCode(code = 6, description = "Terminal failure")
	public void terminalFailure();
	
	@ExitCode(code = 7, description = "Data failure")
	public void dataFailure();
}
