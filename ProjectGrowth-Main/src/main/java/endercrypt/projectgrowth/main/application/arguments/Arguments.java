package endercrypt.projectgrowth.main.application.arguments;


import endercrypt.library.groundwork.command.StandardArguments;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;


@Getter
@Command(mixinStandardHelpOptions = true, helpCommand = true)
public class Arguments extends StandardArguments
{
	@Parameters(description = "Commands to automatically execute on startup")
	private List<String> parameters = new ArrayList<>();
}
