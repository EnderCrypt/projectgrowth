package endercrypt.projectgrowth.main.application;


import endercrypt.library.framework.Application;
import endercrypt.library.groundwork.information.Information;
import endercrypt.projectgrowth.main.application.arguments.Arguments;
import endercrypt.projectgrowth.main.application.exits.Exits;
import endercrypt.projectgrowth.main.application.modules.Modules;
import endercrypt.projectgrowth.setting.Configurations;


public class ProjectGrowth extends Application
{
	public static void main(String[] args)
	{
		Application.launch(ProjectGrowth.class, args);
	}
	
	@Override
	protected void main() throws Exception
	{
		// hardware accelerated rendering of gui
		System.setProperty("sun.java2d.opengl", "true");
		
		// welcome
		modules().log().write(getWelcomeMessage());
		
		// command line parameters
		modules().command().executeParameterArguments();
	}
	
	private String getWelcomeMessage()
	{
		Information information = getInformation();
		return """
			Welcome to %s
			Created by %s
			Version: %s
			
			Type 'start' to start.
			"""
			.formatted(
				information.getName(),
				information.getAuthor(),
				information.getVersion());
	}
	
	@Override
	public Modules modules()
	{
		return (Modules) super.modules();
	}
	
	@Override
	public Arguments arguments()
	{
		return (Arguments) super.arguments();
	}
	
	@Override
	public Configurations configurations()
	{
		return (Configurations) super.configurations();
	}
	
	@Override
	public Exits exits()
	{
		return (Exits) super.exits();
	}
}
