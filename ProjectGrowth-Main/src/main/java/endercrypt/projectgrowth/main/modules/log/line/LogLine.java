package endercrypt.projectgrowth.main.modules.log.line;


import java.util.List;

import com.googlecode.lanterna.TextColor;


public interface LogLine
{
	public abstract TextColor getColor();

	public abstract List<String> getLines(int width);
}
