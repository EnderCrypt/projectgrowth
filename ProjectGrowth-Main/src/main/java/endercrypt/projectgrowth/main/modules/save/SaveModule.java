package endercrypt.projectgrowth.main.modules.save;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.log.LogModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import humanize.Humanize;


public class SaveModule extends CoreModule<ProjectGrowth>
{
	private static final Path directory = Paths.get("./saves");
	
	public SaveModule(ProjectGrowth application)
	{
		super(application);
	}
	
	public void considerAutosave(long tick)
	{
		if (tick % application.configurations().getAutosave().getFrequency() == 0)
		{
			autosave();
		}
	}
	
	private synchronized void autosave()
	{
		perform("autosave", application.configurations().getAutosave().getFilename(), IoAction.save);
	}
	
	public void perform(String actionName, Path filename, IoAction action)
	{
		LogModule logModule = application.modules().log();
		try
		{
			// create directory
			if (!Files.exists(directory))
			{
				Files.createDirectory(directory);
			}
			Path path = directory.resolve(filename);
			
			// commit
			long time = System.nanoTime();
			boolean result = action.commit(application, path);
			time = System.nanoTime() - time;
			String resultMessage = result ? "completed" : "failed";
			logModule.write(actionName + " " + resultMessage + " (" + Humanize.nanoTime(time) + ")");
		}
		catch (IOException | ClassNotFoundException e)
		{
			logModule.write("failed to " + actionName);
			logModule.writeException(e);
		}
	}
}
