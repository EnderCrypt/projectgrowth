package endercrypt.projectgrowth.main.modules.log;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.log.line.LogLine;
import endercrypt.projectgrowth.main.modules.log.line.TextLogLine;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import lombok.NonNull;


public class LogModule extends CoreModule<ProjectGrowth>
{
	private List<LogLine> lines = new ArrayList<>();
	
	public LogModule(ProjectGrowth application)
	{
		super(application);
	}
	
	public void write()
	{
		write("");
	}
	
	public void write(@NonNull String text)
	{
		write(text, Color.WHITE);
	}
	
	public void writeException(@NonNull Throwable e)
	{
		writeErr(ExceptionUtils.getStackTrace(e));
	}
	
	public void writeErr(@NonNull String text)
	{
		write(text, Color.RED);
	}
	
	public void write(@NonNull String text, @NonNull Color color)
	{
		for (String line : text.split("\n"))
		{
			write(new TextLogLine(line, color));
		}
	}
	
	public void write(@NonNull LogLine consoleLine)
	{
		lines.add(consoleLine);
		application.modules().terminal().requestRedraw();
	}
	
	public synchronized void draw(TextGraphics graphics)
	{
		TerminalSize size = graphics.getSize();
		int position = size.getRows() - 1;
		int index = lines.size() - 1;
		while (position >= 0 && index >= 0)
		{
			LogLine consoleLine = lines.get(index--);
			graphics.setForegroundColor(consoleLine.getColor());
			List<String> localLines = consoleLine.getLines(size.getColumns());
			position -= localLines.size();
			for (int i = 0; i < localLines.size(); i++)
			{
				int y = position + i + 1;
				graphics.putString(0, y, localLines.get(i));
			}
		}
	}
}
