package endercrypt.projectgrowth.main.modules.simulation;


import endercrypt.library.framework.systems.modules.CoreModuleDependencies;
import endercrypt.library.framework.systems.modules.CoreThreadModule;
import endercrypt.library.framework.systems.modules.exception.CoreModuleStateException;
import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.simulation.TpsCounter.Timer;
import endercrypt.projectgrowth.main.modules.thread.ThreadModule;
import endercrypt.projectgrowth.setting.Configurations;
import endercrypt.projectgrowth.setting.environment.arena.EnvironmentArena;
import endercrypt.projectgrowth.simulation.arena.abstr.Arena;
import endercrypt.projectgrowth.simulation.arena.abstr.ArenaFactory;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.executor.SimulationExecutor;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.simulation.SimulationControl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lombok.Getter;


public class SimulationModule extends CoreThreadModule<ProjectGrowth>
{
	private static final Logger logger = LogManager.getLogger(SimulationModule.class);
	
	@Getter
	private final TpsCounter tpsCounter = new TpsCounter();
	
	@Getter
	private final SimulationControl control = new SimulationControl();
	
	@Getter
	private Simulation simulation;
	
	public SimulationModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		setupSimulation();
		super.onStart();
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(ThreadModule.class);
	}
	
	private void setupSimulation()
	{
		Configurations configurations = application.configurations();
		EnvironmentArena arenaConfiguration = configurations.getEnvironment().getArena();
		Arena arena = ArenaFactory.create(arenaConfiguration);
		simulation = new Simulation(configurations, arena);
	}
	
	@Override
	protected void thread() throws Exception
	{
		while (true)
		{
			getControl().awaitTick();
			tryTick();
			application.modules().save().considerAutosave(getSimulation().getTickCount());
		}
	}
	
	private synchronized void tryTick() throws InterruptedException
	{
		if (getControl().isRunning() && getSimulation().getEntities().count(Microbe.class) == 0)
		{
			application.modules().log().writeErr("Simulation ran out of microbes");
			getControl().stop();
			return;
		}
		try
		{
			SimulationExecutor executor = application.modules().thread().getSimulationExecutor();
			try (Timer timer = tpsCounter.timeThisTick())
			{
				getSimulation().tick(executor);
			}
		}
		catch (InterruptedException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			logger.error("Simulation update failed", e);
			application.modules().log().write("simulation update failed");
			application.modules().log().writeException(e);
		}
		try
		{
			application.modules().terminal().requestRedraw();
		}
		catch (CoreModuleStateException e)
		{
			// shutdown ongoing
		}
		application.modules().gui().repaint();
	}
	
	public synchronized void load(ObjectInputStream stream) throws ClassNotFoundException, IOException
	{
		setupSimulation();
		SmartLayeredArray<Entity> entities = simulation.getEntities();
		entities.clear();
		int entityCount = stream.readInt();
		for (int i = 0; i < entityCount; i++)
		{
			entities.add((Entity) stream.readObject());
		}
		entities.flush();
	}
	
	public synchronized void save(ObjectOutputStream stream) throws IOException
	{
		SmartLayeredArray<Entity> entities = simulation.getEntities();
		
		List<Serializable> serializables = entities.serializable();
		stream.writeInt(serializables.size());
		for (Serializable serializable : serializables)
		{
			stream.writeObject(serializable);
		}
	}
	
	@Override
	protected void onException(Exception e)
	{
		logger.error("Simulation thread crashed", e);
		super.onException(e);
	}
}
