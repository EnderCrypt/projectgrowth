package endercrypt.projectgrowth.main.modules.information;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.utility.Utility;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;


public class InformationModule extends CoreModule<ProjectGrowth>
{
	private static final Logger logger = LogManager.getLogger(InformationModule.class);
	
	protected static final Gson gson = new Gson();
	
	private LocalDateTime startTime;
	private boolean developmentVersion;
	private BuildInformation buildInformation;
	
	public InformationModule(ProjectGrowth projectGrowth)
	{
		super(projectGrowth);
		
		this.startTime = LocalDateTime.now();
		this.developmentVersion = Files.exists(Paths.get(".git"));
	}
	
	@Override
	protected void onStart() throws Exception
	{
		this.buildInformation = BuildInformation.generate().orElse(null);
		if (buildInformation == null)
		{
			logger.warn("Build information unavailable!");
		}
		else
		{
			logger.info("Build information: \n" + buildInformation);
		}
	}
	
	public LocalDateTime getStartTime()
	{
		return startTime;
	}
	
	public Duration getUptime()
	{
		return Duration.between(getStartTime(), LocalDateTime.now());
	}
	
	public boolean isDevelopmentVersion()
	{
		return developmentVersion;
	}
	
	public Optional<BuildInformation> getBuildInformation()
	{
		return Optional.ofNullable(buildInformation);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Dev Version:   ").append(isDevelopmentVersion() ? "Yes" : "No").append("\n");
		sb.append("Uptime:    ").append(Utility.formatDuration(getUptime())).append("\n");
		sb.append("Build:\n");
		String information = getBuildInformation()
			.map(b -> b.toString() + "\n")
			.orElse("No build information available.");
		
		sb.append(Utility.tabLines(information));
		return sb.toString().strip();
	}
}
