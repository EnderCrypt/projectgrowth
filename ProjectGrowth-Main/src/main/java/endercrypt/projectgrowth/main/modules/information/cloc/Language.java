package endercrypt.projectgrowth.main.modules.information.cloc;


import com.google.gson.JsonObject;

import lombok.NonNull;


public class Language extends Summary
{
	private final String name;
	
	public Language(@NonNull String name, @NonNull JsonObject root)
	{
		super(root);
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
}
