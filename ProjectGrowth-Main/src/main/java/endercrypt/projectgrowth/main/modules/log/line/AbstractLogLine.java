package endercrypt.projectgrowth.main.modules.log.line;


import java.awt.Color;

import com.googlecode.lanterna.TextColor;

import lombok.NonNull;


public abstract class AbstractLogLine implements LogLine
{
	private final TextColor color;
	
	public AbstractLogLine(Color color)
	{
		this(new TextColor.RGB(color.getRed(), color.getGreen(), color.getBlue()));
	}
	
	public AbstractLogLine(@NonNull TextColor color)
	{
		this.color = color;
	}
	
	@Override
	public TextColor getColor()
	{
		return color;
	}
}
