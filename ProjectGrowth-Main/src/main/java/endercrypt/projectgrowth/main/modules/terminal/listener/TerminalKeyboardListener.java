package endercrypt.projectgrowth.main.modules.terminal.listener;


import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.utility.thread.SmartRunnable;

import java.io.IOException;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import lombok.NonNull;


public class TerminalKeyboardListener extends SmartRunnable
{
	private final ProjectGrowth projectGrowth;
	
	public TerminalKeyboardListener(@NonNull ProjectGrowth projectGrowth)
	{
		this.projectGrowth = projectGrowth;
	}
	
	@Override
	protected void execute() throws Exception
	{
		while (true)
		{
			KeyStroke keyStroke = readInput();
			if (keyStroke.getKeyType() == KeyType.EOF)
			{
				return;
			}
			projectGrowth.modules().tui().onKeyType(keyStroke);
			projectGrowth.modules().terminal().requestRedraw();
		}
	}
	
	private KeyStroke readInput() throws InterruptedException, IOException
	{
		try
		{
			return projectGrowth.modules().terminal().getScreen().readInput();
		}
		catch (RuntimeException e) // temp fix due to TerminalScreen.readInput() throwing RuntimeException instead of InterruptedException
		{
			if ("Blocking input was interrupted".equals(e.getMessage()))
			{
				throw new InterruptedException();
			}
			throw e;
		}
	}
}
