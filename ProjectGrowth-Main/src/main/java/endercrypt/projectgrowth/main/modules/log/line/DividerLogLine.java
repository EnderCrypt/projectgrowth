package endercrypt.projectgrowth.main.modules.log.line;


import java.awt.Color;
import java.util.List;


public class DividerLogLine extends AbstractLogLine
{
	private final String pattern;

	public DividerLogLine(String pattern, Color color)
	{
		super(color);
		this.pattern = pattern;
	}

	public String getPattern()
	{
		return pattern;
	}

	@Override
	public List<String> getLines(int width)
	{
		int repeats = (int) Math.ceil((double) width / pattern.length());
		String output = pattern.repeat(repeats);
		output = output.substring(0, width);
		return List.of(output);
	}

	public int getHeight(int width)
	{
		return width;
	}
}
