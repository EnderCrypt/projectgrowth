package endercrypt.projectgrowth.main.modules.commands.list;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;


@CommandSignature("")
public class ControlCommands
{
	@CommandSignature("status")
	public void status(@Include("growth") ProjectGrowth projectGrowth)
	{
		projectGrowth.modules().log().write("Simulation status:");
		String message = projectGrowth.modules().simulation().getControl().isRunning() ? "Running" : "Paused";
		projectGrowth.modules().log().write(message);
	}
	
	@CommandSignature("start")
	public void start(@Include("growth") ProjectGrowth projectGrowth)
	{
		if (projectGrowth.modules().simulation().getControl().isRunning())
		{
			projectGrowth.modules().log().writeErr("Simulation already running");
			return;
		}
		if (projectGrowth.modules().simulation().getSimulation().getEntities().count(Microbe.class) == 0)
		{
			projectGrowth.modules().log().writeErr("Simulation has no microbes");
			return;
		}
		projectGrowth.modules().simulation().getControl().play();
	}
	
	@CommandSignature("stop")
	public void stop(@Include("growth") ProjectGrowth projectGrowth)
	{
		if (!projectGrowth.modules().simulation().getControl().isRunning())
		{
			projectGrowth.modules().log().writeErr("Simulation already paused");
			return;
		}
		projectGrowth.modules().simulation().getControl().stop();
	}
	
	@CommandSignature("step")
	public void step(@Include("growth") ProjectGrowth projectGrowth)
	{
		projectGrowth.modules().simulation().getControl().step();
	}
}
