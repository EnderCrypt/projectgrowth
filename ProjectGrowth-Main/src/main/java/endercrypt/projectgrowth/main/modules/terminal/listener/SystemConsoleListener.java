package endercrypt.projectgrowth.main.modules.terminal.listener;


import endercrypt.projectgrowth.main.application.ProjectGrowth;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import lombok.NonNull;


public class SystemConsoleListener extends OutputStream
{
	public static PrintStream create(ProjectGrowth projectGrowth, Color color)
	{
		return new PrintStream(new SystemConsoleListener(projectGrowth, color));
	}
	
	private final ProjectGrowth projectGrowth;
	private final Color color;
	private StringBuilder stringBuilder = new StringBuilder();
	
	public SystemConsoleListener(@NonNull ProjectGrowth projectGrowth, @NonNull Color color)
	{
		this.projectGrowth = projectGrowth;
		this.color = color;
	}
	
	@Override
	public void write(int b) throws IOException
	{
		if (b == '\n')
		{
			projectGrowth.modules().log().write(stringBuilder.toString(), color);
			stringBuilder.setLength(0);
		}
		else
		{
			stringBuilder.append((char) b);
		}
	}
}
