package endercrypt.projectgrowth.main.modules.commands.list;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Piece;
import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.food.Food;
import endercrypt.projectgrowth.simulation.entities.food.FoodInfo;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


@CommandSignature("")
public class MicrobeCommands
{
	@CommandSignature("feed {quantity}")
	public void feed(@Include("growth") ProjectGrowth projectGrowth, @Piece int quantity)
	{
		for (int i = 0; i < quantity; i++)
		{
			FoodInfo info = FoodInfo.createDefault();
			Food food = new Food(info);
			projectGrowth.modules().simulation().getSimulation().getEntities().add(food);
		}
	}
	
	@CommandSignature("spawn {quantity} {path}")
	public void spawn(@Include("growth") ProjectGrowth projectGrowth, @Piece int quantity, @Piece Path path) throws FileNotFoundException, IOException
	{
		if (quantity < 1)
		{
			projectGrowth.modules().log().writeErr("cannot spawn " + quantity + " microbes");
			return;
		}
		if (!Files.exists(path))
		{
			projectGrowth.modules().log().writeErr("unable to locate " + path);
			return;
		}
		Genome genome = projectGrowth.modules().compiler().compile(path);
		for (int i = 0; i < quantity; i++)
		{
			MicrobeInfo info = MicrobeInfo.createDefault();
			Microbe microbe = new Microbe(info, genome);
			projectGrowth.modules().simulation().getSimulation().getEntities().add(microbe);
		}
		projectGrowth.modules().log().write("spawned " + quantity + " microbes based of " + path);
	}
	
	@CommandSignature("exterminate")
	public void extermine(@Include("growth") ProjectGrowth projectGrowth)
	{
		Simulation simulation = projectGrowth.modules().simulation().getSimulation();
		SmartLayeredArray<Entity> entities = simulation.getEntities();
		int count = entities.count(Microbe.class);
		entities.clear(Microbe.class);
		projectGrowth.modules().log().write("exterminated all life (" + count + ")");
	}
}
