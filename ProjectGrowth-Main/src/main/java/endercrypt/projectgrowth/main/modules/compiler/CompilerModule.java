package endercrypt.projectgrowth.main.modules.compiler;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.compiler.parse.DarwinCompiler;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.simulation.SimulationModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class CompilerModule extends CoreModule<ProjectGrowth>
{
	private DarwinCompiler compiler;
	
	public CompilerModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(SimulationModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		OperatorCollection operatorCollection = application.modules().simulation().getSimulation().getOperatorCollection();
		compiler = new DarwinCompiler(operatorCollection);
	}
	
	public Genome compile(Path file) throws IOException
	{
		return compile(Files.readString(file));
	}
	
	public Genome compile(String dna)
	{
		return compiler.compile(dna);
	}
}
