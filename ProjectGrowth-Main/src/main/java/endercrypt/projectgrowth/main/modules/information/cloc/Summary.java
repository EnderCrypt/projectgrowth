package endercrypt.projectgrowth.main.modules.information.cloc;


import com.google.gson.JsonObject;

import lombok.Getter;


@Getter
public class Summary
{
	private final int files;
	private final int lines;
	private final int blank;
	private final int comment;
	
	public Summary(JsonObject root)
	{
		this.files = root.get("nFiles").getAsInt();
		this.lines = root.get("code").getAsInt();
		this.blank = root.get("blank").getAsInt();
		this.comment = root.get("comment").getAsInt();
	}
}
