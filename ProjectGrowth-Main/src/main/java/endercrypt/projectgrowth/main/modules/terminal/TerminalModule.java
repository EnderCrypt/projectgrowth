package endercrypt.projectgrowth.main.modules.terminal;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;
import endercrypt.library.groundwork.information.Information;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.log.LogModule;
import endercrypt.projectgrowth.main.modules.terminal.listener.TerminalDrawThread;
import endercrypt.projectgrowth.main.modules.terminal.listener.TerminalKeyboardListener;
import endercrypt.projectgrowth.main.modules.terminal.listener.TerminalResizeListener;
import endercrypt.projectgrowth.main.modules.tui.TuiModule;

import java.io.IOException;

import javax.swing.WindowConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.screen.Screen.RefreshType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.ansi.UnixLikeTerminal.CtrlCBehaviour;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;


public class TerminalModule extends CoreModule<ProjectGrowth>
{
	private static final Logger logger = LogManager.getLogger(TerminalModule.class);
	
	private static final DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory()
		.setUnixTerminalCtrlCBehaviour(CtrlCBehaviour.TRAP);
	
	private Terminal terminal;
	private TerminalScreen screen;
	
	private Thread keyboardThread;
	private Thread drawThread;
	private TerminalDrawThread consoleDrawer = new TerminalDrawThread(application);
	
	public TerminalModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(TuiModule.class)
			.add(LogModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		// lanterna
		try
		{
			// terminal
			terminal = defaultTerminalFactory.createTerminal();
			terminal.addResizeListener(new TerminalResizeListener(application));
			terminal.setCursorVisible(true);
			
			// awt
			if (terminal instanceof SwingTerminalFrame swingTerminal)
			{
				Information information = application.getInformation();
				swingTerminal.setTitle(information.getName() + " - Version " + information.getVersion());
				swingTerminal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // TODO: use dispose on close and then listen for it to dispose and exit application normally
			}
			
			// console
			screen = new TerminalScreen(terminal);
			screen.startScreen();
			
			// keyboard listener
			keyboardThread = new Thread(new TerminalKeyboardListener(application), "Terminal keyboard thread");
			keyboardThread.start();
			
			// draw
			drawThread = new Thread(consoleDrawer, "Terminal draw thread");
			drawThread.start();
		}
		catch (IOException e)
		{
			logger.fatal("initializing terminal", e);
			application.exits().terminalFailure();
		}
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		keyboardThread.interrupt();
		drawThread.interrupt();
	}
	
	public Terminal getTerminal()
	{
		return terminal;
	}
	
	public TerminalScreen getScreen()
	{
		return screen;
	}
	
	public void requestRedraw()
	{
		consoleDrawer.draw();
	}
	
	public void forceRedraw()
	{
		// resize
		boolean resized = (screen.doResizeIfNecessary() != null);
		
		// clear
		screen.clear();
		
		// draw
		TextGraphics graphics = screen.newTextGraphics();
		TerminalPosition cursor = application.modules().tui().draw(graphics);
		screen.setCursorPosition(cursor);
		
		// refresh
		try
		{
			screen.refresh(resized ? RefreshType.COMPLETE : RefreshType.DELTA);
		}
		catch (IOException e)
		{
			logger.fatal("drawing screen", e);
			application.exits().terminalFailure();
		}
	}
}
