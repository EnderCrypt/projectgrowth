package endercrypt.projectgrowth.main.modules.tui;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.log.LogModule;
import endercrypt.projectgrowth.main.modules.tui.bar.StatusBar;
import endercrypt.projectgrowth.main.modules.tui.bar.impl.StatusBarEntryText;
import endercrypt.projectgrowth.main.utility.Utility;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.awt.Color;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;

import humanize.Humanize;


public class TuiModule extends CoreModule<ProjectGrowth>
{
	private StatusBar statusBar = new StatusBar(Color.BLACK, 1);
	private StatusBarEntryText statusBarExecutionStatus = new StatusBarEntryText(Color.BLACK);
	private StatusBarEntryText statusBarTicks = new StatusBarEntryText(Color.BLACK);
	private StatusBarEntryText statusBarTicksPerSecond = new StatusBarEntryText(Color.BLACK);
	private StatusBarEntryText statusBarMicrobes = new StatusBarEntryText(Color.BLACK);
	
	private StringBuilder input = new StringBuilder();
	
	public TuiModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(LogModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		statusBar.addEntry(new StatusBarEntryText("[" + application.getInformation().getName() + "]", Color.BLACK));
		statusBar.addEntry(statusBarExecutionStatus);
		statusBar.addEntry(statusBarTicks);
		statusBar.addEntry(statusBarTicksPerSecond);
		statusBar.addEntry(statusBarMicrobes);
	}
	
	public void onKeyType(KeyStroke keyStroke)
	{
		if (Utility.isCtrlC(keyStroke) && (input.isEmpty() || input.toString().equalsIgnoreCase("quit")))
		{
			input.setLength(0);
			return;
		}
		switch (keyStroke.getKeyType())
		{
		case Character:
			input.append(keyStroke.getCharacter());
		break;
		case Enter:
			if (input.length() > 0)
			{
				String command = input.toString();
				input.setLength(0);
				application.modules().command().execute(command);
			}
		break;
		case Backspace:
			if (input.length() > 0)
			{
				input.setLength(input.length() - 1);
			}
		break;
		default:
		break;
		}
	}
	
	public TerminalPosition draw(TextGraphics graphics)
	{
		// console
		TerminalSize size = graphics.getSize();
		TerminalPosition consolePosition = new TerminalPosition(0, 1);
		TerminalSize consoleSize = new TerminalSize(size.getColumns(), size.getRows() - 2);
		TextGraphics consoleGraphics = graphics.newTextGraphics(consolePosition, consoleSize);
		application.modules().log().draw(consoleGraphics);
		
		// status
		boolean running = application.modules().simulation().getControl().isRunning();
		statusBar.setBackgroundColor(running ? new Color(100, 200, 100) : Color.RED);
		statusBarExecutionStatus.setText(running ? "Running" : "Stopped");
		statusBarTicks.setText("Tick: " + application.modules().simulation().getSimulation().getTickCount());
		String tpsText = application.modules().simulation().getTpsCounter()
			.getTimePerTick()
			.map(t -> (long) (t * 1_000_000_000))
			.map(Humanize::nanoTime)
			.map(String::valueOf)
			.orElse("---");
		statusBarTicksPerSecond.setText("(Delta: " + tpsText + ")");
		statusBarMicrobes.setText("Microbes: " + application.modules().simulation().getSimulation().getEntities().count(Microbe.class));
		statusBar.draw(graphics, 0);
		
		// input
		graphics.setForegroundColor(new TextColor.RGB(255, 255, 255));
		graphics.setBackgroundColor(new TextColor.RGB(100, 100, 100));
		graphics.drawLine(0, size.getRows() - 1, size.getColumns() - 1, size.getRows() - 1, ' ');
		String inputString = input.toString();
		if (inputString.length() > size.getColumns())
		{
			inputString = inputString.substring(inputString.length() - size.getColumns());
		}
		int inputY = size.getRows() - 1;
		graphics.putString(0, inputY, inputString);
		
		return new TerminalPosition(inputString.length(), inputY);
	}
}
