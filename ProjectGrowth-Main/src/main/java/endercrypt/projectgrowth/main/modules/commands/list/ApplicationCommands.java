package endercrypt.projectgrowth.main.modules.commands.list;


import endercrypt.library.intellicommand.annotation.CommandAlias;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.utility.Utility;


@CommandSignature("")
public class ApplicationCommands
{
	@CommandSignature("gui")
	public void gui(@Include("growth") ProjectGrowth projectGrowth)
	{
		projectGrowth.modules().gui().createWindow();
	}
	
	@CommandSignature("info")
	public void info(@Include("growth") ProjectGrowth projectGrowth)
	{
		String information = Utility.tabLines(projectGrowth.modules().information().toString());
		projectGrowth.modules().log().write(information);
	}
	
	@CommandSignature("quit")
	@CommandAlias({ "exit", "shutdown" })
	public void quit(@Include("growth") ProjectGrowth projectGrowth)
	{
		projectGrowth.exits().success();
	}
}
