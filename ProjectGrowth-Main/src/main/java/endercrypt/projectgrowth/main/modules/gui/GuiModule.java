package endercrypt.projectgrowth.main.modules.gui;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.groundwork.information.Information;
import endercrypt.projectgrowth.gui.GuiWindow;
import endercrypt.projectgrowth.gui.GuiWindowManager;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.simulation.SimulationControl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class GuiModule extends CoreModule<ProjectGrowth>
{
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	private final GuiWindowManager guiWindowManager = new GuiWindowManager(scheduler);
	
	public GuiModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		scheduler.shutdown();
	}
	
	public GuiWindow createWindow()
	{
		Simulation simulation = application.modules().simulation().getSimulation();
		SimulationControl simulationControl = application.modules().simulation().getControl();
		Information information = application.getInformation();
		String title = information.getName() + " - Version " + information.getVersion();
		return guiWindowManager.createWindow(simulation, simulationControl, title);
	}
	
	public void repaint()
	{
		guiWindowManager.repaint();
	}
}
