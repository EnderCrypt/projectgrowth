package endercrypt.projectgrowth.main.modules.tui.bar;


import endercrypt.projectgrowth.main.utility.Utility;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;

import lombok.NonNull;


public class StatusBar
{
	private TextColor backgroundColor;
	private int padding;
	private List<ConsoleBarEntry> entries = new ArrayList<>();
	
	public StatusBar(Color backgroundColor, int padding)
	{
		this(Utility.toTextColor(backgroundColor), padding);
	}
	
	public StatusBar(@NonNull TextColor backgroundColor, int padding)
	{
		this.backgroundColor = backgroundColor;
		this.padding = padding;
	}
	
	public void addEntry(ConsoleBarEntry entry)
	{
		entries.add(entry);
	}
	
	public void setBackgroundColor(Color backgroundColor)
	{
		setBackgroundColor(Utility.toTextColor(backgroundColor));
	}
	
	public void setBackgroundColor(TextColor backgroundColor)
	{
		this.backgroundColor = backgroundColor;
	}
	
	public TextColor getBackgroundColor()
	{
		return backgroundColor;
	}
	
	public void draw(TextGraphics graphics, int y)
	{
		TerminalSize size = graphics.getSize();
		
		// line
		TerminalPosition linePosition = new TerminalPosition(0, y);
		TerminalSize lineSize = new TerminalSize(size.getColumns(), 1);
		TextGraphics lineGraphics = graphics.newTextGraphics(linePosition, lineSize);
		
		// backgrond
		lineGraphics.setBackgroundColor(backgroundColor);
		lineGraphics.drawLine(0, 0, size.getColumns() - 1, 0, ' ');
		
		// entries
		int position = 0;
		for (ConsoleBarEntry entry : entries)
		{
			// position
			int width = entry.getWidth();
			TerminalPosition entryPosition = new TerminalPosition(position, 0);
			TerminalSize entrySize = new TerminalSize(width, 1);
			TextGraphics entryGraphics = graphics.newTextGraphics(entryPosition, entrySize);
			
			// draw
			entryGraphics.setBackgroundColor(backgroundColor);
			entry.draw(entryGraphics);
			
			// position
			position += width + padding;
		}
	}
}
