package endercrypt.projectgrowth.main.modules.thread;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.simulation.executor.SimulationExecutor;


public class ThreadModule extends CoreModule<ProjectGrowth>
{
	private SimulationExecutor simulationExecutor;
	
	public ThreadModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		int threadCount = application.configurations().getPerformance().getThreads().getSimulation();
		simulationExecutor = new SimulationExecutor(threadCount);
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		simulationExecutor.shutdown();
	}
	
	public SimulationExecutor getSimulationExecutor()
	{
		return simulationExecutor;
	}
}
