package endercrypt.projectgrowth.main.modules.commands.list;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Piece;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.utility.Utility;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;


@CommandSignature("")
public class ScreenshotCommands
{
	private static final String SCREENSHOT_FORMAT = "png";
	private static final Path SCREENSHOT_DIRECTORY = Paths.get("screenshots/");
	
	@CommandSignature("screenshot {size}")
	public void screenshot(@Include("growth") ProjectGrowth projectGrowth, @Piece int size) throws IOException
	{
		projectGrowth.modules().log().write("Processing screenshot...");
		
		// image
		BufferedImage image = new BufferedImage(size * 2, size * 2, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = image.createGraphics();
		
		// clear
		g2d.setColor(Color.WHITE);
		g2d.setBackground(new Color(255, 255, 255));
		g2d.clearRect(0, 0, size * 2, size * 2);
		
		// setup transform
		AffineTransform transform = new AffineTransform();
		transform.translate(size, size);
		g2d.setTransform(transform);
		
		// microbes
		projectGrowth.modules().simulation().getSimulation().draw(g2d);
		
		// draw center
		g2d.setColor(Color.BLACK);
		g2d.drawLine(-10, 0, 10, 0);
		g2d.drawLine(0, -10, 0, 10);
		g2d.dispose();
		
		// save
		Files.createDirectories(SCREENSHOT_DIRECTORY);
		Path screenshotFile = SCREENSHOT_DIRECTORY.resolve(Utility.dateString() + "." + (SCREENSHOT_FORMAT.toLowerCase()));
		ImageIO.write(image, "PNG", screenshotFile.toFile());
		projectGrowth.modules().log().write("saved as " + screenshotFile);
	}
}
