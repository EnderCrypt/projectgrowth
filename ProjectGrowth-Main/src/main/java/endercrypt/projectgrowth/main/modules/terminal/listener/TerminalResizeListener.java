package endercrypt.projectgrowth.main.modules.terminal.listener;


import endercrypt.projectgrowth.main.application.ProjectGrowth;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.terminal.Terminal;


public class TerminalResizeListener implements com.googlecode.lanterna.terminal.TerminalResizeListener
{
	private final ProjectGrowth projectGrowth;

	public TerminalResizeListener(ProjectGrowth projectGrowth)
	{
		this.projectGrowth = projectGrowth;
	}

	@Override
	public void onResized(Terminal terminal, TerminalSize newSize)
	{
		projectGrowth.modules().terminal().requestRedraw();
	}
}
