package endercrypt.projectgrowth.main.modules.tui.bar;


import com.googlecode.lanterna.graphics.TextGraphics;


public interface ConsoleBarEntry
{
	public int getWidth();

	public void draw(TextGraphics graphics);
}
