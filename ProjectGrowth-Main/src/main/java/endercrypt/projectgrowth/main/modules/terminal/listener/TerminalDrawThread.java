package endercrypt.projectgrowth.main.modules.terminal.listener;


import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.utility.thread.LoopedSmartRunnable;


public class TerminalDrawThread extends LoopedSmartRunnable
{
	private final ProjectGrowth projectGrowth;

	private volatile boolean draw = true;

	public TerminalDrawThread(ProjectGrowth projectGrowth)
	{
		this.projectGrowth = projectGrowth;
	}

	public void draw()
	{
		synchronized (this)
		{
			draw = true;
			notifyAll();
		}
	}

	@Override
	protected void loop() throws Exception
	{
		if (draw)
		{
			draw = false;
			projectGrowth.modules().terminal().forceRedraw();
		}

		synchronized (this)
		{
			if (!draw)
			{
				wait();
			}
		}
	}
}
