package endercrypt.projectgrowth.main.modules.simulation;


import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;

import lombok.RequiredArgsConstructor;


public class TpsCounter
{
	private static final int MAX_SIZE = 100;
	
	private final Queue<Double> tickTime = new ArrayDeque<>(MAX_SIZE);
	
	public Timer timeThisTick()
	{
		return new Timer();
	}
	
	public boolean isReady()
	{
		return tickTime.size() > 0;
	}
	
	/**
	 * Calculate a rough time that a tick takes. the last ticks delay has most
	 * value while the second last tick has half the value and so on.
	 */
	public Optional<Double> getTimePerTick()
	{
		if (isReady() == false)
		{
			return Optional.empty();
		}
		double result = 0;
		int counter = 0;
		for (double delta : tickTime)
		{
			counter++;
			double difference = (delta - result);
			double normalizedDelta = difference / counter;
			result += normalizedDelta;
		}
		return Optional.of(result);
	}
	
	public Optional<Double> getTps()
	{
		return getTimePerTick()
			.map(timePerTick -> 1.0 / timePerTick);
	}
	
	public void addTime(double time)
	{
		synchronized (tickTime)
		{
			if (tickTime.size() == MAX_SIZE)
			{
				tickTime.remove();
			}
			tickTime.add(time);
		}
	}
	
	@RequiredArgsConstructor
	public class Timer implements Closeable
	{
		private final long start = System.currentTimeMillis();
		
		@Override
		public void close() throws IOException
		{
			long end = System.currentTimeMillis();
			long delta = end - start;
			addTime(delta / 1000.0);
		}
	}
}
