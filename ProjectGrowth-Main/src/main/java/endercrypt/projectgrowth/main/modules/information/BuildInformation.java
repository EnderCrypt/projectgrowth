package endercrypt.projectgrowth.main.modules.information;


import endercrypt.library.commons.data.DataSource;
import endercrypt.projectgrowth.main.utility.Utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class BuildInformation
{
	private static final Logger logger = LogManager.getLogger(BuildInformation.class);
	
	public static Optional<BuildInformation> generate()
	{
		try
		{
			ClocReport clocReport = generateClocReport();
			String hash = generateHash();
			ZonedDateTime time = generateTime();
			return Optional.of(new BuildInformation(clocReport, hash, time));
		}
		catch (FileNotFoundException e)
		{
			return Optional.empty();
		}
		catch (IOException e)
		{
			logger.error("Failed to read resource " + e.getMessage(), e);
			return Optional.empty();
		}
	}
	
	private static ClocReport generateClocReport() throws IOException
	{
		String text = DataSource.RESOURCES.read("build/cloc.json").asString().strip();
		JsonObject json = InformationModule.gson.fromJson(text, JsonObject.class);
		return new ClocReport(json);
	}
	
	private static String generateHash() throws IOException
	{
		return DataSource.RESOURCES.read("build/hash.txt").asString().strip();
	}
	
	private static ZonedDateTime generateTime() throws IOException
	{
		String text = DataSource.RESOURCES.read("build/time.txt").asString().strip();
		return ZonedDateTime.parse(text);
	}
	
	@NonNull
	private final ClocReport clocReport;
	@NonNull
	private final String hash;
	@NonNull
	private final ZonedDateTime time;
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Git Hash:   ").append(getHash()).append("\n");
		sb.append("Build Date:   ").append(Utility.dateString(getTime())).append("\n");
		sb.append("Lines of Code:\n").append(Utility.tabLines(getClocReport().toString()));
		
		return sb.toString().strip();
	}
}
