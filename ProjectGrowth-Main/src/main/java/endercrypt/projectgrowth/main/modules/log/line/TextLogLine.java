package endercrypt.projectgrowth.main.modules.log.line;


import endercrypt.projectgrowth.main.utility.Utility;

import java.awt.Color;
import java.util.List;

import lombok.NonNull;


public class TextLogLine extends AbstractLogLine
{
	private static final int TAB_SIZE = 3;
	private final String text;
	
	public TextLogLine(@NonNull String text, @NonNull Color color)
	{
		super(color);
		
		// check
		if (text.contains("\n"))
		{
			throw new IllegalArgumentException("text cannot contain newlines");
		}
		
		// tabs
		text = text.replace("\t", " ".repeat(TAB_SIZE));
		
		// vars
		this.text = text;
	}
	
	public String getText()
	{
		return text;
	}
	
	@Override
	public List<String> getLines(int width)
	{
		return Utility.lineDivider(getText(), width);
	}
	
	public int getHeight(int width)
	{
		return getLines(width).size();
	}
}
