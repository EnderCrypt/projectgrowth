package endercrypt.projectgrowth.main.modules.commands;


import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;
import endercrypt.projectgrowth.common.ProjectGrowthRuntimeException;
import endercrypt.projectgrowth.common.modules.ProjectGrowthModuleInfo;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.log.line.DividerLogLine;

import java.awt.Color;
import java.lang.reflect.Constructor;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;


public class CommandModule extends CoreModule<ProjectGrowth>
{
	private final IntelliCommand intelliCommand = new IntelliCommand();
	
	public CommandModule(ProjectGrowth application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		String commandPackageString = ProjectGrowthModuleInfo.main.getPackageString() + ".modules.commands.list";
		Reflections reflections = new Reflections(commandPackageString);
		Set<Class<?>> commandClasses = reflections.getTypesAnnotatedWith(CommandSignature.class);
		if (commandClasses.isEmpty())
		{
			throw new ProjectGrowthRuntimeException("Found zero commands in " + commandPackageString);
		}
		for (Class<?> commandClass : commandClasses)
		{
			Constructor<?> constructor = commandClass.getConstructor();
			Object instance = constructor.newInstance();
			intelliCommand.commands().register(instance);
		}
	}
	
	public void executeParameterArguments()
	{
		for (String parameter : application.arguments().getParameters())
		{
			execute(parameter);
		}
	}
	
	public void execute(String command)
	{
		command = command.strip();
		
		application.modules().log().write(new DividerLogLine("---", Color.GRAY));
		application.modules().log().write("» " + command);
		
		Bundle bundle = new Bundle();
		bundle.attach().instance("growth", application);
		
		try
		{
			intelliCommand.execute(command, bundle);
		}
		catch (UnderlyingCommandException e)
		{
			e.printStackTrace();
			application.modules().log().writeException(e.getCause());
		}
		catch (CommandNotFoundException e)
		{
			application.modules().log().writeErr("Command not found");
		}
		catch (CommandArgumentMismatchException e)
		{
			application.modules().log().writeErr("did you mean?\n\t - " + e.getCommands()
				.stream()
				.map(c -> c.getPrimarySignature().asText())
				.collect(Collectors.joining("\n\t - ")));
		}
		catch (MalformedCommandException e)
		{
			application.modules().log().writeErr("Malformed command");
		}
		if (application.modules().simulation().getControl().isRunning() == false)
		{
			application.modules().simulation().getSimulation().getEntities().flush();
		}
	}
}
