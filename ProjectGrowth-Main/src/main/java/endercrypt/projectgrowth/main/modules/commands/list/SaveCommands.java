package endercrypt.projectgrowth.main.modules.commands.list;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Piece;
import endercrypt.projectgrowth.main.application.ProjectGrowth;
import endercrypt.projectgrowth.main.modules.save.IoAction;

import java.nio.file.Path;


@CommandSignature("")
public class SaveCommands
{
	@CommandSignature("load {filename}")
	public void load(@Include("growth") ProjectGrowth projectGrowth, @Piece Path filename)
	{
		projectGrowth.modules().simulation().getControl().stop();
		projectGrowth.modules().save().perform("load", filename, IoAction.load);
	}
	
	@CommandSignature("save {filename}")
	public void save(@Include("growth") ProjectGrowth projectGrowth, @Piece Path filename)
	{
		projectGrowth.modules().save().perform("save", filename, IoAction.save);
	}
}
