package endercrypt.projectgrowth.main.modules.information;


import endercrypt.projectgrowth.main.modules.information.cloc.Language;
import endercrypt.projectgrowth.main.modules.information.cloc.Summary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import lombok.Getter;


public class ClocReport
{
	@Getter
	private final Summary summary;
	
	private List<Language> languages = new ArrayList<>();
	
	public ClocReport(JsonObject root)
	{
		summary = new Summary(root.get("SUM").getAsJsonObject());
		
		for (java.util.Map.Entry<String, JsonElement> entry : root.entrySet())
		{
			String name = entry.getKey();
			if (!name.equals("SUM") && !name.equals("header"))
			{
				JsonObject json = entry.getValue().getAsJsonObject();
				languages.add(new Language(name, json));
			}
		}
	}
	
	public List<Language> getLanguages()
	{
		return Collections.unmodifiableList(languages);
	}
	
	public Optional<Language> getLanguage(String language)
	{
		return languages.stream().filter(l -> l.getName().equalsIgnoreCase(language)).findFirst();
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("Total: " + getSummary().getLines() + "\n");
		for (Language language : languages)
		{
			builder.append("- " + language.getName() + ": " + language.getLines() + "\n");
		}
		return builder.toString().strip();
	}
}
