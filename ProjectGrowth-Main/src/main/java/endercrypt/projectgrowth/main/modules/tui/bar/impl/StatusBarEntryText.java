package endercrypt.projectgrowth.main.modules.tui.bar.impl;


import endercrypt.projectgrowth.main.modules.tui.bar.ConsoleBarEntry;
import endercrypt.projectgrowth.main.utility.Utility;

import java.awt.Color;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;

import lombok.NonNull;
import lombok.Setter;


@Setter
public class StatusBarEntryText implements ConsoleBarEntry
{
	@NonNull
	private String text;
	
	@NonNull
	private TextColor color;
	
	public StatusBarEntryText(Color color)
	{
		this("", color);
	}
	
	public StatusBarEntryText(String text, Color color)
	{
		this(text, Utility.toTextColor(color));
	}
	
	public StatusBarEntryText(@NonNull String text, @NonNull TextColor color)
	{
		this.text = text;
		this.color = color;
	}
	
	public void setColor(@NonNull Color color)
	{
		setColor(Utility.toTextColor(color));
	}
	
	public void setColor(@NonNull TextColor color)
	{
		this.color = color;
	}
	
	@Override
	public int getWidth()
	{
		return text.length();
	}
	
	@Override
	public void draw(TextGraphics graphics)
	{
		graphics.setForegroundColor(color);
		graphics.putString(0, 0, text);
	}
}
