package endercrypt.projectgrowth.main.modules.save;


import endercrypt.library.commons.journal.JournalIo;
import endercrypt.projectgrowth.main.application.ProjectGrowth;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;


public interface IoAction
{
	public static final IoAction load = new IoAction()
	{
		@Override
		public boolean commit(ProjectGrowth projectGrowth, Path target) throws IOException, ClassNotFoundException
		{
			try
			{
				try (ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(JournalIo.read(target))))
				{
					projectGrowth.modules().simulation().load(stream);
				}
				return true;
			}
			catch (FileNotFoundException e)
			{
				projectGrowth.modules().log().write("file not found: " + target);
			}
			return false;
		}
	};
	
	public static final IoAction save = new IoAction()
	{
		@Override
		public boolean commit(ProjectGrowth projectGrowth, Path target) throws IOException, ClassNotFoundException
		{
			try (ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(JournalIo.write(target))))
			{
				projectGrowth.modules().simulation().save(stream);
			}
			return true;
		}
	};
	
	public boolean commit(ProjectGrowth projectGrowth, Path target) throws IOException, ClassNotFoundException;
}
