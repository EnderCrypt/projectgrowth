package endercrypt.projectgrowth.main.modules.simulation;


import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;


public class TpsCounterTest
{
	@Test
	public void timeDropOffMakesSense()
	{
		TpsCounter counter = new TpsCounter();
		assertEquals(Optional.empty(), counter.getTimePerTick());
		
		counter.addTime(10);
		assertEquals(Optional.of(10.0), counter.getTimePerTick());
		
		counter.addTime(5);
		assertEquals(Optional.of(7.5), counter.getTimePerTick());
	}
	
}
