package endercrypt.projectgrowth.simulation.entities.entity;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.utility.MicrobeUtility;

import java.awt.Graphics2D;
import java.io.Serializable;

import javax.annotation.OverridingMethodsMustInvokeSuper;


public abstract class Entity implements Serializable
{
	private static final long serialVersionUID = 5104386947709466468L;
	
	/**
	 *
	 */
	
	private boolean alive = true;
	
	public void update(Simulation simulation)
	{
		getInfo().setLastHitBy(null);
	}
	
	@OverridingMethodsMustInvokeSuper
	public void physicsUpdate(Simulation simulation)
	{
		EntityInfo info = getInfo();
		
		if (info.isMovable())
		{
			// movement
			info.getPosition().add(info.getMotion());
			
			// movement friction
			info.getMotion().multiply(1.0 - EntityInfo.MOVEMENT_FRICTION);
		}
		else
		{
			// stop
			info.getMotion().set(0, 0);
		}
	}
	
	public abstract EntityInfo getInfo();
	
	public abstract double getRadius();
	
	public final synchronized void destroy()
	{
		if (alive)
		{
			Microbe lastHitBy = getInfo().getLastHitBy().orElse(null);
			if (lastHitBy != null)
			{
				lastHitBy.getInfo().increaseKills();
			}
			alive = false;
		}
	}
	
	public boolean isAlive()
	{
		return alive;
	}
	
	public <T extends Entity> T spawn(T entity, double direction, double speed)
	{
		return spawn(entity, direction, Vector.byAngle(direction, speed));
	}
	
	public <T extends Entity> T spawn(T entity, double direction, Vector motion)
	{
		Vector particlePosition = entity.getInfo().getPosition();
		particlePosition.set(getInfo().getPosition());
		particlePosition.add(Vector.byAngle(direction, getRadius()));
		
		Vector particleMotion = entity.getInfo().getMotion();
		particleMotion.add(getInfo().getMotion());
		particleMotion.add(motion);
		
		return entity;
	}
	
	public void draw(Graphics2D g2d)
	{
		EntityInfo info = getInfo();
		Vector position = info.getPosition();
		double radius = getRadius();
		
		MicrobeUtility.drawCircle(g2d, position, radius, false);
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
	}
}
