package endercrypt.projectgrowth.simulation.entities.growth;


import endercrypt.projectgrowth.simulation.entities.entity.EntityInfo;
import endercrypt.projectgrowth.simulation.matter.GrowthComposition;

import lombok.Getter;


@Getter
public abstract class GrowthInfo extends EntityInfo
{
	private static final long serialVersionUID = 7536325534130433299L;
	
	/**
	 *
	 */
	
	private GrowthComposition composition = new GrowthComposition();
	
	public GrowthInfo()
	{
		super();
	}
	
	public GrowthInfo(GrowthInfo other)
	{
		super(other);
		this.composition.setAll(other.composition);
	}
}
