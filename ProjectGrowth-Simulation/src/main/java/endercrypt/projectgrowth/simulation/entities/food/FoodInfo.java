package endercrypt.projectgrowth.simulation.entities.food;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.entities.growth.GrowthInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;


public class FoodInfo extends GrowthInfo
{
	private static final long serialVersionUID = 8872092874232981342L;
	
	/**
	 *
	 */
	
	public static FoodInfo createDefault()
	{
		FoodInfo info = new FoodInfo();
		info.getComposition().add(Matter.ENERGY, Ender.math.randomRange(1000, 3000));
		info.getComposition().add(Matter.BODY, Ender.math.randomRange(100, 200));
		info.getPosition().add(Vector.random(10.0));
		return info;
	}
	
	public FoodInfo()
	{
		super();
	}
	
	public FoodInfo(GrowthInfo other)
	{
		super(other);
	}
}
