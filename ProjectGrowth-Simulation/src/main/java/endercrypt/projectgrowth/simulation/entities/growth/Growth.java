package endercrypt.projectgrowth.simulation.entities.growth;


import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.collision.CollisionVectorFactory;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.tie.TieCollection;
import endercrypt.projectgrowth.simulation.utility.MicrobeUtility;

import lombok.Getter;


public abstract class Growth extends Entity
{
	private static final long serialVersionUID = -8877101583137954926L;
	
	/**
	 *
	 */
	
	@Getter
	private final TieCollection ties = new TieCollection(this);
	
	@Override
	public abstract GrowthInfo getInfo();
	
	@Override
	public void physicsUpdate(Simulation simulation)
	{
		ties.update(simulation);
		
		applyCollisions(simulation);
		
		super.physicsUpdate(simulation);
	}
	
	private void applyCollisions(Simulation simulation)
	{
		double maxPush = 0;
		Vector push = new Vector(0.0, 0.0);
		
		for (Growth other : simulation.getEntities().iterate(Growth.class))
		{
			if (this != other)
			{
				CollisionVector<Growth> collision = CollisionVectorFactory.create(this, other);
				if (collision.isTouching())
				{
					double pushAmount = (collision.getOverlap() * 0.5) + 0.2;
					double direction = collision.getDirection() - Circle.HALF;
					push.add(Vector.byAngle(direction, pushAmount));
					maxPush = Math.max(maxPush, pushAmount);
				}
			}
		}
		
		push.truncate(maxPush * 0.2);
		getInfo().getMotion().add(push);
	}
	
	@Override
	public double getRadius()
	{
		return MicrobeUtility.areaToCircleRadius(getInfo().getComposition().getMass());
	}
	
	@Override
	public boolean isAlive()
	{
		if (getInfo().getComposition().lacks(Matter.ENERGY))
		{
			destroy();
		}
		return super.isAlive();
	}
}
