package endercrypt.projectgrowth.simulation.entities.microbe;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.entities.growth.GrowthInfo;
import endercrypt.projectgrowth.simulation.eye.Eyes;
import endercrypt.projectgrowth.simulation.matter.Matter;

import lombok.Getter;


@Getter
public class MicrobeInfo extends GrowthInfo
{
	private static final long serialVersionUID = 3689306572593513812L;
	
	/**
	 *
	 */
	
	public static final double MOVEMENT_SPEED_MULTIPLIER = 0.01;
	public static final double MAX_THRUST = 5;
	
	public static MicrobeInfo createDefault()
	{
		MicrobeInfo info = new MicrobeInfo();
		info.getComposition().set(Matter.ENERGY, 3000);
		info.getComposition().set(Matter.BODY, 3000);
		info.getPosition().set(Vector.random(50));
		return info;
	}
	
	private final Eyes eyes;
	private int age;
	private int kills;
	
	public MicrobeInfo()
	{
		super();
		this.eyes = new Eyes();
		this.age = 0;
		this.kills = 0;
	}
	
	public MicrobeInfo(MicrobeInfo other)
	{
		super(other);
		this.eyes = other.eyes;
		this.age = other.age;
		this.kills = other.kills;
	}
	
	public void increaseAge()
	{
		age++;
	}
	
	public void increaseKills()
	{
		kills++;
	}
}
