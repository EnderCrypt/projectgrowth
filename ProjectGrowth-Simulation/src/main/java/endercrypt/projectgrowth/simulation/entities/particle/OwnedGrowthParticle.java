package endercrypt.projectgrowth.simulation.entities.particle;


import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public abstract class OwnedGrowthParticle extends Particle
{
	private static final long serialVersionUID = 7012263207141843395L;
	
	/**
	 *
	 */
	
	private final Growth owner;
}
