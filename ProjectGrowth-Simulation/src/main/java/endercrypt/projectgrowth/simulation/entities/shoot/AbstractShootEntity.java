package endercrypt.projectgrowth.simulation.entities.shoot;


import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.entities.entity.EntityInfo;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.particle.OwnedGrowthParticle;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Graphics2D;


public abstract class AbstractShootEntity extends OwnedGrowthParticle
{
	private static final long serialVersionUID = 8477865318459103608L;
	
	/**
	 *
	 */
	
	private final EntityInfo info = new EntityInfo();
	protected int lifetime = 0;
	private int age = 0;
	
	public AbstractShootEntity(Growth owner)
	{
		super(owner);
	}
	
	@Override
	public final EntityInfo getInfo()
	{
		return info;
	}
	
	protected abstract boolean onCollision(Simulation simulation, CollisionVector<Growth> collision);
	
	@Override
	public void update(Simulation simulation)
	{
		lifetime--;
		if (lifetime <= 0)
		{
			if (age == 0)
			{
				throw new IllegalStateException(getClass() + " died of old age, at age 0");
			}
			destroy();
		}
		age++;
		
		CollisionVector.getGrowthCollisions(simulation, this)
			.filter(microbeVector -> microbeVector.getTarget().equals(getOwner()) == false)
			.anyMatch(collision -> onCollision(simulation, collision));
		
		super.update(simulation);
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		super.draw(g2d);
	}
}
