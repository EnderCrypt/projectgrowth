package endercrypt.projectgrowth.simulation.entities.microbe;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.genome.activation.ActivationContext;
import endercrypt.projectgrowth.dna.genome.activation.GenomeActivation;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.particle.OwnedGrowthParticle;
import endercrypt.projectgrowth.simulation.eye.EyesOutput;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Graphics2D;
import java.util.Optional;

import lombok.Getter;


@Getter
public class Microbe extends Growth
{
	private static final long serialVersionUID = -648365863243419751L;
	
	/**
	 *
	 */
	
	private final Memory memory = new Memory();
	private final Genome genome;
	private final MicrobeInfo info;
	private MicrobeInfo oldInfo;
	private EyesOutput eyesOutput;
	private GenomeActivation genomeActivation;
	
	public Microbe(MicrobeInfo info, Genome genome)
	{
		this.info = info;
		this.genome = genome;
		this.oldInfo = new MicrobeInfo(info);
	}
	
	public Optional<GenomeActivation> getGenomeActivation()
	{
		return Optional.ofNullable(genomeActivation);
	}
	
	@Override
	public void update(Simulation simulation)
	{
		// info
		oldInfo = new MicrobeInfo(info);
		
		// eyes
		eyesOutput = EyesOutput.perceive(simulation, this);
		
		// update
		simulation.getComponentCollection().microbeUpdate(simulation, this);
		
		// execute genome
		genomeActivation = GenomeActivation.of(getGenome(), new ActivationContext(memory));
		
		super.update(simulation);
	}
	
	@Override
	public void physicsUpdate(Simulation simulation)
	{
		// commit
		simulation.getComponentCollection().microbeCommit(simulation, this);
		
		super.physicsUpdate(simulation);
	}
	
	public <T extends OwnedGrowthParticle> T shootParticle(T particle, double speed)
	{
		double direction = getInfo().getEyes().getCenter().getVisionDirection(this);
		return spawn(particle, direction, speed);
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		// microbe
		g2d.setColor(genome.getVisualization().getSkinColor());
		super.draw(g2d);
		
		// facing line
		double radius = getRadius();
		Vector line = Vector.copy(info.getPosition());
		line.add(Vector.byAngle(info.getRotation().getRadians(), radius));
		g2d.drawLine((int) info.getPosition().x, (int) info.getPosition().y, (int) line.x, (int) line.y);
		
		// eye
		if (eyesOutput != null)
		{
			eyesOutput.draw(this, g2d);
		}
	}
}
