package endercrypt.projectgrowth.simulation.entities.shoot;


import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Color;
import java.awt.Graphics2D;


public class MemoryShootEntity extends AbstractShootEntity
{
	private static final long serialVersionUID = 2755161115809111752L;
	
	/**
	 *
	 */
	
	private final int location;
	private final int value;
	
	public MemoryShootEntity(Microbe owner, int location, int value)
	{
		super(owner);
		this.location = location;
		this.value = value;
		lifetime = 10;
	}
	
	@Override
	protected boolean onCollision(Simulation simulation, CollisionVector<Growth> collision)
	{
		destroy();
		
		Growth target = collision.getTarget();
		
		// set attacked by
		if (getOwner() instanceof Microbe attacker)
		{
			target.getInfo().setLastHitBy(attacker);
		}
		
		// set memory
		if (target instanceof Microbe victim)
		{
			victim.getMemory().set(location, value);
		}
		
		return true;
	}
	
	@Override
	public double getRadius()
	{
		return 5.0;
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.setColor(Color.MAGENTA);
		super.draw(g2d);
	}
}
