package endercrypt.projectgrowth.simulation.entities.shoot;


import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.growth.GrowthInfo;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.matter.GrowthComposition;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.utility.MicrobeUtility;

import java.awt.Color;
import java.awt.Graphics2D;


public class AttackShootEntity extends AbstractShootEntity
{
	private static final long serialVersionUID = 4436587045649769549L;
	
	/**
	 *
	 */
	
	private final double strength;
	
	public AttackShootEntity(Microbe owner, double strength)
	{
		super(owner);
		this.strength = strength;
		lifetime = 10;
	}
	
	@Override
	protected boolean onCollision(Simulation simulation, CollisionVector<Growth> collision)
	{
		destroy();
		
		Growth target = collision.getTarget();
		GrowthInfo info = target.getInfo();
		
		// set attacked by
		if (getOwner() instanceof Microbe attacker)
		{
			target.getInfo().setLastHitBy(attacker);
		}
		
		// drain energy
		GrowthComposition extract = info.getComposition().extract(strength);
		
		// feed direction
		double feedDirection = collision.getDirection() - Circle.HALF;
		
		// feed motion
		Vector feedMotion = Vector.copy(getInfo().getMotion());
		feedMotion.flip();
		feedMotion.normalize();
		feedMotion.multiply(5);
		
		// spawn feed
		FeedShootEntity feed = target.spawn(new FeedShootEntity(target, extract), feedDirection, feedMotion);
		simulation.getEntities().add(feed);
		return true;
	}
	
	@Override
	public double getRadius()
	{
		return MicrobeUtility.areaToCircleRadius(strength * 0.5);
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.setColor(Color.RED);
		super.draw(g2d);
	}
}
