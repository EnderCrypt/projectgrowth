package endercrypt.projectgrowth.simulation.entities.entity;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.common.geometry.Rotation;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.io.Serializable;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class EntityInfo implements Serializable
{
	private static final long serialVersionUID = 8713517932907799374L;
	
	/**
	 *
	 */
	
	public static final double MOVEMENT_FRICTION = 0.05;
	
	private final Rotation rotation;
	private final Vector position;
	private final Vector motion;
	private boolean movable;
	private Microbe lastHitBy;
	
	public EntityInfo()
	{
		this.rotation = Rotation.random();
		this.position = new Vector(0, 0);
		this.motion = new Vector(0, 0);
		this.movable = true;
	}
	
	public EntityInfo(EntityInfo other)
	{
		this.rotation = Rotation.copy(other.rotation);
		this.position = Vector.copy(other.position);
		this.motion = Vector.copy(other.motion);
		this.movable = other.movable;
	}
	
	public Optional<Microbe> getLastHitBy()
	{
		return Optional.ofNullable(lastHitBy);
	}
}
