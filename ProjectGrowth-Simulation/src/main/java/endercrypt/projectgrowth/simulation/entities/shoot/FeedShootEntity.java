package endercrypt.projectgrowth.simulation.entities.shoot;


import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.matter.GrowthComposition;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.utility.MicrobeUtility;

import java.awt.Color;
import java.awt.Graphics2D;

import lombok.NonNull;


public class FeedShootEntity extends AbstractShootEntity
{
	private static final long serialVersionUID = -8161360673855411704L;
	
	/**
	 *
	 */
	
	private final GrowthComposition contents;
	
	public FeedShootEntity(Growth owner, @NonNull GrowthComposition contents)
	{
		super(owner);
		this.contents = contents;
		lifetime = 15;
	}
	
	@Override
	protected boolean onCollision(Simulation simulation, CollisionVector<Growth> collision)
	{
		destroy();
		if (collision.getTarget() instanceof Microbe microbe)
		{
			microbe.getInfo().getComposition().addAll(contents);
		}
		return true;
	}
	
	@Override
	public double getRadius()
	{
		return MicrobeUtility.areaToCircleRadius(contents.getSum());
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.setColor(Color.CYAN);
		super.draw(g2d);
	}
}
