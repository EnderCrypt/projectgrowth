package endercrypt.projectgrowth.simulation.entities.food;


import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Color;
import java.awt.Graphics2D;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class Food extends Growth
{
	private static final long serialVersionUID = -1154778180838936916L;
	
	/**
	 *
	 */
	
	public static final Color COLOR = new Color(150, 150, 150);
	
	@Getter
	private final FoodInfo info;
	
	@Override
	public void update(Simulation simulation)
	{
		info.getComposition().remove(Matter.ENERGY, 0.01);
		
		super.update(simulation);
	}
	
	@Override
	public double getRadius()
	{
		return super.getRadius() * 5.0;
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.setColor(COLOR);
		super.draw(g2d);
	}
}
