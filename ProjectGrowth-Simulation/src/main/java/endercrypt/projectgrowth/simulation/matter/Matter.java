package endercrypt.projectgrowth.simulation.matter;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Matter
{
	ENERGY(0.01),
	BODY(1.0),
	CHRLORPLASTS(1.5),
	SHELL(1.75),
	WASTE(2.0),
	POISON(0.25);
	
	public static final double MAX = 32_000;
	
	private final double mass;
}
