package endercrypt.projectgrowth.simulation.matter;


import endercrypt.library.commons.misc.Ender;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map.Entry;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;


public class GrowthComposition implements Serializable
{
	private static final long serialVersionUID = 551438482135734433L;
	
	/**
	 *
	 */
	
	private final EnumMap<Matter, Double> map = new EnumMap<>(Matter.class);
	
	public GrowthComposition()
	{
		for (Matter matter : Matter.values())
		{
			map.put(matter, 0.0);
		}
	}
	
	public double get(Matter matter)
	{
		return map.get(matter);
	}
	
	public void set(Matter matter, double amount)
	{
		if (!Double.isFinite(amount))
		{
			throw new IllegalArgumentException(matter + " amount cannot be " + amount);
		}
		if (amount < 0.001)
		{
			amount = 0;
		}
		map.put(matter, amount);
	}
	
	public boolean lacks(Matter matter)
	{
		return get(matter) == 0.0;
	}
	
	public boolean contains(Matter matter)
	{
		return get(matter) > 0.0;
	}
	
	public boolean contains(Matter matter, double amount)
	{
		return get(matter) >= amount;
	}
	
	public double modify(Matter matter, DoubleUnaryOperator operator)
	{
		double value = get(matter);
		value = operator.applyAsDouble(value);
		set(matter, value);
		return value;
	}
	
	public double add(Matter matter, double amount)
	{
		return modify(matter, (current) -> current + amount);
	}
	
	public RemovedMatter remove(Matter matter, double amount)
	{
		double change = Math.min(get(matter), amount);
		boolean all = change == amount;
		add(matter, -change);
		return new RemovedMatter(all, change);
	}
	
	public void addAll(GrowthComposition composition)
	{
		composition.map.forEach(this::add);
	}
	
	public void setAll(GrowthComposition composition)
	{
		composition.map.forEach(this::set);
	}
	
	public double getSum()
	{
		return map.values().stream()
			.mapToDouble(Double::valueOf)
			.sum();
	}
	
	public double getMass()
	{
		double result = 0.0;
		for (Entry<Matter, Double> entry : map.entrySet())
		{
			Matter matter = entry.getKey();
			double amount = entry.getValue();
			result += matter.getMass() * amount;
		}
		return result;
	}
	
	public GrowthComposition extract(double amount)
	{
		double total = getSum();
		if (amount > total)
		{
			amount = total;
		}
		GrowthComposition result = new GrowthComposition();
		if (total > 0)
		{
			for (Matter matter : Matter.values())
			{
				double value = get(matter);
				double extract = 1.0 / total * value * amount;
				set(matter, value - extract);
				result.set(matter, extract);
			}
		}
		return result;
	}
	
	public void clear()
	{
		map.replaceAll((matter, value) -> 0.0);
	}
	
	@Override
	public String toString()
	{
		return map.entrySet().stream()
			.map(e -> e.getKey() + " = " + Ender.math.round(e.getValue(), 2))
			.collect(Collectors.joining("\n"));
	}
}
