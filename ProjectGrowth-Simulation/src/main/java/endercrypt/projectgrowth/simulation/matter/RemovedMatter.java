package endercrypt.projectgrowth.simulation.matter;

public class RemovedMatter
{
	private final boolean all;
	private final double amount;

	public RemovedMatter(boolean all, double amount)
	{
		this.all = all;
		this.amount = amount;
	}

	public boolean isAll()
	{
		return all;
	}

	public double getAmount()
	{
		return amount;
	}
}
