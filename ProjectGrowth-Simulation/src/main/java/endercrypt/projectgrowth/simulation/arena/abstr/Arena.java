package endercrypt.projectgrowth.simulation.arena.abstr;


import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Graphics2D;


public interface Arena
{
	public void actUpon(Growth growth);

	public void draw(Graphics2D g2d);
}
