package endercrypt.projectgrowth.simulation.arena;


import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.position.Vector;
import endercrypt.library.commons.structs.sign.Sign;
import endercrypt.library.commons.structs.sign.Signs;
import endercrypt.projectgrowth.simulation.arena.abstr.Arena;
import endercrypt.projectgrowth.simulation.entities.entity.EntityInfo;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.function.DoubleBinaryOperator;
import java.util.function.ToDoubleFunction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
public class SquareArena implements Arena
{
	private final double radius;
	private final Shape shape;
	
	public SquareArena(double radius)
	{
		this.radius = radius;
		double diameter = radius * 2;
		this.shape = new Rectangle2D.Double(-radius, -radius, diameter, diameter);
	}
	
	@Override
	public void actUpon(Growth growth)
	{
		EntityInfo info = growth.getInfo();
		Vector position = info.getPosition();
		double microbeRadius = growth.getRadius();
		double trimmedRadius = (radius - microbeRadius);
		Overlapper overlapper = new Overlapper(position, trimmedRadius);
		overlapper.test(v -> v.x, Signs.NEGATIVE, (v, a) -> a - v); // left
		overlapper.test(v -> v.y, Signs.NEGATIVE, (v, a) -> a - v); // up
		overlapper.test(v -> v.x, Signs.POSITIVE, (v, a) -> v - a); // right
		overlapper.test(v -> v.y, Signs.POSITIVE, (v, a) -> v - a); // down
		double overlap = overlapper.getOverlap();
		if (overlap > 0)
		{
			double squareAngle = Math.round(position.getAngle() / Circle.QUARTER) * Circle.QUARTER - Circle.HALF;
			info.getMotion().add(Vector.byAngle(squareAngle, overlap));
		}
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.draw(shape);
	}
	
	@Getter
	@RequiredArgsConstructor
	private static class Overlapper
	{
		private final Vector position;
		private final double trimmedRadius;
		private double overlap = 0;
		
		public void test(ToDoubleFunction<Vector> getPosition, Sign sign, DoubleBinaryOperator overlapCalculator)
		{
			double growthPosition = getPosition.applyAsDouble(position);
			double arenaPosition = sign.asInt() * trimmedRadius;
			double virtualOverlap = overlapCalculator.applyAsDouble(growthPosition, arenaPosition);
			overlap += Math.max(0, virtualOverlap);
		}
	}
}
