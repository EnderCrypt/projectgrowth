package endercrypt.projectgrowth.simulation.arena;


import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.arena.abstr.Arena;
import endercrypt.projectgrowth.simulation.entities.entity.EntityInfo;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import lombok.Getter;


@Getter
public class CircleArena implements Arena
{
	private final double radius;
	private final Shape shape;
	
	public CircleArena(double radius)
	{
		this.radius = radius;
		double diameter = radius * 2;
		this.shape = new Ellipse2D.Double(-radius, -radius, diameter, diameter);
	}
	
	@Override
	public void actUpon(Growth growth)
	{
		EntityInfo info = growth.getInfo();
		Vector position = info.getPosition();
		double microbeRadius = growth.getRadius();
		double overlap = position.getLength() - (radius - microbeRadius);
		if (overlap > 0)
		{
			info.getMotion().add(Vector.byAngle(position.getAngle() - Circle.HALF, overlap));
		}
	}
	
	@Override
	public void draw(Graphics2D g2d)
	{
		g2d.draw(shape);
	}
}
