package endercrypt.projectgrowth.simulation.arena.abstr;


import endercrypt.projectgrowth.common.ProjectGrowthRuntimeException;
import endercrypt.projectgrowth.setting.environment.arena.EnvironmentArena;
import endercrypt.projectgrowth.simulation.arena.CircleArena;
import endercrypt.projectgrowth.simulation.arena.SquareArena;

import java.util.HashMap;
import java.util.Map;


public class ArenaFactory
{
	private static final Map<String, ArenaConstructor> constructors = new HashMap<>();
	static
	{
		constructors.put("circle", CircleArena::new);
		constructors.put("square", SquareArena::new);
	}
	
	public static Arena create(EnvironmentArena arenaConfiguration)
	{
		int size = arenaConfiguration.getSize();
		String arenaStringKey = arenaConfiguration.getType();
		ArenaConstructor constructor = constructors.get(arenaStringKey);
		if (constructor == null)
		{
			throw new ProjectGrowthRuntimeException("Arena configuration does not support type \"" + arenaStringKey + "\", must be one of " + constructors.keySet());
		}
		return constructor.construct(size);
	}
	
	private interface ArenaConstructor
	{
		public Arena construct(double size);
	}
}
