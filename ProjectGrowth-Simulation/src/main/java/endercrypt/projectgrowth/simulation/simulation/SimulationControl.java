package endercrypt.projectgrowth.simulation.simulation;

public class SimulationControl
{
	private final Object lock = new Object();
	
	private boolean tickOnce = false;
	private boolean tickForever = false;
	
	public void step()
	{
		tickOnce = true;
		synchronized (lock)
		{
			lock.notifyAll();
		}
	}
	
	public void play()
	{
		tickForever = true;
		synchronized (lock)
		{
			lock.notifyAll();
		}
	}
	
	public void stop()
	{
		tickForever = false;
	}
	
	public void toggle()
	{
		if (isRunning())
		{
			stop();
		}
		else
		{
			play();
		}
	}
	
	public boolean isRunning()
	{
		return tickForever;
	}
	
	protected boolean tryTick()
	{
		if (tickOnce || tickForever)
		{
			tickOnce = false;
			return true;
		}
		return false;
	}
	
	public void awaitTick() throws InterruptedException
	{
		if (tryTick())
		{
			return;
		}
		synchronized (lock)
		{
			while (!tryTick())
			{
				lock.wait();
			}
		}
	}
}
