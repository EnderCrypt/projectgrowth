package endercrypt.projectgrowth.simulation.simulation;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.mutation.Mutator;
import endercrypt.projectgrowth.setting.Configurations;
import endercrypt.projectgrowth.simulation.arena.abstr.Arena;
import endercrypt.projectgrowth.simulation.collision.CollisionVectorFactory;
import endercrypt.projectgrowth.simulation.component.ComponentCollection;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.food.Food;
import endercrypt.projectgrowth.simulation.entities.food.FoodInfo;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.executor.SimulationExecutor;
import endercrypt.projectgrowth.simulation.mutation.StandardMutator;
import endercrypt.projectgrowth.simulation.tie.Tie;
import endercrypt.projectgrowth.simulation.tie.TieConnector;
import endercrypt.projectgrowth.simulation.utility.MicrobeUtility;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import lombok.Getter;
import lombok.NonNull;


@Getter
public class Simulation
{
	private final SmartLayeredArray<Entity> entities = new SmartLayeredArray<>(Entity.class);
	
	private final ComponentCollection componentCollection = new ComponentCollection();
	private final OperatorCollection operatorCollection = new OperatorCollection();
	
	private final Configurations configurations;
	private final Arena arena;
	private final Mutator mutator;
	
	private long tickCount = 0;
	
	public Simulation(@NonNull Configurations configurations, @NonNull Arena arena)
	{
		this.configurations = configurations;
		this.arena = arena;
		this.mutator = new StandardMutator(configurations.getMutation(), operatorCollection);
	}
	
	public synchronized void tick(SimulationExecutor executor) throws InterruptedException, ExecutionException
	{
		// food
		int foodFrequency = configurations.getEnvironment().getFood().getFrequency();
		if (foodFrequency > 0 && getTickCount() % foodFrequency == 0)
		{
			FoodInfo info = FoodInfo.createDefault();
			Food food = new Food(info);
			double angle = Math.random() * Math.PI * 2;
			double length = Math.random() * 10;
			food.getInfo().getPosition().add(Vector.byAngle(angle, length));
			getEntities().add(food);
		}
		
		synchronized (entities)
		{
			entities.flush();
		}
		
		CollisionVectorFactory.clearCache();
		
		// internal update
		executor.perform(entities, (entity) -> entity.update(this));
		
		// arena collision
		executor.perform(entities, Growth.class, (growth) -> arena.actUpon(growth));
		
		// physics update
		executor.perform(entities, (entity) -> entity.physicsUpdate(this));
		
		// finish
		cleanup();
		tickCount++;
	}
	
	public void draw(Graphics2D g2d)
	{
		draw(g2d, (entity) -> null);
	}
	
	public void draw(Graphics2D g2d, Function<Entity, Color> entityHighlightFunction)
	{
		List<Entity> entities = getEntities().getAll();
		
		// draw highlighting
		for (Entity entity : entities)
		{
			Color color = entityHighlightFunction.apply(entity);
			if (color != null)
			{
				Vector position = entity.getInfo().getPosition();
				double radius = entity.getRadius();
				g2d.setColor(color);
				MicrobeUtility.drawCircle(g2d, position, radius + 10, true);
			}
		}
		
		// draw ties
		Set<Tie> drawnTies = new HashSet<>(entities.size() * 2);
		for (Entity entity : entities)
		{
			if (entity instanceof Growth growth)
			{
				for (TieConnector connector : growth.getTies().getAllConnectors())
				{
					Tie tie = connector.getTie();
					if (drawnTies.contains(tie) == false)
					{
						tie.draw(g2d);
						drawnTies.add(tie);
					}
				}
			}
		}
		
		// draw entities
		for (Entity entity : entities)
		{
			entity.draw(g2d);
		}
		
		g2d.setColor(Color.BLACK);
		arena.draw(g2d);
	}
	
	public void cleanup()
	{
		synchronized (entities)
		{
			// flush
			entities.flush();
			
			// kill off dead microbes
			getEntities()
				.getAllHierarchalLayers()
				.forEach(array -> array
					.removeIf(entity -> !entity.isAlive()));
		}
	}
}
