package endercrypt.projectgrowth.simulation.tie.exception;

public class TieException extends RuntimeException
{
	public TieException(String message)
	{
		super(message);
	}
	
	public TieException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
