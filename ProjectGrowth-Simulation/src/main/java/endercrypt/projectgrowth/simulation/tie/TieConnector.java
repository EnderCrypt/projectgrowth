package endercrypt.projectgrowth.simulation.tie;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.collision.CollisionVectorFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.io.Serializable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class TieConnector implements Serializable
{
	private final Growth parent;
	private final Growth other;
	private final Tie tie;
	
	public void update(Simulation simulation)
	{
		tie.update(simulation);
		
		// pull towards
		CollisionVector<Growth> collision = getCollisionVector();
		double distance = collision.getBodilyDistance();
		double stretch = distance - tie.getPrefferedLength();
		Vector vector = Vector.byAngle(collision.getDirection(), stretch * 0.005);
		parent.getInfo().getMotion().add(vector);
	}
	
	public CollisionVector<Growth> getCollisionVector()
	{
		return CollisionVectorFactory.create(parent, other);
	}
}
