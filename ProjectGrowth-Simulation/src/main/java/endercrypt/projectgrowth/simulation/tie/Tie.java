package endercrypt.projectgrowth.simulation.tie;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.collision.CollisionVectorFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Graphics2D;
import java.io.Serializable;

import lombok.Getter;


@Getter
public class Tie implements Serializable
{
	private final int id;
	private final TieConnector left;
	private final TieConnector right;
	private boolean alive = true;
	
	private long lastTick = 0;
	
	private int lifetime = 0;
	private int prefferedLength = 100;
	
	public Tie(int id, Growth left, Growth right)
	{
		this.id = id;
		this.left = new TieConnector(left, right, this);
		this.right = new TieConnector(right, left, this);
	}
	
	public double getBodilyDistance()
	{
		// we CANNOT return the CollisionVector, as its direction is hardcoded
		// we really only need distance here anyways
		return CollisionVectorFactory.create(left.getParent(), right.getParent())
			.getBodilyDistance();
	}
	
	/**
	 * Ensures update only occurs once
	 */
	public void update(Simulation simulation)
	{
		// ensure update is only called twice
		long currentTick = simulation.getTickCount();
		synchronized (this)
		{
			if (currentTick <= lastTick)
			{
				return;
			}
			lastTick = currentTick;
		}
		
		internalUpdate();
	}
	
	private void internalUpdate()
	{
		lifetime++;
	}
	
	public boolean isHardened()
	{
		return lifetime >= 20;
	}
	
	public void delete()
	{
		if (alive)
		{
			alive = false;
			left.getParent().getTies().delete(left);
			right.getParent().getTies().delete(right);
		}
	}
	
	public void draw(Graphics2D g2d)
	{
		Vector vectorLeft = left.getParent().getInfo().getPosition();
		Vector vectorRight = right.getParent().getInfo().getPosition();
		g2d.drawLine((int) vectorLeft.x, (int) vectorLeft.y, (int) vectorRight.x, (int) vectorRight.y);
	}
	
	@Override
	public String toString()
	{
		return left + " <---> " + right;
	}
}
