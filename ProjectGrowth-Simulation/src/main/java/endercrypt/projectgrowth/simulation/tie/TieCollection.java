package endercrypt.projectgrowth.simulation.tie;


import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class TieCollection implements Serializable
{
	public static final synchronized boolean commit(Tie tie)
	{
		TieConnector left = tie.getLeft();
		Growth leftGrowth = left.getParent();
		TieCollection leftTies = leftGrowth.getTies();
		
		TieConnector right = tie.getRight();
		Growth rightGrowth = right.getParent();
		TieCollection rightTies = rightGrowth.getTies();
		
		if (leftTies.isAcceptable(left) == false || rightTies.isAcceptable(right) == false)
			return false;
		
		// commit
		leftTies.add(left);
		rightTies.add(right);
		return true;
	}
	
	public static final int MAX_CONNECTIONS = 50;
	
	private final Growth parent;
	protected final Map<Integer, TieConnector> byId = new HashMap<>();
	protected final Map<Growth, TieConnector> byGrowth = new HashMap<>();
	private int lastId = 0;
	
	private boolean isAcceptable(TieConnector connector)
	{
		if (connector.getParent() != parent)
			throw new IllegalArgumentException("connector intended for different growth");
		
		if (count() >= MAX_CONNECTIONS)
			return false;
		
		Tie tie = connector.getTie();
		int id = tie.getId();
		if (get(id).isPresent())
			return false;
		
		Growth other = connector.getOther();
		if (get(other).isPresent())
			return false;
		
		return true;
	}
	
	private void add(TieConnector connector)
	{
		Growth parent = connector.getParent();
		Growth other = connector.getOther();
		if (this.parent != parent)
			throw new IllegalArgumentException("connector intended for different growth");
		
		Tie tie = connector.getTie();
		int id = tie.getId();
		
		synchronized (this)
		{
			byId.put(id, connector);
			byGrowth.put(other, connector);
		}
	}
	
	public OptionalInt getLastId()
	{
		if (get(lastId).isEmpty())
			return OptionalInt.empty();
		
		return OptionalInt.of(lastId);
	}
	
	public Optional<TieConnector> get(int id)
	{
		return Optional.ofNullable(byId.get(id));
	}
	
	public Optional<TieConnector> get(Growth other)
	{
		return Optional.ofNullable(byGrowth.get(other));
	}
	
	public int count()
	{
		return byGrowth.size();
	}
	
	public void update(Simulation simulation)
	{
		for (TieConnector connector : byGrowth.values())
		{
			connector.update(simulation);
		}
	}
	
	public boolean isMultibot()
	{
		return byGrowth.values()
			.stream()
			.anyMatch(connector -> connector.getTie().isHardened());
	}
	
	protected void delete(TieConnector connector)
	{
		int id = connector.getTie().getId();
		byId.remove(id, connector);
		
		Growth other = connector.getOther();
		byGrowth.remove(other, connector);
	}
	
	public List<TieConnector> getAllConnectors()
	{
		synchronized (this)
		{
			return List.copyOf(byGrowth.values());
		}
	}
	
	@Override
	public String toString()
	{
		String entries = byGrowth.values()
			.stream()
			.map(TieConnector::getTie)
			.map(Objects::toString)
			.collect(Collectors.joining(", "));
		
		return "[" + entries + "]";
	}
}
