package endercrypt.projectgrowth.simulation.executor;


import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;


public class SimulationExecutor extends ProjectGrowthExecutor<ExecutorService>
{
	public SimulationExecutor(int threadCount)
	{
		super(threadCount);
	}
	
	@Override
	protected String getServiceName()
	{
		return "growth-simulation-%d";
	}
	
	@Override
	protected ExecutorService createService(int threads, ThreadFactory threadFactory)
	{
		return Executors.newFixedThreadPool(threads, threadFactory);
	}
	
	public void perform(SmartLayeredArray<Entity> entities, Task<Entity> task) throws InterruptedException, ExecutionException
	{
		perform(entities, Entity.class, task);
	}
	
	public <T extends Entity> void perform(SmartLayeredArray<Entity> entities, Class<T> targetClass, Task<T> task) throws InterruptedException, ExecutionException
	{
		List<Callable<Void>> calls = entities.stream(targetClass)
			.map(element -> taskToCallable(element, task))
			.collect(Collectors.toList());
		
		for (Future<Void> future : executor.invokeAll(calls))
		{
			future.get();
		}
	}
	
	private static <T extends Entity> Callable<Void> taskToCallable(T element, Task<T> task)
	{
		return new Callable<>()
		{
			@Override
			public Void call() throws Exception
			{
				task.perform(element);
				return null;
			}
		};
	}
}
