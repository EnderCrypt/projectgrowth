package endercrypt.projectgrowth.simulation.executor;


import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;


public abstract class ProjectGrowthExecutor<T extends ExecutorService>
{
	protected final int threadCount;
	protected final T executor;
	
	public ProjectGrowthExecutor(int threadCount)
	{
		this.threadCount = threadCount;
		ThreadFactory threadFactory = new ThreadFactoryBuilder()
			.setNameFormat(getServiceName())
			.build();
		this.executor = createService(threadCount, threadFactory);
		Objects.requireNonNull(this.executor, "executor");
	}
	
	protected abstract String getServiceName();
	
	protected abstract T createService(int threads, ThreadFactory threadFactory);
	
	public void shutdown()
	{
		executor.shutdown();
	}
}
