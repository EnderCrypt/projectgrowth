package endercrypt.projectgrowth.simulation.executor;


import endercrypt.projectgrowth.simulation.entities.entity.Entity;


public interface Task<T extends Entity>
{
	public void perform(T entity);
}
