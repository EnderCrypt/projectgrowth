package endercrypt.projectgrowth.simulation.utility;


import endercrypt.library.commons.position.Vector;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.awt.Graphics2D;


public class MicrobeUtility
{
	private MicrobeUtility()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	// A = π r² (reversed)
	public static double areaToCircleRadius(double area)
	{
		return Math.sqrt(area / Math.PI);
	}
	
	public static void drawCircle(Graphics2D g2d, Vector position, double radius, boolean filled)
	{
		double x = position.x - radius;
		double y = position.y - radius;
		double diameter = radius * 2;
		if (filled)
		{
			g2d.fillOval((int) x, (int) y, (int) diameter, (int) diameter);
		}
		else
		{
			g2d.drawOval((int) x, (int) y, (int) diameter, (int) diameter);
		}
	}
}
