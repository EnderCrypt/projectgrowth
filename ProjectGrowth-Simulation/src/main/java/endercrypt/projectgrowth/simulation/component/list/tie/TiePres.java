package endercrypt.projectgrowth.simulation.component.list.tie;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class TiePres implements ComponentSetup
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int lastId = microbe.getTies()
			.getLastId()
			.orElse(0);
		
		memory.set(Sysvar.tiepres, lastId);
	}
}
