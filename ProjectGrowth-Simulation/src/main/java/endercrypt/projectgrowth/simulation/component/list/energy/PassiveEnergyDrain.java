package endercrypt.projectgrowth.simulation.component.list.energy;


import endercrypt.projectgrowth.dna.genome.activation.GenomeActivation;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class PassiveEnergyDrain implements ComponentCommit
{
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		double cost = 0.0;
		
		// global cost
		cost += 0.1;
		
		// genome execution cost
		cost += microbe.getGenomeActivation().stream()
			.mapToDouble(GenomeActivation::getEnergyCost)
			.sum();
		
		info.getComposition().remove(Matter.ENERGY, cost);
	}
}
