package endercrypt.projectgrowth.simulation.component.steps;


import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.util.ArrayList;
import java.util.List;

import lombok.NonNull;


public abstract class ComponentList<T extends DarwinComponent>
{
	protected final List<T> components = new ArrayList<>();
	
	public final void register(@NonNull T component)
	{
		components.add(component);
	}
	
	public abstract void trigger(Simulation simulation, Microbe microbe);
}
