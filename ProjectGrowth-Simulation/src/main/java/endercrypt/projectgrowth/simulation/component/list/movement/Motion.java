package endercrypt.projectgrowth.simulation.component.list.movement;


import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Motion implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		// thrust
		memory.set(Sysvar.up, 0);
		memory.set(Sysvar.dn, 0);
		memory.set(Sysvar.sx, 0);
		memory.set(Sysvar.dx, 0);
		
		// velocity
		double leftVel = info.getMotion().getLengthFromPerspective(info.getRotation().getRadians() - Circle.QUARTER);
		memory.set(Sysvar.velsx, (int) -leftVel);
		memory.set(Sysvar.veldx, (int) leftVel);
		double upVel = info.getMotion().getLengthFromPerspective(info.getRotation().getRadians());
		memory.set(Sysvar.veldn, (int) -upVel);
		memory.set(Sysvar.velup, (int) upVel);
		memory.set(Sysvar.vel, (int) upVel);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		// thrust
		int up = memory.get(Sysvar.up) - memory.get(Sysvar.dn);
		int right = memory.get(Sysvar.sx) - memory.get(Sysvar.dx);
		
		Vector thrust = new Vector(0, 0);
		thrust.add(Vector.byAngle(info.getRotation().getRadians(), up));
		thrust.add(Vector.byAngle(info.getRotation().getRadians() - Circle.QUARTER, right));
		thrust.multiply(MicrobeInfo.MOVEMENT_SPEED_MULTIPLIER);
		thrust.truncate(MicrobeInfo.MAX_THRUST);
		info.getMotion().add(thrust);
	}
}
