package endercrypt.projectgrowth.simulation.component;


import endercrypt.projectgrowth.common.modules.ProjectGrowthModuleInfo;
import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;
import endercrypt.projectgrowth.simulation.component.steps.ComponentList;
import endercrypt.projectgrowth.simulation.component.steps.DarwinComponent;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentListCommit;
import endercrypt.projectgrowth.simulation.component.steps.excess.ComponentExcess;
import endercrypt.projectgrowth.simulation.component.steps.excess.ComponentListExcess;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentListSetup;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.lang.reflect.Constructor;

import org.reflections.Reflections;


public class ComponentCollection
{
	private final ComponentListSetup setupComponents = new ComponentListSetup();
	private final ComponentListCommit commitComponents = new ComponentListCommit();
	private final ComponentListExcess excessComponents = new ComponentListExcess();
	
	public ComponentCollection()
	{
		collect(ComponentSetup.class, setupComponents);
		collect(ComponentCommit.class, commitComponents);
		collect(ComponentExcess.class, excessComponents);
	}
	
	private <T extends DarwinComponent> void collect(Class<T> targetClass, ComponentList<T> list)
	{
		try
		{
			Reflections reflections = new Reflections(ProjectGrowthModuleInfo.simulation.getPackageString() + ".component.list");
			for (Class<? extends T> componentClass : reflections.getSubTypesOf(targetClass))
			{
				Constructor<? extends T> constructor = componentClass.getConstructor();
				T instance = constructor.newInstance();
				list.register(instance);
			}
		}
		catch (ReflectiveOperationException e)
		{
			throw new DarwinCompileException("Failed to reflectively retrieve components of type " + targetClass, e);
		}
	}
	
	public void microbeUpdate(Simulation simulation, Microbe microbe)
	{
		setupComponents.trigger(simulation, microbe);
	}
	
	public void microbeCommit(Simulation simulation, Microbe microbe)
	{
		commitComponents.trigger(simulation, microbe);
		excessComponents.trigger(simulation, microbe);
	}
}
