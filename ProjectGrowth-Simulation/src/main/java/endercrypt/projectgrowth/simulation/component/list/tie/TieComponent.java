package endercrypt.projectgrowth.simulation.component.list.tie;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.tie.Tie;
import endercrypt.projectgrowth.simulation.tie.TieCollection;


public class TieComponent implements ComponentCommit
{
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int tieId = memory.get(Sysvar.tie);
		if (tieId == 0)
			return;
		
		Growth target = microbe.getEyesOutput().getTarget().orElse(null);
		if (target == null)
			return;
		
		Tie tie = new Tie(tieId, microbe, target);
		TieCollection.commit(tie);
	}
}
