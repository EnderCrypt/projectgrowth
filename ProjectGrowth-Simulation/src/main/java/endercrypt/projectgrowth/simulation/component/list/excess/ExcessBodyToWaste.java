package endercrypt.projectgrowth.simulation.component.list.excess;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.component.steps.excess.ComponentExcess;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class ExcessBodyToWaste implements ComponentExcess
{
	public static final double MAX = 32_000;
	
	@Override
	public boolean excess(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		double excess = info.getComposition().get(Matter.BODY) - MAX;
		
		if (excess > 0)
		{
			info.getComposition().remove(Matter.BODY, excess);
			info.getComposition().add(Matter.WASTE, excess / 10.0);
			return false;
		}
		return true;
	}
}
