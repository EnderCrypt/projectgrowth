package endercrypt.projectgrowth.simulation.component.list.defense;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class MakeShell implements ComponentSetup, ComponentCommit
{
	private static final double CONVERSION_RATIO = 0.1;
	
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		double shellCount = info.getComposition().get(Matter.SHELL);
		memory.set(Sysvar.shell, (int) shellCount);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int requested = memory.get(Sysvar.mkshell);
		int shellChange = (int) Ender.math.clamp(requested, -100, 100);
		createShell(info, shellChange);
	}
	
	public static void createShell(MicrobeInfo info, int shell)
	{
		info.getComposition().add(Matter.SHELL, shell);
		info.getComposition().remove(Matter.ENERGY, shell * CONVERSION_RATIO);
	}
}
