package endercrypt.projectgrowth.simulation.component.list.excess;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Alzheimer implements ComponentSetup
{
	private static final int THRESHOLD = 50;
	
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		double waste = info.getComposition().get(Matter.WASTE);
		while (waste > THRESHOLD)
		{
			waste -= THRESHOLD;
			int address = (int) (Math.random() * Memory.MEMORY_LOCATIONS);
			int value = Memory.randomDarwinNumber();
			memory.set(address, value);
		}
	}
}
