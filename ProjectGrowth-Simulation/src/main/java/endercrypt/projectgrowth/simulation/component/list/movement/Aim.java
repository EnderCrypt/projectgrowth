package endercrypt.projectgrowth.simulation.component.list.movement;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Aim implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.aimdx, 0);
		memory.set(Sysvar.aimright, 0);
		
		memory.set(Sysvar.aimsx, 0);
		memory.set(Sysvar.aimleft, 0);
		
		int aim = info.getRotation().getDarwinInt();
		memory.set(Sysvar.aim, aim);
		memory.set(Sysvar.setaim, aim);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int aim = memory.get(Sysvar.setaim);
		int left = memory.get(Sysvar.aimsx) + memory.get(Sysvar.aimleft);
		int right = memory.get(Sysvar.aimdx) + memory.get(Sysvar.aimright);
		aim += left - right;
		info.getRotation().setDarwin(aim);
	}
}
