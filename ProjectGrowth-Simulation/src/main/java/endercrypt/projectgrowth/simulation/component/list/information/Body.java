package endercrypt.projectgrowth.simulation.component.list.information;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Body implements ComponentSetup
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.body, (int) info.getComposition().get(Matter.BODY));
		
		MicrobeInfo oldInfo = microbe.getOldInfo();
		int bodgain = (int) Math.round(info.getComposition().get(Matter.BODY) - oldInfo.getComposition().get(Matter.BODY));
		memory.set(Sysvar.bodgain, bodgain);
		memory.set(Sysvar.bodloss, -bodgain);
	}
}
