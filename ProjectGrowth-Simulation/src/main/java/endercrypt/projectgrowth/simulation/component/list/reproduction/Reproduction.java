package endercrypt.projectgrowth.simulation.component.list.reproduction;


import endercrypt.library.commons.position.Circle;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.mutation.Mutator;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.GrowthComposition;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Reproduction implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.repro, 0);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int reproPercentage = memory.get(Sysvar.repro);
		
		Genome genome = microbe.getGenome();
		Mutator mutator = simulation.getMutator();
		if (mutator.isEnabled())
		{
			genome = mutator.mutate(genome);
		}
		
		spawnChildWithGenome(simulation, microbe, genome, reproPercentage);
	}
	
	public static boolean spawnChildWithGenome(Simulation simulation, Microbe parent, Genome childGenome, int reproPercentage)
	{
		if (reproPercentage <= 0 || reproPercentage > 100)
		{
			return false;
		}
		
		GrowthComposition parentComposition = parent.getInfo().getComposition();
		double matterExtractionAmount = parentComposition.getSum() * (reproPercentage / 100.0);
		GrowthComposition childComposition = parentComposition.extract(matterExtractionAmount);
		
		double direction = parent.getInfo().getRotation().getRadians() - Circle.HALF;
		
		MicrobeInfo childInfo = new MicrobeInfo();
		childInfo.getComposition().setAll(childComposition);
		childInfo.getRotation().setRadians(direction);
		
		Microbe child = new Microbe(childInfo, childGenome);
		parent.spawn(child, direction, 0);
		simulation.getEntities().add(child);
		assert child.isAlive();
		
		return true;
	}
}
