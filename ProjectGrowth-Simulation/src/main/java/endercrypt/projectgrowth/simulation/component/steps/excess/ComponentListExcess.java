package endercrypt.projectgrowth.simulation.component.steps.excess;


import endercrypt.projectgrowth.simulation.component.steps.ComponentList;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class ComponentListExcess extends ComponentList<ComponentExcess>
{
	@Override
	public void trigger(Simulation simulation, Microbe microbe)
	{
		boolean excess = true;
		while (excess)
		{
			excess = false;
			for (ComponentExcess component : components)
			{
				if (component.excess(simulation, microbe, microbe.getInfo(), microbe.getMemory()) == false)
				{
					excess = true;
				}
			}
		}
	}
}
