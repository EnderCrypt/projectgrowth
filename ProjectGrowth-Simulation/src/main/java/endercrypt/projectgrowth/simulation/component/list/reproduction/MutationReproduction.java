package endercrypt.projectgrowth.simulation.component.list.reproduction;


import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.mutation.Mutator;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class MutationReproduction implements ComponentSetup, ComponentCommit
{
	public static final int MUTATION_MULTIPLIER = 10;
	
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.mrepro, 0);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int reproPercentage = memory.get(Sysvar.mrepro);
		
		Genome genome = microbe.getGenome();
		Mutator mutator = simulation.getMutator();
		for (int i = 0; i < MUTATION_MULTIPLIER; i++)
		{
			genome = mutator.mutate(genome);
		}
		
		Reproduction.spawnChildWithGenome(simulation, microbe, genome, reproPercentage);
	}
}
