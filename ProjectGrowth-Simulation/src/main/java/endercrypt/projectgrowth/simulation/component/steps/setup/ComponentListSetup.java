package endercrypt.projectgrowth.simulation.component.steps.setup;


import endercrypt.projectgrowth.simulation.component.steps.ComponentList;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class ComponentListSetup extends ComponentList<ComponentSetup>
{
	@Override
	public void trigger(Simulation simulation, Microbe microbe)
	{
		for (ComponentSetup component : components)
		{
			component.setup(simulation, microbe, microbe.getInfo(), microbe.getMemory());
		}
	}
}
