package endercrypt.projectgrowth.simulation.component.list.attack;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.entities.shoot.AbstractShootEntity;
import endercrypt.projectgrowth.simulation.entities.shoot.AttackShootEntity;
import endercrypt.projectgrowth.simulation.entities.shoot.MemoryShootEntity;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Shoot implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.shoot, 0);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		AbstractShootEntity shoot = create(microbe);
		if (shoot != null)
		{
			simulation.getEntities().add(shoot);
		}
	}
	
	private static AbstractShootEntity create(Microbe microbe)
	{
		int shoot = microbe.getMemory().get(Sysvar.shoot);
		
		if (shoot > 0)
		{
			return shootMemory(microbe);
		}
		
		switch (shoot)
		{
		case -1:
			return shootHunting(microbe);
		}
		
		return null;
	}
	
	private static AttackShootEntity shootHunting(Microbe microbe)
	{
		double body = microbe.getInfo().getComposition().get(Matter.BODY);
		double payload = Math.sqrt(body);
		double drain = payload * 0.02;
		microbe.getInfo().getComposition().remove(Matter.ENERGY, drain);
		return microbe.shootParticle(new AttackShootEntity(microbe, payload), 5);
	}
	
	private static MemoryShootEntity shootMemory(Microbe microbe)
	{
		int location = microbe.getMemory().get(Sysvar.shoot);
		int value = microbe.getMemory().get(Sysvar.shootval);
		return microbe.shootParticle(new MemoryShootEntity(microbe, location, value), 5);
	}
}
