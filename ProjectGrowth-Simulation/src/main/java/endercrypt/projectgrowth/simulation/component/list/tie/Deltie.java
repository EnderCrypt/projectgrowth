package endercrypt.projectgrowth.simulation.component.list.tie;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.tie.TieConnector;


public class Deltie implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.deltie, 0);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int id = memory.get(Sysvar.deltie);
		if (id == 0)
			return;
		
		TieConnector connector = microbe.getTies().get(id).orElse(null);
		if (connector == null)
			return;
		
		connector.getTie().delete();
	}
}
