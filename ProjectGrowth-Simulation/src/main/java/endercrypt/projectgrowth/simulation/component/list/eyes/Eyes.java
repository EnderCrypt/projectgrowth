package endercrypt.projectgrowth.simulation.component.list.eyes;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.eye.Eye;
import endercrypt.projectgrowth.simulation.eye.EyeOutput;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Eyes implements ComponentSetup
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		for (EyeOutput eyeOutput : microbe.getEyesOutput())
		{
			Eye eye = eyeOutput.getEye();
			Sysvar sysvar = eye.getSysvar();
			int inverseStrength = eyeOutput.getInverseStrength();
			memory.set(sysvar, inverseStrength);
		}
	}
}
