package endercrypt.projectgrowth.simulation.component.steps.setup;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.component.steps.DarwinComponent;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public interface ComponentSetup extends DarwinComponent
{
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory);
}
