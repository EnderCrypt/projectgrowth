package endercrypt.projectgrowth.simulation.component.list.ref;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.util.function.Function;


public class RefPosition implements ComponentSetup
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		set(microbe, Sysvar.refxpos, (p) -> p.x);
		set(microbe, Sysvar.refypos, (p) -> p.y);
	}
	
	private void set(Microbe microbe, Sysvar sysvar, Function<Vector, Double> positionSupplier)
	{
		Vector vector = microbe.getEyesOutput().getTarget()
			.map(m -> m.getInfo().getPosition())
			.orElse(new Vector(0, 0));
		
		int value = (int) Math.round(positionSupplier.apply(vector));
		microbe.getMemory().set(sysvar, value);
	}
}
