package endercrypt.projectgrowth.simulation.component.list.information;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class Energy implements ComponentSetup
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int energy = (int) Math.ceil(info.getComposition().get(Matter.ENERGY));
		memory.set(Sysvar.nrg, energy);
	}
}
