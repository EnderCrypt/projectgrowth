package endercrypt.projectgrowth.simulation.component.steps.commit;


import endercrypt.projectgrowth.simulation.component.steps.ComponentList;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class ComponentListCommit extends ComponentList<ComponentCommit>
{
	@Override
	public void trigger(Simulation simulation, Microbe microbe)
	{
		for (ComponentCommit component : components)
		{
			component.commit(simulation, microbe, microbe.getInfo(), microbe.getMemory());
		}
	}
}
