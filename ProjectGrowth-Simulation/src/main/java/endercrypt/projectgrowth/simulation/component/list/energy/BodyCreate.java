package endercrypt.projectgrowth.simulation.component.list.energy;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class BodyCreate implements ComponentSetup, ComponentCommit
{
	private static final double CONVERSION_RATIO = 10;
	
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.fdbody, 0);
		memory.set(Sysvar.strbody, 0);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		int requested = (memory.get(Sysvar.strbody) - memory.get(Sysvar.fdbody));
		int bodyChange = (int) Ender.math.clamp(requested, -100, 100);
		convertEnergyToBody(info, bodyChange);
	}
	
	public static void convertEnergyToBody(MicrobeInfo info, int energy)
	{
		createBody(info, energy / CONVERSION_RATIO);
	}
	
	public static void createBody(MicrobeInfo info, double body)
	{
		info.getComposition().add(Matter.BODY, body);
		info.getComposition().remove(Matter.ENERGY, body * CONVERSION_RATIO); // warning: does not check for over-conversion
	}
}
