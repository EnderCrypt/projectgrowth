package endercrypt.projectgrowth.simulation.component.list.energy;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class EnergyShock implements ComponentCommit
{
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		MicrobeInfo oldInfo = microbe.getOldInfo();
		if (info.getComposition().get(Matter.ENERGY) < oldInfo.getComposition().get(Matter.ENERGY) / 2)
		{
			microbe.destroy();
		}
	}
}
