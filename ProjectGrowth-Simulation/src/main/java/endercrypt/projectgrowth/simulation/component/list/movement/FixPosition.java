package endercrypt.projectgrowth.simulation.component.list.movement;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.component.steps.commit.ComponentCommit;
import endercrypt.projectgrowth.simulation.component.steps.setup.ComponentSetup;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;


public class FixPosition implements ComponentSetup, ComponentCommit
{
	@Override
	public void setup(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		memory.set(Sysvar.fixed, info.isMovable() ? 0 : 1);
	}
	
	@Override
	public void commit(Simulation simulation, Microbe microbe, MicrobeInfo info, Memory memory)
	{
		boolean movable = memory.get(Sysvar.fixpos) == 0;
		info.setMovable(movable);
	}
}
