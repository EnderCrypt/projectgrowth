package endercrypt.projectgrowth.simulation.collision;


import endercrypt.projectgrowth.simulation.entities.entity.Entity;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class CollisionVectorFactory
{
	private static final Map<CacheKey, CollisionVector<Entity>> vectors = new ConcurrentHashMap<>();
	
	public static <T extends Entity> CollisionVector<T> create(Entity source, T target)
	{
		CacheKey key = new CacheKey(source, target);
		CollisionVector<Entity> result = vectors.get(key);
		if (result != null)
		{
			return (CollisionVector<T>) result;
		}
		
		result = CollisionVector.calculate(source, target);
		vectors.put(key, result);
		return (CollisionVector<T>) result;
	}
	
	public static void clearCache()
	{
		synchronized (vectors)
		{
			vectors.clear();
		}
	}
	
}
