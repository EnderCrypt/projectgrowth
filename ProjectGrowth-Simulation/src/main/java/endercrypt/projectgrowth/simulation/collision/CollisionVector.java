package endercrypt.projectgrowth.simulation.collision;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.entity.EntityInfo;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.util.stream.Stream;

import lombok.Getter;
import lombok.NonNull;


@Getter
public class CollisionVector<T>
{
	protected static <T extends Entity> CollisionVector<T> calculate(Entity source, T target)
	{
		if (source.equals(target))
		{
			throw new IllegalArgumentException("source and target cannot be the same");
		}
		EntityInfo sourceInfo = source.getInfo();
		EntityInfo targetInfo = target.getInfo();
		
		Vector sourceVector = sourceInfo.getPosition();
		Vector targetVector = targetInfo.getPosition();
		
		double distance = sourceVector.getLength(targetVector);
		double direction = sourceVector.getAngle(targetVector);
		
		double sourceRadius = source.getRadius();
		double targetRadius = target.getRadius();
		
		double overlap = (sourceRadius + targetRadius) - distance;
		return new CollisionVector<>(target, distance, direction, overlap);
	}
	
	public static Stream<CollisionVector<Growth>> getGrowthCollisions(Simulation simulation, Entity source)
	{
		return simulation.getEntities().stream(Growth.class)
			.filter(target -> target != source)
			.filter(Entity::isAlive)
			.map(target -> calculate(source, target))
			.filter(CollisionVector::isTouching);
	}
	
	private final T target;
	private final double distance;
	private final double direction;
	private final double overlap;
	
	protected CollisionVector(@NonNull T target, double distance, double direction, double overlap)
	{
		this.target = target;
		this.distance = distance;
		this.direction = direction;
		this.overlap = overlap;
	}
	
	public double getBodilyDistance()
	{
		return -overlap;
	}
	
	public boolean isTouching()
	{
		return getOverlap() >= 0;
	}
	
	@Override
	public String toString()
	{
		return "CollisionVector [target=" + target + ", distance=" + distance + ", direction=" + direction + "]";
	}
}
