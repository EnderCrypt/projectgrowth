package endercrypt.projectgrowth.simulation.collision;


import endercrypt.projectgrowth.simulation.entities.entity.Entity;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@EqualsAndHashCode
class CacheKey
{
	private final Entity left;
	private final Entity right;
}
