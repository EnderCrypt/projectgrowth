package endercrypt.projectgrowth.simulation.eye;


import endercrypt.projectgrowth.common.geometry.Rotation;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;


public class Eyes implements Iterable<Eye>, Serializable
{
	private static final long serialVersionUID = 2561097055148263669L;
	
	/**
	 *
	 */
	
	public static final int CENTER = 5;
	public static final int COUNT = (CENTER * 2) - 1;
	
	public static final Color colorOutline = new Color(0, 0, 0, 20);
	public static final Color colorPerceiving = new Color(0, 0, 0, 255);
	
	private final List<Eye> eyes = new ArrayList<>();
	
	public Eyes()
	{
		for (int i = 0; i < COUNT; i++)
		{
			int eyeNumber = i + 1;
			double eyeDirection = (eyeNumber - CENTER);
			Rotation rotation = Rotation.fromRadians(eyeDirection * Eye.OFFSET);
			Eye eye = new Eye(eyeNumber, rotation);
			eyes.add(eye);
		}
	}
	
	public Eye getCenter()
	{
		return get(CENTER - 1);
	}
	
	public Eye get(Sysvar sysvar)
	{
		return get(sysvar.getAddress() - Sysvar.eye1.getAddress());
	}
	
	public Eye get(int index)
	{
		if (index < 0 || index >= eyes.size())
		{
			throw new IllegalArgumentException("invalid eye: " + index);
		}
		return eyes.get(index);
	}
	
	public Stream<Eye> stream()
	{
		return eyes.stream();
	}
	
	@Override
	public Iterator<Eye> iterator()
	{
		return stream().iterator();
	}
}
