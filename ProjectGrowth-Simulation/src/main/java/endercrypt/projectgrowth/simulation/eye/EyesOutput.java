package endercrypt.projectgrowth.simulation.eye;


import endercrypt.library.commons.iterator.ImmutableIterator;
import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class EyesOutput implements Iterable<EyeOutput>, Serializable
{
	public static EyesOutput perceive(Simulation simulation, Microbe microbe)
	{
		List<EyeOutput> outputs = microbe.getInfo()
			.getEyes()
			.stream()
			.map(eye -> EyeOutput.perceive(simulation, microbe, eye))
			.toList();
		
		return new EyesOutput(outputs);
	}
	
	private final List<EyeOutput> outputs;
	
	public Optional<Growth> getTarget()
	{
		EyeOutput centerEye = outputs.get(Eyes.CENTER - 1);
		return centerEye.getTarget();
	}
	
	public void draw(Microbe microbe, Graphics2D g2d)
	{
		MicrobeInfo info = microbe.getInfo();
		double radius = microbe.getRadius();
		for (EyeOutput eyeOutput : this)
		{
			g2d.setColor(Eyes.colorOutline);
			
			Eye eye = eyeOutput.getEye();
			double direction = eye.getVisionDirection(microbe);
			double eyeWidthHalf = (eye.getWidth() / 2.0);
			
			double viewDistance = eye.getVisionRange(microbe);
			
			double directionLeft = direction - eyeWidthHalf;
			Vector vectorLeftStart = Vector.copy(info.getPosition());
			vectorLeftStart.add(Vector.byAngle(directionLeft, radius));
			Vector vectorLeftStop = Vector.copy(vectorLeftStart);
			vectorLeftStop.add(Vector.byAngle(directionLeft, viewDistance));
			g2d.drawLine((int) vectorLeftStart.x, (int) vectorLeftStart.y, (int) vectorLeftStop.x, (int) vectorLeftStop.y);
			
			double directionRight = direction + eyeWidthHalf;
			Vector vectorRightStart = Vector.copy(info.getPosition());
			vectorRightStart.add(Vector.byAngle(directionRight, radius));
			Vector vectorRightStop = Vector.copy(vectorRightStart);
			vectorRightStop.add(Vector.byAngle(directionRight, viewDistance));
			g2d.drawLine((int) vectorRightStart.x, (int) vectorRightStart.y, (int) vectorRightStop.x, (int) vectorRightStop.y);
			
			g2d.drawLine((int) vectorLeftStop.x, (int) vectorLeftStop.y, (int) vectorRightStop.x, (int) vectorRightStop.y);
			
			// perception
			if (eyeOutput.isPerceiving())
			{
				g2d.setColor(Eyes.colorPerceiving);
				Vector perceiveLeft = Vector.copy(vectorLeftStart);
				perceiveLeft.add(Vector.byAngle(directionLeft, viewDistance * eyeOutput.getStrength()));
				
				Vector perceiveRight = Vector.copy(vectorRightStart);
				perceiveRight.add(Vector.byAngle(directionRight, viewDistance * eyeOutput.getStrength()));
				
				g2d.drawLine((int) perceiveLeft.x, (int) perceiveLeft.y, (int) perceiveRight.x, (int) perceiveRight.y);
			}
		}
	}
	
	@Override
	public Iterator<EyeOutput> iterator()
	{
		return new ImmutableIterator<>(outputs.iterator());
	}
}
