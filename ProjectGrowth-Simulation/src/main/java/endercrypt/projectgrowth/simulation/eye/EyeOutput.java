package endercrypt.projectgrowth.simulation.eye;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.simulation.collision.CollisionVector;
import endercrypt.projectgrowth.simulation.collision.CollisionVectorFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.io.Serializable;
import java.util.Optional;

import lombok.Getter;


@Getter
public class EyeOutput implements Serializable
{
	public static EyeOutput perceive(Simulation simulation, Microbe microbe, Eye eye)
	{
		double minimumVisibleBodySize = microbe.getRadius() * Eye.MINIMUM_PERCEIVABLE_SIZE;
		
		double eyeRange = eye.getVisionRange(microbe);
		double eyeDirection = eye.getVisionDirection(microbe);
		
		Growth[] growths = simulation
			.getEntities()
			.stream(Growth.class)
			.filter(g -> g.getRadius() >= minimumVisibleBodySize)
			.filter(g -> g != microbe) // not yourself
			.toArray(Growth[]::new);
		
		double strength = 1.0;
		Growth target = null;
		
		for (Growth growth : growths)
		{
			CollisionVector<Growth> vector = CollisionVectorFactory.create(microbe, growth);
			double perceive = 1.0 / eyeRange * vector.getBodilyDistance();
			if (perceive < 0)
			{
				perceive = 0;
			}
			if (perceive <= 1.0 && perceive < strength)
			{
				double growthDirection = vector.getDirection();
				double growthAngleOffset = Math.abs(Ender.math.angleDifference(eyeDirection, growthDirection));
				double maxWidth = (eye.getWidth() / 2) + (Math.atan2(growth.getRadius(), vector.getBodilyDistance()) / 3);
				if (growthAngleOffset < maxWidth)
				{
					strength = perceive;
					target = growth;
				}
			}
		}
		
		return new EyeOutput(eye, strength, target);
	}
	
	private final Eye eye;
	private final double strength;
	private final Growth target;
	
	public EyeOutput(Eye eye, double strength, Growth target)
	{
		if (strength < 0 || strength > 1.0)
		{
			throw new IllegalArgumentException("perceiving strength cant be " + strength);
		}
		
		this.eye = eye;
		this.strength = strength;
		this.target = target;
	}
	
	public Optional<Growth> getTarget()
	{
		return Optional.ofNullable(target);
	}
	
	public boolean isPerceiving()
	{
		return target != null;
	}
	
	public int getInverseStrength()
	{
		if (isPerceiving() == false)
		{
			return 0;
		}
		return (int) Math.ceil(Ender.math.fastInverseSqrt(getStrength() * 100) * 32_000);
	}
}
