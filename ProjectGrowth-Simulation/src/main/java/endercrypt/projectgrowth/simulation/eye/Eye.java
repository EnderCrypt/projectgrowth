package endercrypt.projectgrowth.simulation.eye;


import endercrypt.projectgrowth.common.geometry.Rotation;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
public class Eye implements Serializable
{
	private static final long serialVersionUID = 7340333144767065412L;
	
	/**
	 *
	 */
	
	public static final double MINIMUM_PERCEIVABLE_SIZE = 0.05; // 5% of your own size
	public static final double OFFSET = Math.PI / 18;
	
	private final int number;
	
	private final Rotation defaultDirection;
	private Rotation directionOffset;
	
	@Setter
	private double widthOffset;
	
	private final Sysvar sysvar;
	
	public Eye(int number, Rotation defaultDirection)
	{
		this.number = number;
		
		this.defaultDirection = defaultDirection;
		this.directionOffset = Rotation.fromRadians(0.0);
		
		this.widthOffset = 0;
		
		int sysvarIndex = Sysvar.eye1.getAddress() - 1 + getNumber();
		this.sysvar = Sysvar.lookup(sysvarIndex).stream().findFirst().orElseThrow();
	}
	
	public String getName()
	{
		return "eye" + getNumber();
	}
	
	// direction offset //
	
	public double getVisionDirection(Microbe microbe)
	{
		return (microbe.getInfo().getRotation().getRadians() + defaultDirection.getRadians() + directionOffset.getRadians());
	}
	
	// width //
	
	public double getWidth()
	{
		return OFFSET + widthOffset;
	}
	
	// vision //
	
	public double getVisionRange(Microbe microbe)
	{
		return microbe.getRadius() * (1.25 / getWidth());
	}
	
	@Override
	public String toString()
	{
		return "Eye [eyeNumber=" + number + ", directionOffset=" + directionOffset + ", widthOffset=" + widthOffset + "]";
	}
}
