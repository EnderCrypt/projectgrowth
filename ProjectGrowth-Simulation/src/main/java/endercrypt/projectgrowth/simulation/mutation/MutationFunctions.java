package endercrypt.projectgrowth.simulation.mutation;


import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.List;
import java.util.function.Supplier;


public class MutationFunctions
{
	private MutationFunctions()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	private static boolean roll(double chance)
	{
		return Math.random() < (chance * 0.01);
	}
	
	public static <T> void delete(List<T> list, double chance)
	{
		list.removeIf(e -> roll(chance));
	}
	
	public static <T> void create(List<T> list, double chance, Supplier<T> factory)
	{
		for (int i = list.size(); i >= 0; i--)
		{
			if (roll(chance))
			{
				list.add(i, factory.get());
			}
		}
	}
	
	public static <T> void duplicate(List<T> list, double chance)
	{
		for (int i = list.size() - 1; i >= 0; i--)
		{
			if (roll(chance))
			{
				list.add(i, list.get(i));
			}
		}
	}
	
	public static <T> void swap(List<T> list, double chance)
	{
		for (int i = list.size() - 2; i >= 0; i--)
		{
			if (roll(chance))
			{
				list.add(i, list.remove(i + 1));
			}
		}
	}
}
