package endercrypt.projectgrowth.simulation.mutation;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.genome.Sequence;
import endercrypt.projectgrowth.dna.mutation.AbstractMutator;
import endercrypt.projectgrowth.dna.mutation.factory.DnaRandomizerFactory;
import endercrypt.projectgrowth.setting.mutation.MutationConfiguration;
import endercrypt.projectgrowth.setting.mutation.MutationDna;
import endercrypt.projectgrowth.setting.mutation.MutationGene;

import java.util.List;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class StandardMutator extends AbstractMutator
{
	private final MutationConfiguration mutationConfiguration;
	private final DnaRandomizerFactory dnaRandomizerFactory;
	
	public StandardMutator(MutationConfiguration mutationConfiguration, OperatorCollection operatorCollection)
	{
		this.mutationConfiguration = mutationConfiguration;
		dnaRandomizerFactory = new DnaRandomizerFactory(operatorCollection);
	}
	
	@Override
	public boolean isEnabled()
	{
		return mutationConfiguration.isEnabled();
	}
	
	@Override
	public Genome performMutation(Genome genome)
	{
		List<Gene> genes = genome.asList();
		
		MutationGene settings = mutationConfiguration.getGene();
		MutationFunctions.delete(genes, settings.getDelete());
		MutationFunctions.duplicate(genes, settings.getDuplicate());
		MutationFunctions.swap(genes, settings.getSwap());
		
		genes.replaceAll(this::mutate);
		
		return new Genome(genes);
	}
	
	@Override
	public Sequence performMutation(Sequence sequence)
	{
		List<Dna> dnas = sequence.asList();
		
		MutationDna settings = mutationConfiguration.getDna();
		MutationFunctions.delete(dnas, settings.getDelete());
		MutationFunctions.create(dnas, settings.getCreate(), dnaRandomizerFactory::createDna);
		MutationFunctions.duplicate(dnas, settings.getDuplicate());
		MutationFunctions.swap(dnas, settings.getSwap());
		
		return new Sequence(dnas);
	}
}
