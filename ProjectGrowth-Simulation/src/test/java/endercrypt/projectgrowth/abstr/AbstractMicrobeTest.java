package endercrypt.projectgrowth.abstr;


import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.dna.compiler.parse.DarwinCompiler;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;

import org.junit.jupiter.api.BeforeEach;


public class AbstractMicrobeTest extends AbstractSimulationTest
{
	private DarwinCompiler compiler;
	
	@BeforeEach
	private void beforeAll()
	{
		compiler = new DarwinCompiler(simulation.getOperatorCollection());
	}
	
	protected Microbe createMicrobe(String dna)
	{
		MicrobeInfo info = MicrobeInfo.createDefault();
		Genome genome = compiler.compile(dna);
		return createMicrobe(info, genome);
	}
	
	protected Microbe createMicrobe(MicrobeInfo info, Genome genome)
	{
		Microbe microbe = new Microbe(info, genome);
		
		SmartLayeredArray<Entity> entities = simulation.getEntities();
		entities.add(microbe);
		entities.flush();
		
		return microbe;
	}
}
