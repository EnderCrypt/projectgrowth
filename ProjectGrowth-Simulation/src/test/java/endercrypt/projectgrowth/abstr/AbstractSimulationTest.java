package endercrypt.projectgrowth.abstr;


import endercrypt.library.commons.SneakyThrow;
import endercrypt.library.commons.reject.ThrowRejectDueTo;
import endercrypt.library.groundwork.information.RawInformation;
import endercrypt.projectgrowth.setting.Configurations;
import endercrypt.projectgrowth.setting.autosave.AutosaveConfiguration;
import endercrypt.projectgrowth.setting.environment.EnvironmentConfiguration;
import endercrypt.projectgrowth.setting.environment.arena.EnvironmentArena;
import endercrypt.projectgrowth.setting.mutation.MutationConfiguration;
import endercrypt.projectgrowth.setting.performance.PerformanceConfiguration;
import endercrypt.projectgrowth.simulation.arena.abstr.Arena;
import endercrypt.projectgrowth.simulation.arena.abstr.ArenaFactory;
import endercrypt.projectgrowth.simulation.executor.SimulationExecutor;
import endercrypt.projectgrowth.simulation.simulation.Simulation;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.BeforeEach;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;

import lombok.Getter;


public class AbstractSimulationTest
{
	private static final Yaml yaml = new Yaml();
	static
	{
		yaml.setBeanAccess(BeanAccess.FIELD);
	}
	
	private static final SimulationExecutor executor = new SimulationExecutor(1);
	
	protected Simulation simulation;
	
	public final BufferedImage createScreenshot(int size)
	{
		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = image.createGraphics();
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, size, size);
		
		// camera
		AffineTransform transform = new AffineTransform();
		transform.translate(size / 2, size / 2);
		transform.scale(1, 1);
		g2d.transform(transform);
		
		// world
		simulation.draw(g2d);
		
		// cross
		g2d.setColor(Color.BLACK);
		g2d.drawLine(-32, 0, 32, 0);
		g2d.drawLine(0, -32, 0, 32);
		
		g2d.dispose();
		return image;
	}
	
	public final void saveDebugScreenshot(int size, String path)
	{
		BufferedImage image = createScreenshot(size);
		try
		{
			ImageIO.write(image, "PNG", new File(path));
		}
		catch (IOException e)
		{ // since this is a debug method, we do not expect to need to catch IOException
			throw new UncheckedIOException(e);
		}
	}
	
	@BeforeEach
	private void beforeEach()
	{
		Configurations configurations = new TestEnvironment();
		Arena arena = createArena(configurations.getEnvironment().getArena());
		simulation = new Simulation(configurations, arena);
	}
	
	public void tick()
	{
		try
		{
			simulation.tick(executor);
		}
		catch (InterruptedException e)
		{
			SneakyThrow.perform(e);
		}
		catch (ExecutionException e)
		{
			SneakyThrow.perform(e.getCause());
		}
	}
	
	protected Arena createArena(EnvironmentArena arenaConfiguration)
	{
		return ArenaFactory.create(arenaConfiguration);
	}
	
	protected String getAutosaveConfiguration()
	{
		return """
			filename: autosave
			frequency: 200
			""";
	}
	
	protected String getPerformanceConfiguration()
	{
		return """
			threads:
			  simulation: 8
			""";
	}
	
	protected String getEnvironmentConfiguration()
	{
		return """
			arena:
			  size: 5000
			  type: square
			
			food:
			  frequency: -1
			  energy:
			    min: 1000
			    max: 3000
			  body:
			    min: 100
			    max: 200
			""";
	}
	
	protected String getMutationConfiguration()
	{
		return """
			enabled: false
			
			gene:
			  duplicate: 0.005
			  swap: 0.005
			  delete: 0.005
			
			dna:
			  duplicate: 0.005
			  swap: 0.005
			  delete: 0.005
			  create: 0.005
			""";
	}
	
	@Getter
	public class TestEnvironment implements Configurations
	{
		private final AutosaveConfiguration autosave = yaml.loadAs(getAutosaveConfiguration(), AutosaveConfiguration.class);
		private final PerformanceConfiguration performance = yaml.loadAs(getPerformanceConfiguration(), PerformanceConfiguration.class);
		private final EnvironmentConfiguration environment = yaml.loadAs(getEnvironmentConfiguration(), EnvironmentConfiguration.class);
		private final MutationConfiguration mutation = yaml.loadAs(getMutationConfiguration(), MutationConfiguration.class);
		
		@Override
		public RawInformation getRawInformation()
		{
			ThrowRejectDueTo.notYetImplemented();
			return null;
		}
	}
}
