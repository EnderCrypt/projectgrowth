package endercrypt.projectgrowth.mutation;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.projectgrowth.simulation.mutation.MutationFunctions;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;


public class MutationFunctionsTest
{
	@Test
	void createWorksWithZeroElements()
	{
		List<String> words = new ArrayList<>();
		MutationFunctions.create(words, 100.0, () -> "%");
		assertEquals(List.of("%"), words);
	}
	
	@Test
	void createCanInsertBeforeAndAfterDna()
	{
		List<String> words = new ArrayList<>();
		words.add("hello");
		words.add("world");
		MutationFunctions.create(words, 100.0, () -> "%");
		assertEquals(List.of("%", "hello", "%", "world", "%"), words);
	}
}
