package endercrypt.projectgrowth.simulation;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.projectgrowth.abstr.AbstractMicrobeTest;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.matter.Matter;

import org.junit.jupiter.api.Test;


public class ReproductionTest extends AbstractMicrobeTest
{
	@Test
	public void reproductionWorks()
	{
		createMicrobe("""
			cond
			start
				25 .repro store
			stop
			""");
		
		assertEquals(1, simulation.getEntities().count());
		tick();
		assertEquals(2, simulation.getEntities().count());
		tick();
		assertEquals(4, simulation.getEntities().count());
		tick();
		assertEquals(8, simulation.getEntities().count());
		tick();
		assertEquals(16, simulation.getEntities().count());
		tick();
		assertEquals(32, simulation.getEntities().count());
		tick();
		assertEquals(64, simulation.getEntities().count());
		tick();
		assertEquals(128, simulation.getEntities().count());
		
		double totalEnergy = simulation.getEntities().stream(Microbe.class)
			.mapToDouble(microbe -> microbe.getInfo().getComposition().get(Matter.ENERGY))
			.sum();
		
		assertTrue(totalEnergy < 3000);
		assertTrue(totalEnergy > 2900);
	}
}
