package endercrypt.projectgrowth.simulation;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.abstr.AbstractMicrobeTest;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import org.junit.jupiter.api.Test;


public class RefXYPosition extends AbstractMicrobeTest
{
	@Test
	public void xyPositionIsZeroByDefault()
	{
		Microbe microbe = createMicrobe("""
			cond
			start
				0 .setaim store
			stop
			""");
		
		tick();
		
		Memory memory = microbe.getMemory();
		assertEquals(0, memory.get(Sysvar.refxpos));
		assertEquals(0, memory.get(Sysvar.refypos));
	}
	
	@Test
	public void xyPositionWorks()
	{
		Microbe microbeLeft = createMicrobe("""
			cond
			start
				1 .fixpos store
				0 .setaim store
			stop
			end
			""");
		Vector leftPosition = microbeLeft.getInfo().getPosition();
		leftPosition.set(-30, 50);
		
		Microbe microbeRight = createMicrobe("""
			cond
			start
				1 .fixpos store
			stop
			end
			""");
		Vector rightPosition = microbeRight.getInfo().getPosition();
		rightPosition.set(30, 50);
		
		tick();
		tick();
		saveDebugScreenshot(200, "debug.png");
		
		Memory memory = microbeLeft.getMemory();
		assertEquals(rightPosition.x, memory.get(Sysvar.refxpos));
		assertEquals(rightPosition.y, memory.get(Sysvar.refypos));
	}
}
