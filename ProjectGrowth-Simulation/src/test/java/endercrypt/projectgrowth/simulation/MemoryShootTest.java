package endercrypt.projectgrowth.simulation;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.projectgrowth.abstr.AbstractMicrobeTest;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import org.junit.jupiter.api.Test;


public class MemoryShootTest extends AbstractMicrobeTest
{
	@Test
	public void memoryShootsSetsValue()
	{
		Microbe microbeLeftSide = createMicrobe("""
			cond
			start
				0 .setaim store
				95 .shootval store
				99 .shoot store
			stop
			end
			""");
		microbeLeftSide.getInfo().getPosition().set(-microbeLeftSide.getRadius(), 0);
		
		Microbe microbeRightSide = createMicrobe("""
			cond
			start
				628 .setaim store
			stop
			end
			""");
		microbeRightSide.getInfo().getPosition().set(microbeRightSide.getRadius(), 0);
		
		tick(); // spawns in bots
		assertSame(0, microbeRightSide.getMemory().get(99));
		
		tick(); // memory shot should land
		assertSame(95, microbeRightSide.getMemory().get(99));
	}
}
