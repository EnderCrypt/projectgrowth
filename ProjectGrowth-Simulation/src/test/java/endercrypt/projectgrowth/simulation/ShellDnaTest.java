package endercrypt.projectgrowth.simulation;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.projectgrowth.abstr.AbstractMicrobeTest;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.matter.Matter;

import org.junit.jupiter.api.Test;


public class ShellDnaTest extends AbstractMicrobeTest
{
	@Test
	public void shellCostsTenthEnergy()
	{
		Microbe microbe = createMicrobe("""
			cond
			start
				100 .mkshell store
			stop
			end
			""");
		
		assertEquals(3000, microbe.getInfo().getComposition().get(Matter.ENERGY));
		assertEquals(0, microbe.getInfo().getComposition().get(Matter.SHELL));
		
		tick();
		
		assertEquals(3000 - 10, microbe.getInfo().getComposition().get(Matter.ENERGY), 0.2);
		assertEquals(100, microbe.getInfo().getComposition().get(Matter.SHELL));
	}
}
