package endercrypt.projectgrowth.gui.camera;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.gui.GuiWindow;

import java.awt.geom.AffineTransform;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class Camera
{
	private static final double CAMERA_SPEED = 3.0;
	private static final double CAMERA_FRICTION = 0.1;
	
	private final CameraZoom zoom = new CameraZoom();
	
	private final Vector position = new Vector(0, 0);
	private final Vector motion = new Vector(0, 0);
	
	private final GuiWindow guiWindow;
	
	public void move(int x, int y)
	{
		double xSpeed = CAMERA_SPEED * x * zoom.getInverseZoom();
		double ySpeed = CAMERA_SPEED * y * zoom.getInverseZoom();
		motion.add(xSpeed, ySpeed);
	}
	
	public void update()
	{
		position.add(motion);
		motion.multiply(1.0 - CAMERA_FRICTION);
		zoom.update();
	}
	
	public Vector screenToSurface(Vector positionSurface)
	{
		Vector screenOffset = guiWindow.getSize();
		screenOffset.multiply(0.5);
		
		Vector result = Vector.copy(positionSurface);
		result.subtract(screenOffset);
		result.multiply(zoom.getInverseZoom());
		result.subtract(position);
		return result;
	}
	
	public Vector surfaceToScreen(Vector positionScreen)
	{
		Vector result = Vector.copy(positionScreen);
		
		result.add(position);
		result.multiply(zoom.getZoom());
		
		Vector screenOffset = guiWindow.getSize();
		screenOffset.multiply(0.5);
		result.add(screenOffset);
		
		return result;
	}
	
	public void apply(AffineTransform transform)
	{
		zoom.apply(transform);
		transform.translate(position.x, position.y);
	}
}
