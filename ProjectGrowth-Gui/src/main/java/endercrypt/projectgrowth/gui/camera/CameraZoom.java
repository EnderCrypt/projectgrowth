package endercrypt.projectgrowth.gui.camera;


import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;

import lombok.Getter;


@Getter
public class CameraZoom
{
	private static final double ZOOM_SPEED = 0.05;
	private static final double ZOOM_FRICTION = 0.15;
	
	private static final double ZOOM_MIN = 0.1;
	private static final double ZOOM_MAX = 5.0;
	
	private final Listener listener = new Listener();
	
	private double zoom = 1.0;
	private double motion = 0;
	
	public void update()
	{
		zoom += motion;
		motion *= (1.0 - ZOOM_FRICTION);
		
		if (zoom <= ZOOM_MIN)
		{
			zoom = ZOOM_MIN;
			motion = 0;
		}
		if (zoom >= ZOOM_MAX)
		{
			zoom = ZOOM_MAX;
			motion = 0;
		}
	}
	
	public double getInverseZoom()
	{
		return 1.0 / getZoom();
	}
	
	public void apply(AffineTransform transform)
	{
		transform.scale(getZoom(), getZoom());
	}
	
	public class Listener implements MouseWheelListener
	{
		@Override
		public void mouseWheelMoved(MouseWheelEvent e)
		{
			motion = (-e.getWheelRotation() * ZOOM_SPEED) * zoom;
		}
	}
}
