package endercrypt.projectgrowth.gui;


import endercrypt.library.commons.iterator.PurifyingIterator;
import endercrypt.projectgrowth.common.layerarray.LayeredArray;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.simulation.SimulationControl;

import java.util.Iterator;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class GuiWindowManager implements Iterable<GuiWindow>
{
	private static final int FPS = 50;
	private final LayeredArray<GuiWindow> windows = new LayeredArray<>();
	
	public GuiWindowManager(ScheduledExecutorService scheduler)
	{
		int delay = 1000 / FPS;
		scheduler.scheduleAtFixedRate(this::update, delay, delay, TimeUnit.MILLISECONDS);
	}
	
	private void update()
	{
		windows.forEach(GuiWindow::update);
		repaint();
	}
	
	public GuiWindow createWindow(Simulation simulation, SimulationControl simulationControl, String title)
	{
		GuiWindow window = new GuiWindow(simulation, simulationControl, title);
		windows.add(window);
		return window;
	}
	
	public void repaint()
	{
		synchronized (windows)
		{
			windows.flush();
			Iterator<GuiWindow> iterator = iterator();
			
			while (iterator.hasNext())
			{
				GuiWindow window = iterator.next();
				window.repaint();
			}
			windows.flush();
		}
	}
	
	@Override
	public Iterator<GuiWindow> iterator()
	{
		return new PurifyingIterator<>(windows, (window) -> window.getJFrame().isDisplayable())
			.addPurifyListener(gui -> gui.dispose());
	}
}
