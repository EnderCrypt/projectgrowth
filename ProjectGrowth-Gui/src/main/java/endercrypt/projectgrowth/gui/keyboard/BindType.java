package endercrypt.projectgrowth.gui.keyboard;

/**
 * enum for choosing bindtypes (Press, Hold, Release)
 *
 * @author EnderCrypt
 */
public enum BindType
{
	PRESS,
	HOLD,
	RELEASE;
}
