package endercrypt.projectgrowth.gui.keyboard.listener.group;


import endercrypt.projectgrowth.gui.keyboard.KeyboardBindFilter;
import endercrypt.projectgrowth.gui.keyboard.KeyboardBinding;
import endercrypt.projectgrowth.gui.keyboard.KeyboardEvent;
import endercrypt.projectgrowth.gui.keyboard.listener.KeyboardListener;

import java.util.HashSet;
import java.util.Set;


/**
 * class representing a group of keyboard listener, this class must be added to
 * a {@link ListenerGroups} to recieve events
 *
 * @author EnderCrypt
 */
public class StandardListenerGroup implements ListenerGroup
{
	private Set<KeyboardBinding> bindings = new HashSet<>();

	@Override
	public void bind(KeyboardBindFilter keyboardBindFilter, KeyboardListener keyboardListener)
	{
		synchronized (bindings)
		{
			bindings.add(new KeyboardBinding(keyboardBindFilter, keyboardListener));
		}
	}

	@Override
	public void trigger(KeyboardEvent keyboardEvent)
	{
		synchronized (bindings)
		{
			for (KeyboardBinding binding : bindings)
			{
				binding.attemptTrigger(keyboardEvent);
			}
		}
	}

	@Override
	public void clear()
	{
		bindings.clear();
	}
}
