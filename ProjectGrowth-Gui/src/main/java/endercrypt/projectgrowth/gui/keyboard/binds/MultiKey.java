package endercrypt.projectgrowth.gui.keyboard.binds;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.KeyboardBindFilter;
import endercrypt.projectgrowth.gui.keyboard.KeyboardEvent;


/**
 * keybind for multiple keys
 *
 * @author EnderCrypt
 * @see KeyEvent
 */
public class MultiKey implements KeyboardBindFilter
{
	private BindType bindType;
	private int[] keys;

	public MultiKey(BindType bindType, int... keys)
	{
		this.bindType = bindType;
		this.keys = keys;
	}

	@Override
	public boolean check(KeyboardEvent keyboardEvent)
	{
		if (keyboardEvent.getBindType() == bindType)
		{
			for (int key : keys)
			{
				if (keyboardEvent.getKeyCode() == key)
				{
					return true;
				}
			}
		}
		return false;
	}
}
