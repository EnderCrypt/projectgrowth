package endercrypt.projectgrowth.gui.keyboard.binds.directional;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.binds.MultiKey;

import java.awt.event.KeyEvent;


/**
 * keybind for the up arrow OR W key (as in WASD)
 *
 * @author EnderCrypt
 */
public class UpKey extends MultiKey
{
	public UpKey(BindType bindType)
	{
		super(bindType, KeyEvent.VK_UP, KeyEvent.VK_W);
	}
}
