package endercrypt.projectgrowth.gui.keyboard.binds.directional;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.binds.MultiKey;

import java.awt.event.KeyEvent;


/**
 * keybind for the right arrow OR D key (as in WASD)
 *
 * @author EnderCrypt
 */
public class RightKey extends MultiKey
{
	public RightKey(BindType bindType)
	{
		super(bindType, KeyEvent.VK_RIGHT, KeyEvent.VK_D);
	}
}
