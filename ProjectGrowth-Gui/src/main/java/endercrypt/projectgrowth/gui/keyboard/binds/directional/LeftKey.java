package endercrypt.projectgrowth.gui.keyboard.binds.directional;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.binds.MultiKey;

import java.awt.event.KeyEvent;


/**
 * keybind for the left arrow OR A key (as in WASD)
 *
 * @author EnderCrypt
 */
public class LeftKey extends MultiKey
{
	public LeftKey(BindType bindType)
	{
		super(bindType, KeyEvent.VK_LEFT, KeyEvent.VK_A);
	}
}
