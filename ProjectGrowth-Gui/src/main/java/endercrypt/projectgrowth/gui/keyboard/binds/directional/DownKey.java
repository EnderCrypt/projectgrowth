package endercrypt.projectgrowth.gui.keyboard.binds.directional;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.binds.MultiKey;

import java.awt.event.KeyEvent;


/**
 * keybind for the down arrow OR S key (as in WASD)
 *
 * @author EnderCrypt
 */
public class DownKey extends MultiKey
{
	public DownKey(BindType bindType)
	{
		super(bindType, KeyEvent.VK_DOWN, KeyEvent.VK_S);
	}
}
