package endercrypt.projectgrowth.gui.keyboard.binds;


import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.KeyboardBindFilter;
import endercrypt.projectgrowth.gui.keyboard.KeyboardEvent;


/**
 * Keybind for a single key
 *
 * @author EnderCrypt
 * @see KeyEvent
 */
public class SpecificKey implements KeyboardBindFilter
{
	private BindType bindType;
	private int keyCode;

	public SpecificKey(BindType bindType, int keyCode)
	{
		this.bindType = bindType;
		this.keyCode = keyCode;
	}

	@Override
	public boolean check(KeyboardEvent keyboardEvent)
	{
		if ((keyboardEvent.getBindType() != bindType) || (keyboardEvent.getKeyCode() != keyCode))
		{
			return false;
		}
		return true;
	}
}
