package endercrypt.projectgrowth.gui.keyboard.binds;


import endercrypt.projectgrowth.gui.keyboard.BindType;

import java.awt.event.KeyEvent;


/**
 * keybind for all the numbers on the keyboard
 *
 * @author EnderCrypt
 * @see KeyEvent
 */
public class AnyNumbers extends BindRange
{
	public AnyNumbers(BindType bindType)
	{
		super(bindType, KeyEvent.VK_0, KeyEvent.VK_9);
	}
}
