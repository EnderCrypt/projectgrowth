package endercrypt.projectgrowth.gui.overlay;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.gui.overlay.OverlayPanel.Alignment;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Optional;


public abstract class OverlayTextPanelFactory implements OverlayPanelFactory
{
	public static final int padding = 5;
	public static final Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
	public static final FontMetrics fontMetrics = Ender.graphics.createFontMetrics(font);
	
	@Override
	public final Optional<OverlayPanel> render(Growth growth)
	{
		String[] lines = getText(growth)
			.map(text -> text.split("\n"))
			.orElse(new String[0]);
		
		if (lines.length == 0)
			return Optional.empty();
		
		int width = Arrays.stream(lines)
			.mapToInt(fontMetrics::stringWidth)
			.max().orElse(0) + (padding * 2);
		int height = (lines.length * fontMetrics.getHeight()) + (padding * 2);
		
		BufferedImage image = OverlayPanelFactory.createPanel(width, height);
		Graphics2D g2d = image.createGraphics();
		
		try
		{
			g2d.setColor(Color.BLACK);
			g2d.setFont(OverlayTextPanelFactory.font);
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
			render(g2d, lines);
		}
		finally
		{
			g2d.dispose();
		}
		return Optional.of(new OverlayPanel(image, Alignment.LEFT));
	}
	
	private void render(Graphics2D g2d, String[] lines)
	{
		int index = 0;
		for (String line : lines)
		{
			int linePosition = (index * fontMetrics.getHeight());
			int x = padding;
			int y = padding + linePosition + fontMetrics.getAscent();
			g2d.drawString(line, x, y);
			index++;
		}
	}
	
	protected abstract Optional<String> getText(Growth growth);
}
