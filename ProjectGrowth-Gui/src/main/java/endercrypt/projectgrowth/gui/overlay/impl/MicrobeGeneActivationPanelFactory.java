package endercrypt.projectgrowth.gui.overlay.impl;


import endercrypt.projectgrowth.dna.genome.activation.GeneActivation;
import endercrypt.projectgrowth.dna.genome.activation.GenomeActivation;
import endercrypt.projectgrowth.gui.overlay.OverlayPanel;
import endercrypt.projectgrowth.gui.overlay.OverlayPanel.Alignment;
import endercrypt.projectgrowth.gui.overlay.OverlayPanelFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Optional;


public class MicrobeGeneActivationPanelFactory implements OverlayPanelFactory
{
	private static final int ICON_WIDTH = 32;
	private static final int ICON_HEIGHT = 32;
	private static final int PADDING = 3;
	private static final int PADDING_TOTAL = PADDING * 2;
	
	@Override
	public Optional<OverlayPanel> render(Growth growth)
	{
		if (growth instanceof Microbe == false)
			return Optional.empty();
		Microbe microbe = (Microbe) growth;
		
		BufferedImage image = render(microbe).orElse(null);
		if (image == null)
			return Optional.empty();
		
		OverlayPanel overlayPanel = new OverlayPanel(image, Alignment.CENTER);
		return Optional.of(overlayPanel);
	}
	
	private Optional<BufferedImage> render(Microbe microbe)
	{
		GenomeActivation genomeActivation = microbe.getGenomeActivation().orElse(null);
		if (genomeActivation == null)
			return Optional.empty();
		
		int geneCount = genomeActivation.getGenes().size();
		int genePaddingCount = Math.max(0, geneCount - 1);
		int width = geneCount * ICON_WIDTH + PADDING_TOTAL + (genePaddingCount * PADDING);
		int height = ICON_HEIGHT + PADDING_TOTAL;
		BufferedImage image = OverlayPanelFactory.createPanel(width, height);
		Graphics2D g2d = image.createGraphics();
		try
		{
			render(g2d, genomeActivation);
		}
		finally
		{
			g2d.dispose();
		}
		return Optional.of(image);
	}
	
	private void render(Graphics2D g2d, GenomeActivation genomeActivation)
	{
		int x = 0;
		for (GeneActivation geneActivation : genomeActivation.getGenes())
		{
			g2d.setColor(geneActivation.isActivated() ? Color.GREEN : Color.RED);
			g2d.fillRect(x + PADDING, PADDING, ICON_WIDTH, ICON_HEIGHT);
			
			g2d.setColor(Color.BLACK);
			g2d.drawRect(x + PADDING, PADDING, ICON_WIDTH, ICON_HEIGHT);
			
			x += ICON_WIDTH + PADDING;
		}
	}
}
