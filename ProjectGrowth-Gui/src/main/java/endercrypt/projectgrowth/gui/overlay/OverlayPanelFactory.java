package endercrypt.projectgrowth.gui.overlay;


import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Optional;


public interface OverlayPanelFactory
{
	public static BufferedImage createPanel(int width, int height)
	{
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g2d = image.createGraphics();
		g2d.setColor(GuiOverlayRenderer.backgroundColor);
		g2d.fillRect(0, 0, width, height);
		g2d.dispose();
		
		return image;
	}
	
	public Optional<OverlayPanel> render(Growth growth);
}
