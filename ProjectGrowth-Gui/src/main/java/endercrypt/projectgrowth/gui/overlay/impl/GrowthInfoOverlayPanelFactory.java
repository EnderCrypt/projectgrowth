package endercrypt.projectgrowth.gui.overlay.impl;


import endercrypt.projectgrowth.gui.overlay.OverlayTextPanelFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.growth.GrowthInfo;
import endercrypt.projectgrowth.simulation.matter.Matter;

import java.util.Optional;


public class GrowthInfoOverlayPanelFactory extends OverlayTextPanelFactory
{
	@Override
	protected Optional<String> getText(Growth growth)
	{
		StringBuilder builder = new StringBuilder();
		GrowthInfo info = growth.getInfo();
		
		int energy = (int) Math.ceil(info.getComposition().get(Matter.ENERGY));
		builder.append("Energy: ").append(energy).append("\n");
		
		int body = (int) Math.ceil(info.getComposition().get(Matter.BODY));
		builder.append("Body:   ").append(body).append("\n");
		
		int ties = growth.getTies().count();
		builder.append("Ties:   ").append(ties).append("\n");
		
		return Optional.of(builder.toString());
	}
}
