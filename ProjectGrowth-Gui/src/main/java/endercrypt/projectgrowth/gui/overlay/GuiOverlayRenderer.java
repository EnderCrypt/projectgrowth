package endercrypt.projectgrowth.gui.overlay;


import endercrypt.projectgrowth.gui.overlay.impl.GrowthInfoOverlayPanelFactory;
import endercrypt.projectgrowth.gui.overlay.impl.MicrobeDnaOverlayPanelFactory;
import endercrypt.projectgrowth.gui.overlay.impl.MicrobeGeneActivationPanelFactory;
import endercrypt.projectgrowth.gui.overlay.impl.MicrobeInfoOverlayPanelFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class GuiOverlayRenderer
{
	private static final int ZIG_ZAG_RADIUS = 3;
	private static final int ZIG_ZAG_HEIGHT = ZIG_ZAG_RADIUS * 2;
	private static final int ZIG_ZAG_FREQUENCY = 10;
	
	public static final Color backgroundColor = new Color(255, 255, 180);
	public static final Color borderColor = Color.BLACK;
	
	private static List<OverlayPanelFactory> panelFactories = new ArrayList<>();
	
	static
	{
		panelFactories.add(new GrowthInfoOverlayPanelFactory());
		panelFactories.add(new MicrobeInfoOverlayPanelFactory());
		panelFactories.add(new MicrobeDnaOverlayPanelFactory());
		panelFactories.add(new MicrobeGeneActivationPanelFactory());
	}
	
	public static BufferedImage render(Growth growth)
	{
		List<OverlayPanel> panels = panelFactories.stream()
			.flatMap(panel -> panel.render(growth).stream())
			.toList();
		
		int totalPanelWidth = panels.stream()
			.mapToInt(panel -> panel.getImage().getWidth())
			.max().orElse(0);
		int width = totalPanelWidth + 2;
		
		int totalPanelHeight = panels.stream()
			.mapToInt(panel -> panel.getImage().getHeight())
			.sum();
		int zigZags = Math.max(0, panels.size() - 1);
		int height = totalPanelHeight + (zigZags * ZIG_ZAG_HEIGHT) + 2;
		
		BufferedImage image = OverlayPanelFactory.createPanel(width, height);
		Graphics2D g2d = image.createGraphics();
		
		// border
		g2d.setColor(borderColor);
		g2d.drawRect(0, 0, width - 1, height - 1);
		
		// panels
		int y = 1;
		Iterator<OverlayPanel> iterator = panels.iterator();
		while (iterator.hasNext())
		{
			OverlayPanel panel = iterator.next();
			BufferedImage panelImage = panel.getImage();
			
			int x = (int) (1 + Math.ceil((width - panel.getImage().getWidth() - 2) * panel.getAlignment().getPosition()));
			g2d.drawImage(panelImage, x, y, null);
			y += panelImage.getHeight();
			
			// zig-zag
			if (iterator.hasNext())
			{
				drawHorizontalZigZagLine(g2d, 0, width, y + ZIG_ZAG_RADIUS - 1);
				y += 6;
			}
		}
		
		g2d.dispose();
		return image;
	}
	
	private static void drawHorizontalZigZagLine(Graphics2D g2d, int xLeft, int xRight, int y)
	{
		int lineWidth = xRight - xLeft;
		int shakes = lineWidth / ZIG_ZAG_FREQUENCY;
		for (int i = 0; i < shakes; i++)
		{
			int lx1 = (int) Math.round(xLeft + (lineWidth / (double) shakes * (i + 0)));
			int lx2 = (int) Math.round(xLeft + (lineWidth / (double) shakes * (i + 1)));
			int zigZag = (i % 2 == 0) ? -ZIG_ZAG_RADIUS : ZIG_ZAG_RADIUS;
			g2d.drawLine(lx1, y - zigZag, lx2, y + zigZag);
		}
	}
}
