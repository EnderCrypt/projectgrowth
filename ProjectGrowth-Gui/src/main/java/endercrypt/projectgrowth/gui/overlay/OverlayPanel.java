package endercrypt.projectgrowth.gui.overlay;


import java.awt.image.BufferedImage;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class OverlayPanel
{
	private final BufferedImage image;
	private final Alignment alignment;
	
	@Getter
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public enum Alignment
	{
		LEFT(0.0),
		CENTER(0.5),
		RIGHT(1.0);
		
		private final double position;
	}
}
