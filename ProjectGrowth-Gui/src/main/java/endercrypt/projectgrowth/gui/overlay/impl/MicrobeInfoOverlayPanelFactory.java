package endercrypt.projectgrowth.gui.overlay.impl;


import endercrypt.projectgrowth.gui.overlay.OverlayTextPanelFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;
import endercrypt.projectgrowth.simulation.entities.microbe.MicrobeInfo;

import java.util.Optional;


public class MicrobeInfoOverlayPanelFactory extends OverlayTextPanelFactory
{
	@Override
	protected Optional<String> getText(Growth growth)
	{
		if (growth instanceof Microbe microbe)
		{
			return Optional.of(getText(microbe));
		}
		return Optional.empty();
	}
	
	private String getText(Microbe microbe)
	{
		StringBuilder builder = new StringBuilder();
		MicrobeInfo info = microbe.getInfo();
		
		builder.append("Name:   ").append(microbe.getGenome().getVisualization().getName()).append("\n");
		builder.append("Age:    ").append(info.getAge()).append("\n");
		builder.append("Kills:  ").append(info.getKills()).append("\n");
		
		return builder.toString();
	}
}
