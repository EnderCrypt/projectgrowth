package endercrypt.projectgrowth.gui.overlay.impl;


import endercrypt.projectgrowth.gui.overlay.OverlayTextPanelFactory;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.util.Optional;


public class MicrobeDnaOverlayPanelFactory extends OverlayTextPanelFactory
{
	@Override
	protected Optional<String> getText(Growth growth)
	{
		if (growth instanceof Microbe microbe)
		{
			return Optional.of(getText(microbe));
		}
		return Optional.empty();
	}
	
	private String getText(Microbe microbe)
	{
		StringBuilder builder = new StringBuilder();
		
		for (String dnaLine : microbe.getGenome().toString().split("\n"))
		{
			builder.append(dnaLine).append("\n");
		}
		
		return builder.toString();
	}
}
