package endercrypt.projectgrowth.gui.rendering;


import endercrypt.projectgrowth.gui.GuiWindow;

import java.awt.Graphics;

import javax.swing.JPanel;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class RenderPanel extends JPanel
{
	private static final long serialVersionUID = -7942827343747902415L;
	
	/**
	 * 
	 */
	
	private final GuiWindow guiWindow;
	
	@Override
	protected void paintComponent(Graphics graphics)
	{
		try (Renderer renderer = new Renderer(guiWindow, getSize(), graphics))
		{
			renderer.draw();
			renderer.drawHud();
		}
	}
}
