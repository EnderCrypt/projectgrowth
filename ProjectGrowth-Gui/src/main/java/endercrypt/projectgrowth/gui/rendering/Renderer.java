package endercrypt.projectgrowth.gui.rendering;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.common.layerarray.SmartLayeredArray;
import endercrypt.projectgrowth.gui.GuiWindow;
import endercrypt.projectgrowth.gui.overlay.GuiOverlayRenderer;
import endercrypt.projectgrowth.simulation.entities.entity.Entity;
import endercrypt.projectgrowth.simulation.entities.growth.Growth;
import endercrypt.projectgrowth.simulation.entities.microbe.Microbe;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Optional;


public class Renderer implements AutoCloseable
{
	private final GuiWindow guiWindow;
	private final Dimension size;
	private final Graphics2D g2d;
	private final Graphics2D g2dHud;
	
	private final Vector mouseScreenPosition;
	private final Vector mouseSurfacePosition;
	private final Growth focusedGrowth;
	
	protected Renderer(GuiWindow guiWindow, Dimension size, Graphics g)
	{
		this.guiWindow = guiWindow;
		this.size = size;
		g2d = (Graphics2D) g;
		g2dHud = (Graphics2D) g2d.create();
		
		AffineTransform transform = new AffineTransform();
		transform.translate(size.width / 2, size.height / 2);
		this.guiWindow.getCamera().apply(transform);
		g2d.setTransform(transform);
		
		mouseScreenPosition = guiWindow.getMouse().orElse(null);
		mouseSurfacePosition = Optional.ofNullable(mouseScreenPosition)
			.map(guiWindow.getCamera()::screenToSurface)
			.orElse(null);
		
		focusedGrowth = getFocusedGrowth(mouseSurfacePosition);
	}
	
	private Growth getFocusedGrowth(Vector mousePosition)
	{
		if (mousePosition == null)
		{
			return null;
		}
		
		SmartLayeredArray<Entity> entities = guiWindow.getSimulation().getEntities();
		
		synchronized (entities)
		{
			return entities.stream(Growth.class)
				.filter(growth -> growth.getInfo().getPosition().getLength(mouseSurfacePosition) < growth.getRadius())
				.findAny().orElse(null);
		}
	}
	
	public void draw()
	{
		guiWindow.getSimulation().draw(g2d, this::entityHighlightFunction);
	}
	
	private Color entityHighlightFunction(Entity entity)
	{
		if (entity instanceof Microbe == false)
			return null;
		Microbe microbe = (Microbe) entity;
		
		if (focusedGrowth instanceof Microbe == false)
			return null;
		Microbe focusedMicrobe = (Microbe) focusedGrowth;
		
		return focusedMicrobe.getGenome().equals(microbe.getGenome())
			? Color.GREEN
			: Color.RED;
	}
	
	public void drawHud()
	{
		// interface
		if (guiWindow.getSimulationControl().isRunning() == false)
		{
			int padding = 10;
			g2dHud.setColor(new Color(255, 0, 0, 50));
			g2dHud.setStroke(new BasicStroke(5f));
			g2dHud.drawRect(padding, padding, size.width - (padding * 2), size.height - (padding * 2));
		}
		
		// overlay
		if (focusedGrowth != null)
		{
			drawOverlayForFocusedGrowth();
		}
	}
	
	private void drawOverlayForFocusedGrowth()
	{
		int mouseX = (int) mouseScreenPosition.x;
		int mouseY = (int) mouseScreenPosition.y;
		
		int x = mouseX + 10;
		int y = mouseY + 15;
		
		BufferedImage overlay = GuiOverlayRenderer.render(focusedGrowth);
		g2dHud.drawImage(overlay, x, y, null);
		
		g2dHud.setStroke(new BasicStroke(2f));
		g2dHud.setColor(Color.BLACK);
		Vector growthPosition = guiWindow.getCamera().surfaceToScreen(focusedGrowth.getInfo().getPosition());
		g2dHud.drawLine((int) growthPosition.x, (int) growthPosition.y, mouseX, mouseY);
	}
	
	@Override
	public void close()
	{
		g2d.dispose();
		g2dHud.dispose();
	}
}
