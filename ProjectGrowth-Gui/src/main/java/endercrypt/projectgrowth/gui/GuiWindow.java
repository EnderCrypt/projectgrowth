package endercrypt.projectgrowth.gui;


import endercrypt.library.commons.position.Vector;
import endercrypt.projectgrowth.gui.camera.Camera;
import endercrypt.projectgrowth.gui.keyboard.BindType;
import endercrypt.projectgrowth.gui.keyboard.KeyboardManager;
import endercrypt.projectgrowth.gui.keyboard.binds.SpecificKey;
import endercrypt.projectgrowth.gui.keyboard.binds.directional.DownKey;
import endercrypt.projectgrowth.gui.keyboard.binds.directional.LeftKey;
import endercrypt.projectgrowth.gui.keyboard.binds.directional.RightKey;
import endercrypt.projectgrowth.gui.keyboard.binds.directional.UpKey;
import endercrypt.projectgrowth.gui.rendering.RenderPanel;
import endercrypt.projectgrowth.simulation.simulation.Simulation;
import endercrypt.projectgrowth.simulation.simulation.SimulationControl;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import lombok.Getter;


public class GuiWindow
{
	@Getter
	private final Simulation simulation;
	
	@Getter
	private final SimulationControl simulationControl;
	
	@Getter
	private final JFrame jFrame = new JFrame();
	
	@Getter
	private final Camera camera = new Camera(this);
	
	@Getter
	private final KeyboardManager keyboardManager = new KeyboardManager();
	
	private final RenderPanel renderPanel;
	
	public GuiWindow(Simulation simulation, SimulationControl simulationControl, String title) throws HeadlessException
	{
		this.simulation = simulation;
		this.simulationControl = simulationControl;
		this.renderPanel = new RenderPanel(this);
		jFrame.setTitle(title);
		jFrame.getContentPane().add(renderPanel);
		jFrame.setMinimumSize(new Dimension(500, 500));
		jFrame.setPreferredSize(new Dimension(1000, 1000));
		jFrame.pack();
		jFrame.setLocationRelativeTo(null);
		jFrame.setVisible(true);
		jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		// mouse
		jFrame.addMouseWheelListener(camera.getZoom().getListener());
		
		// keyboard
		keyboardManager.install(jFrame);
		keyboardManager.getListenerGroups().global().bind(new LeftKey(BindType.HOLD), (e) -> camera.move(1, 0));
		keyboardManager.getListenerGroups().global().bind(new RightKey(BindType.HOLD), (e) -> camera.move(-1, 0));
		keyboardManager.getListenerGroups().global().bind(new UpKey(BindType.HOLD), (e) -> camera.move(0, 1));
		keyboardManager.getListenerGroups().global().bind(new DownKey(BindType.HOLD), (e) -> camera.move(0, -1));
		keyboardManager.getListenerGroups().global().bind(new SpecificKey(BindType.PRESS, KeyEvent.VK_SPACE), (e) -> simulationControl.toggle());
		keyboardManager.getListenerGroups().global().bind(new SpecificKey(BindType.PRESS, KeyEvent.VK_PERIOD), (e) -> simulationControl.step());
	}
	
	public Vector getSize()
	{
		return new Vector(renderPanel.getWidth(), renderPanel.getHeight());
	}
	
	public Optional<Vector> getMouse()
	{
		Point mouse = renderPanel.getMousePosition();
		if (mouse == null)
		{
			return Optional.empty();
		}
		return Optional.of(new Vector(mouse.x, mouse.y));
	}
	
	public void update()
	{
		camera.update();
		keyboardManager.triggerHeldKeys();
	}
	
	public void repaint()
	{
		jFrame.repaint();
	}
	
	public void dispose()
	{
		jFrame.dispose();
	}
}
