package endercrypt.projectgrowth.common.modules;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.common.ProjectGrowthRuntimeException;

import lombok.Getter;


@Getter
public class ProjectGrowthModuleInfo
{
	public static final String rootPackageString = "endercrypt.projectgrowth";
	
	public static final ModuleCommon common = new ModuleCommon();
	public static final ModuleDna dna = new ModuleDna();
	public static final ModuleGui gui = new ModuleGui();
	public static final ModuleMain main = new ModuleMain();
	public static final ModuleSetting setting = new ModuleSetting();
	public static final ModuleSimulation simulation = new ModuleSimulation();
	
	private final String name;
	private final String packageString;
	
	protected ProjectGrowthModuleInfo()
	{
		String className = getClass().getSimpleName();
		this.name = Ender.text.removeBeginning(className, "Module")
			.orElseThrow(() -> new ProjectGrowthRuntimeException("Class name '" + className + "' was expected to begin with 'Module'"))
			.toLowerCase();
		this.packageString = rootPackageString + "." + name;
	}
}
