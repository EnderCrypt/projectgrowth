package endercrypt.projectgrowth.common.geometry;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Circle;

import java.io.Serializable;


public class Rotation implements Serializable
{
	private static final long serialVersionUID = -2210104346196889117L;
	
	/**
	 *
	 */
	
	public static Rotation random()
	{
		return new Rotation(Math.random() * Circle.FULL);
	}
	
	public static Rotation copy(Rotation other)
	{
		return fromRadians(other.getRadians());
	}
	
	// primitive convert //
	
	public static double toDarwin(double radians)
	{
		return radians * DARWIN_ROTATION;
	}
	
	public static double toRadians(double darwin)
	{
		return darwin / DARWIN_ROTATION;
	}
	
	// create //
	
	public static Rotation fromDarwin(double darwin)
	{
		return fromRadians(toRadians(darwin));
	}
	
	public static Rotation fromRadians(double radians)
	{
		return new Rotation(radians);
	}
	
	public static final double DARWIN_ROTATION = 200;
	
	public static final double QUARTER_DARWIN_CIRCLE = (Circle.QUARTER * DARWIN_ROTATION);
	public static final double HALF_DARWIN_CIRCLE = (Circle.HALF * DARWIN_ROTATION);
	public static final double FULL_DARWIN_CIRCLE = (Circle.FULL * DARWIN_ROTATION);
	
	private double radians;
	
	private Rotation(double radians)
	{
		setRadians(radians);
	}
	
	// RADIANS //
	
	public void increaseRadians(double value)
	{
		setRadians(getRadians() + value);
	}
	
	public void setRadians(double value)
	{
		this.radians = Ender.math.floorMod(value, Circle.FULL);
	}
	
	public double getRadians()
	{
		return radians;
	}
	
	// DARWIANS //
	
	public void increaseDarwin(int value)
	{
		increaseDarwin((double) value);
	}
	
	public void increaseDarwin(double value)
	{
		setDarwin(getDarwin() + value);
	}
	
	public void setDarwin(int value)
	{
		setRadians(toRadians(value));
	}
	
	public void setDarwin(double value)
	{
		setRadians(toRadians(value));
	}
	
	public double getDarwin()
	{
		return toDarwin(radians);
	}
	
	public int getDarwinInt()
	{
		return (int) Math.round(getDarwin());
	}
}
