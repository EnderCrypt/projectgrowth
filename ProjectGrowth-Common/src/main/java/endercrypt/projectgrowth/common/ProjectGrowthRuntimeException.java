package endercrypt.projectgrowth.common;

@SuppressWarnings("serial")
public class ProjectGrowthRuntimeException extends RuntimeException
{
	public ProjectGrowthRuntimeException(String message)
	{
		super(message);
	}
	
	public ProjectGrowthRuntimeException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
