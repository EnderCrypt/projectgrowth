package endercrypt.projectgrowth.common.layerarray;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;


public class LayeredArray<T> implements Iterable<T>
{
	private final Set<T> elements = new HashSet<>();
	private final List<T> pending = new ArrayList<>();
	
	public void add(T element)
	{
		synchronized (pending)
		{
			pending.add(element);
		}
	}
	
	public synchronized void flush()
	{
		synchronized (pending)
		{
			elements.addAll(pending);
			pending.clear();
		}
	}
	
	public synchronized void removeIf(Predicate<T> condition)
	{
		elements.removeIf(condition);
		synchronized (pending)
		{
			pending.removeIf(condition);
		}
	}
	
	public int getSize()
	{
		return elements.size() + pending.size();
	}
	
	public boolean isEmpty()
	{
		return getSize() == 0;
	}
	
	public Stream<T> stream()
	{
		return elements.stream();
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return elements.iterator();
	}
	
	public synchronized void clear()
	{
		elements.clear();
		synchronized (pending)
		{
			pending.clear();
		}
	}
	
	public synchronized T[] toArray(IntFunction<T[]> generator)
	{
		return elements.toArray(generator);
	}
	
	@Override
	public String toString()
	{
		return LayeredArray.class.getSimpleName() + "[" + pending.size() + "] =" + pending + " Elements[" + elements.size() + "] =" + elements;
	}
}
