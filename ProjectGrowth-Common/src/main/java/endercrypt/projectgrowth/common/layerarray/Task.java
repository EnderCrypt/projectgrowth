package endercrypt.projectgrowth.common.layerarray;

public interface Task<T>
{
	public void perform(T element) throws Exception;
}
