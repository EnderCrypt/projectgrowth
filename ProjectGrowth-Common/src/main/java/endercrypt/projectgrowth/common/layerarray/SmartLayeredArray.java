package endercrypt.projectgrowth.common.layerarray;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.NonNull;


public class SmartLayeredArray<T>
{
	private final Class<T> headClass;
	private final Map<Class<? extends T>, LayeredArray<? extends T>> map = new HashMap<>();
	private final List<T> pending = new ArrayList<>();
	
	public SmartLayeredArray(@NonNull Class<T> headClass)
	{
		this.headClass = headClass;
	}
	
	// ADD
	
	@SuppressWarnings("unchecked")
	public void add(T object)
	{
		LayeredArray<T> array = (LayeredArray<T>) map.get(object.getClass());
		if (array != null)
		{
			array.add(object);
		}
		else
		{
			synchronized (pending)
			{
				pending.add(object);
			}
		}
	}
	
	// GET LAYER
	
	@SuppressWarnings("unchecked")
	private <R extends T> LayeredArray<R> getLayer(Class<R> targetClass)
	{
		synchronized (map)
		{
			return (LayeredArray<R>) map.computeIfAbsent(targetClass, (key) -> new LayeredArray<>());
		}
	}
	
	@SuppressWarnings("unchecked")
	public <R extends T> Stream<LayeredArray<R>> getHierarchalLayers(Class<R> targetClass)
	{
		return map.entrySet().stream()
			.filter(e -> targetClass.isAssignableFrom(e.getKey()))
			.map(e -> (LayeredArray<R>) e.getValue());
	}
	
	public Stream<LayeredArray<? extends T>> getAllHierarchalLayers()
	{
		return map.values().stream();
	}
	
	// STREAM
	
	public <R extends T> Stream<R> stream(Class<R> targetClass)
	{
		return getHierarchalLayers(targetClass)
			.flatMap(array -> (Stream<R>) array.stream());
	}
	
	public Stream<T> streamAll()
	{
		return stream(headClass);
	}
	
	public List<T> getAll()
	{
		synchronized (this)
		{
			return streamAll().toList();
		}
	}
	
	// ITERATE
	
	public <R extends T> Iterable<R> iterate(Class<R> targetClass)
	{
		return () -> stream(targetClass).iterator();
	}
	
	public Iterable<T> iterateAll()
	{
		return iterate(headClass);
	}
	
	// COUNT
	
	public <R extends T> int count(Class<R> targetClass)
	{
		synchronized (pending)
		{
			return (int) (getHierarchalLayers(targetClass)
				.mapToInt(LayeredArray::getSize).sum()
				+
				pending.stream()
					.filter(targetClass::isInstance)
					.count());
		}
	}
	
	public <R extends T> int count()
	{
		return count(headClass);
	}
	
	// SERIALIZABLE
	
	public synchronized List<Serializable> serializable()
	{
		return streamAll()
			.filter(Serializable.class::isInstance)
			.map(Serializable.class::cast)
			.collect(Collectors.toList());
	}
	
	// CLEAR
	
	public void clear()
	{
		clear(headClass);
	}
	
	public synchronized <R extends T> void clear(Class<R> targetClass)
	{
		getHierarchalLayers(targetClass).forEach(LayeredArray::clear);
	}
	
	// FLUSH
	
	@SuppressWarnings("unchecked")
	public synchronized void flush()
	{
		synchronized (pending)
		{
			for (T object : pending)
			{
				Class<T> targetClass = (Class<T>) object.getClass();
				getLayer(targetClass).add(object);
			}
			pending.clear();
		}
		
		getAllHierarchalLayers().forEach(LayeredArray::flush);
	}
	
	@Override
	public String toString()
	{
		return map
			.entrySet()
			.stream()
			.map(e -> e.getKey().getSimpleName() + " (" + e.getValue().getSize() + ")")
			.collect(Collectors.joining("\n"));
	}
}
