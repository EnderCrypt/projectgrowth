#!/usr/bin/env bash

set -e

cd "$(dirname $0)/.."

./tools/generate-build-information.sh
mvn $MAVEN_CLI_OPTS package

mkdir ProjectGrowth
function include {
	LOCATION="$2"
	if [[ -z "$LOCATION" ]]; then
		LOCATION="."
	fi
	DESTINATION="ProjectGrowth/$LOCATION"
	mv "$1" "$DESTINATION"
	echo "$DESTINATION/$(basename $1)"
}

include ./ProjectGrowth-Main/target/ProjectGrowth.jar
include ./ProjectGrowth-Main/settings
include ./ProjectGrowth-Main/bots
include ./tools/ProjectGrowth.sh
include ./docs
