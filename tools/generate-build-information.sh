#!/usr/bin/env bash

set -e

DIRECTORY_ROOT="./ProjectGrowth-Main"
DIRECTORY_BUILDINFO="$DIRECTORY_ROOT/src/main/resources/build"

function buildfile {
	mkdir -p "$DIRECTORY_BUILDINFO" || true
	echo "$DIRECTORY_BUILDINFO/$1"
}

function ifCommandExists {
	command -v "$1" || echo ""
}

function ensureCommand {
	NAME="$1"
	INSTALL="$2"

	COMMAND_PATH="$(ifCommandExists $NAME)"
	if [ -z "$COMMAND_PATH" ]; then
		echo "Missing command $NAME, installing ..."
		if [ ! -d "./bin" ]; then
			mkdir -p "./bin"
			export PATH="$PATH:./bin"
			echo "Created a bin directory for commands ..."
		fi
		$INSTALL
	fi
}

function installWget {
	microdnf install wget
}

function installCloc {
	ensureCommand wget installWget
	wget https://github.com/AlDanial/cloc/releases/download/v1.96/cloc-1.96.tar.gz 2> /dev/null
	tar -xzvf cloc-1.96.tar.gz cloc-1.96/cloc 2> /dev/null
	rm cloc-1.96.tar.gz
	mv cloc-1.96/cloc ./bin/cloc
	rmdir cloc-1.96
	chmod +x ./bin/cloc
}

# cloc
ensureCommand cloc installCloc
cloc --json "$DIRECTORY_ROOT/src/main/java" > `buildfile cloc.json`

# hash
echo "Running hash ..."
git rev-parse HEAD > `buildfile hash.txt`

# time
echo "Running time ..."
date -Iminutes > `buildfile time.txt`

# summary
echo "Generated:"
ls "$DIRECTORY_BUILDINFO"

echo "Done!"
