package endercrypt.projectgrowth.dna.mutation.factory;


import endercrypt.library.commons.RandomEntry;
import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.mutation.factory.creators.MemoryLocationCreator;
import endercrypt.projectgrowth.dna.mutation.factory.creators.MemoryValueCreator;
import endercrypt.projectgrowth.dna.mutation.factory.creators.NumberCreator;
import endercrypt.projectgrowth.dna.mutation.factory.creators.OperatorCreator;

import java.util.ArrayList;
import java.util.List;


public class DnaRandomizerFactory
{
	private List<DnaFactory> creators = new ArrayList<>();
	
	public DnaRandomizerFactory(OperatorCollection operatorCollection)
	{
		creators.add(new OperatorCreator(operatorCollection));
		creators.add(new MemoryLocationCreator());
		creators.add(new MemoryValueCreator());
		creators.add(new NumberCreator());
	}
	
	public Dna createDna()
	{
		DnaFactory operatorFactory = RandomEntry.from(creators);
		Dna operatator = operatorFactory.createDna();
		return operatator;
	}
}
