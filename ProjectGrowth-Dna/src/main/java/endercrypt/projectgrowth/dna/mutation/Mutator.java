package endercrypt.projectgrowth.dna.mutation;


import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.genome.Sequence;


public interface Mutator
{
	public boolean isEnabled();
	
	public Genome mutate(Genome genome);
	
	public Gene mutate(Gene gene);
	
	public Sequence mutate(Sequence sequence);
}
