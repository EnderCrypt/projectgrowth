package endercrypt.projectgrowth.dna.mutation.factory;


import endercrypt.projectgrowth.dna.abstraction.Dna;


public interface DnaFactory
{
	public Dna createDna();
}
