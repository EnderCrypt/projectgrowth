package endercrypt.projectgrowth.dna.mutation;


import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.genome.Sequence;
import endercrypt.projectgrowth.dna.genome.abstr.GeneticMaterial;

import java.util.function.UnaryOperator;


public abstract class AbstractMutator implements Mutator
{
	private <S extends GeneticMaterial<S, E>, E> S mutate(S genetics, UnaryOperator<S> operator)
	{
		S newGenetics = operator.apply(genetics);
		
		if (genetics.equals(newGenetics))
		{
			return genetics;
		}
		
		return newGenetics;
	}
	
	@Override
	public final Genome mutate(Genome genome)
	{
		return mutate(genome, this::performMutation);
	}
	
	protected abstract Genome performMutation(Genome genome);
	
	@Override
	public final Gene mutate(Gene gene)
	{
		return mutate(gene, this::performMutation);
	}
	
	protected Gene performMutation(Gene gene)
	{
		Sequence condition = mutate(gene.getCondition());
		Sequence body = mutate(gene.getBody());
		
		return new Gene(condition, body);
	}
	
	@Override
	public final Sequence mutate(Sequence sequence)
	{
		return mutate(sequence, this::performMutation);
	}
	
	protected abstract Sequence performMutation(Sequence sequence);
}
