package endercrypt.projectgrowth.dna.mutation.factory.creators;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.mutation.factory.DnaFactory;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class OperatorCreator implements DnaFactory
{
	private final OperatorCollection operatorCollection;
	
	@Override
	public Dna createDna()
	{
		return operatorCollection.createRandom();
	}
}
