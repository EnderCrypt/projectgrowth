package endercrypt.projectgrowth.dna.mutation.factory.creators;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.mutation.factory.DnaFactory;
import endercrypt.projectgrowth.dna.special.MemoryLocation;


public class MemoryValueCreator implements DnaFactory
{
	@Override
	public Dna createDna()
	{
		return MemoryLocation.random();
	}
}
