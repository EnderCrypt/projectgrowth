package endercrypt.projectgrowth.dna.mutation.factory.creators;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.mutation.factory.DnaFactory;
import endercrypt.projectgrowth.dna.special.RawNumber;


public class NumberCreator implements DnaFactory
{
	@Override
	public Dna createDna()
	{
		return RawNumber.random();
	}
}
