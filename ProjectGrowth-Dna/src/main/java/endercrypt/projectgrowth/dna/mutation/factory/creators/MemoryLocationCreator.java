package endercrypt.projectgrowth.dna.mutation.factory.creators;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.mutation.factory.DnaFactory;
import endercrypt.projectgrowth.dna.special.MemoryValue;


public class MemoryLocationCreator implements DnaFactory
{
	@Override
	public Dna createDna()
	{
		return MemoryValue.random();
	}
}
