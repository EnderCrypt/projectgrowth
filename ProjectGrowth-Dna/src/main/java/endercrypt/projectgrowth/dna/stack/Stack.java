package endercrypt.projectgrowth.dna.stack;


import java.util.ArrayDeque;
import java.util.Deque;

import lombok.NonNull;


public class Stack<T>
{
	private static final int LIMIT = 20;
	
	private final T defaultValue;
	private Deque<T> internalStack = new ArrayDeque<>();
	
	public Stack(@NonNull T defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	
	public void push(T value)
	{
		if (internalStack.size() == LIMIT)
		{
			internalStack.remove(0);
		}
		internalStack.push(value);
	}
	
	public T pop()
	{
		if (internalStack.isEmpty())
		{
			return defaultValue;
		}
		return internalStack.pop();
	}
	
	public T peek()
	{
		if (internalStack.isEmpty())
		{
			return defaultValue;
		}
		return internalStack.peek();
	}
	
	public void clear()
	{
		internalStack.clear();
	}
}
