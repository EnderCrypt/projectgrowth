package endercrypt.projectgrowth.dna.stack;


import endercrypt.library.commons.misc.Ender;


public class NumberStack extends Stack<Integer>
{
	public static final int MAX_VALUE = 2_000_000_000;
	
	public NumberStack()
	{
		super(0);
	}
	
	@Override
	public void push(Integer value)
	{
		value = (int) Ender.math.clamp(value, -MAX_VALUE, MAX_VALUE);
		super.push(value);
	}
}
