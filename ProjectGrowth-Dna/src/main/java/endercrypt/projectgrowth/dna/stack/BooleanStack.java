package endercrypt.projectgrowth.dna.stack;

public class BooleanStack extends Stack<Boolean>
{
	public BooleanStack()
	{
		super(true);
	}
}
