package endercrypt.projectgrowth.dna.operators.basic;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NamedDna;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NamedDna("*")
public class MemoryReadback extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int location = numberStack.pop();
		if (memory.isValidIndex(location))
		{
			int value = memory.get(location);
			numberStack.push(value);
		}
	}
}
