package endercrypt.projectgrowth.dna.operators.advanced;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Pyth extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int value1 = numberStack.pop();
		int value2 = numberStack.pop();
		value1 = value1 * value1;
		value2 = value2 * value2;
		int result = value1 + value2;
		result = (int) Math.sqrt(result);
		numberStack.push(result);
	}
}
