package endercrypt.projectgrowth.dna.operators.stack.bool;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class SwapBool extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		boolean value2 = booleanStack.pop();
		boolean value1 = booleanStack.pop();
		booleanStack.push(value1);
		booleanStack.push(value2);
	}
}
