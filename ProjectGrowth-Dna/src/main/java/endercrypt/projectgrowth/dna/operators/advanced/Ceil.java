package endercrypt.projectgrowth.dna.operators.advanced;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Ceil extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int max = numberStack.pop();
		int value = numberStack.pop();
		if (value > max)
		{
			value = max;
		}
		numberStack.push(value);
	}
}
