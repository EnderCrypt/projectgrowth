package endercrypt.projectgrowth.dna.operators.flow;


import endercrypt.projectgrowth.dna.management.ControlFlowOperator;
import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.management.RefuseMutatation;


@RefuseMutatation
@NewlineOperator
public class Cond extends ControlFlowOperator
{
	
}
