package endercrypt.projectgrowth.dna.operators.comparison;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NamedDna;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NamedDna("~=")
public class EqualsApprox extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int value2 = numberStack.pop();
		int value1 = numberStack.pop();
		int diff = Math.abs(value1 - value2);
		booleanStack.push(diff <= value1 / 10);
	}
}
