package endercrypt.projectgrowth.dna.operators.stack.integer;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Swap extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int value2 = numberStack.pop();
		int value1 = numberStack.pop();
		numberStack.push(value1);
		numberStack.push(value2);
	}
}
