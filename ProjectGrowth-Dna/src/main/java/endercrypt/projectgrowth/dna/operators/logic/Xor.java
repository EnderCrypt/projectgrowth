package endercrypt.projectgrowth.dna.operators.logic;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Xor extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		boolean value1 = booleanStack.pop();
		boolean value2 = booleanStack.pop();
		booleanStack.push(value1 ^ value2);
	}
}
