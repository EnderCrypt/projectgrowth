package endercrypt.projectgrowth.dna.operators.advanced;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Pow extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int power = numberStack.pop();
		int value = numberStack.pop();
		value = (int) Math.pow(value, power);
		numberStack.push(value);
	}
}
