package endercrypt.projectgrowth.dna.operators.stack.bool;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class DupBool extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		boolean value = booleanStack.pop();
		booleanStack.push(value);
		booleanStack.push(value);
	}
}
