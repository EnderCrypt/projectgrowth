package endercrypt.projectgrowth.dna.operators.store;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NewlineOperator
public class RndStore extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int location = numberStack.pop();
		int value = memory.get(location);
		int result = (int) Math.round(Math.random() * value);
		memory.set(location, result);
	}
}
