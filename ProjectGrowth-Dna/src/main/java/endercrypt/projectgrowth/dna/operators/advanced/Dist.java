package endercrypt.projectgrowth.dna.operators.advanced;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Dist extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int y = numberStack.pop();
		int x = numberStack.pop();
		x = (int) Math.pow(x, 2);
		y = (int) Math.pow(y, 2);
		int distance = (int) Math.sqrt(x + y);
		numberStack.push(distance);
	}
}
