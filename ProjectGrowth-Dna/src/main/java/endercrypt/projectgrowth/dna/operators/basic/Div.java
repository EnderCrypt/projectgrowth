package endercrypt.projectgrowth.dna.operators.basic;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;
import endercrypt.projectgrowth.dna.utility.DnaUtility;


public class Div extends Operator
{
	public static int perform(double dividend, double divisor)
	{
		return DnaUtility.darwinMaths(dividend, divisor, () -> dividend / divisor);
	}
	
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int divisor = numberStack.pop();
		int dividend = numberStack.pop();
		numberStack.push(Div.perform(dividend, divisor));
	}
}
