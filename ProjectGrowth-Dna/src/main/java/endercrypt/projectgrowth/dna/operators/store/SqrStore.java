package endercrypt.projectgrowth.dna.operators.store;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NewlineOperator
public class SqrStore extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int location = numberStack.pop();
		int value = memory.get(location);
		value = (int) Math.sqrt(value);
		memory.set(location, value);
	}
}
