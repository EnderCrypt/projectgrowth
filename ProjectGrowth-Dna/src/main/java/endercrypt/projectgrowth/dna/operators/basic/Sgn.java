package endercrypt.projectgrowth.dna.operators.basic;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Sgn extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int value = numberStack.pop();
		int result = 0;
		if (value > 0)
		{
			result = 1;
		}
		if (value < 0)
		{
			result = -1;
		}
		numberStack.push(result);
	}
}
