package endercrypt.projectgrowth.dna.operators.stack.debug;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.RefuseMutatation;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@RefuseMutatation
public class DebugInt extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		// TODO: replace with log write
		System.out.println(numberStack.peek());
	}
}
