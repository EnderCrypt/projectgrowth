package endercrypt.projectgrowth.dna.operators.store;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NewlineOperator
public class SgnStore extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int location = numberStack.pop();
		int value = memory.get(location);
		int result = 0;
		if (value > 0)
		{
			result = 1;
		}
		if (value < 0)
		{
			result = -1;
		}
		memory.set(location, result);
	}
}
