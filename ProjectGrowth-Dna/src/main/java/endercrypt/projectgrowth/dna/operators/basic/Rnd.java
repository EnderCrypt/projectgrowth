package endercrypt.projectgrowth.dna.operators.basic;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public class Rnd extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int max = numberStack.pop();
		int value = (int) Math.round(Math.random() * max);
		numberStack.push(value);
	}
}
