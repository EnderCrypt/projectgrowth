package endercrypt.projectgrowth.dna.operators.store;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


@NewlineOperator
public class CeilStore extends Operator
{
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int location = numberStack.pop();
		int max = numberStack.pop();
		int current = memory.get(location);
		if (current > max)
		{
			current = max;
		}
		memory.set(location, current);
	}
}
