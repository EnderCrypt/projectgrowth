package endercrypt.projectgrowth.dna.memory;


import endercrypt.projectgrowth.dna.sysvar.Sysvar;
import endercrypt.projectgrowth.dna.utility.DnaUtility;

import java.io.Serializable;


public class Memory implements Serializable
{
	private static final long serialVersionUID = 2120891766287589025L;
	
	/**
	 *
	 */
	
	public static int randomDarwinNumber()
	{
		double value = DnaUtility.weightedRandomNumber(2.0);
		value *= MAX_VALUE;
		if (Math.random() < 0.5)
		{
			value = -value;
		}
		return (int) Math.round(value);
	}
	
	public static final int MEMORY_LOCATIONS = 1000;
	public static final int MAX_VALUE = 30_000;
	public static final int DEFAULT_VALUE = 0;
	
	private int[] memory = new int[MEMORY_LOCATIONS];
	
	public Memory()
	{
		for (int i = 0; i < memory.length; i++)
		{
			memory[i] = DEFAULT_VALUE;
		}
	}
	
	public void set(Sysvar sysvar, int value)
	{
		set(sysvar.getAddress(), value);
	}
	
	public void set(int address, int value)
	{
		if (isValidIndex(address))
		{
			memory[address - 1] = value % MAX_VALUE;
		}
	}
	
	public int get(Sysvar sysvar)
	{
		return get(sysvar.getAddress());
	}
	
	public int get(int address)
	{
		if (isValidIndex(address))
		{
			return memory[address - 1];
		}
		return DEFAULT_VALUE;
	}
	
	public boolean isValidIndex(int location) // 1 to 1000
	{
		return ((location > 0) && (location <= MEMORY_LOCATIONS));
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 1000;)
		{
			sb.append(i + 1);
			for (int ii = 0; ii < 10; i++, ii++)
			{
				sb.append('\t');
				sb.append(memory[i]);
			}
			sb.append('\n');
		}
		return sb.toString();
	}
}
