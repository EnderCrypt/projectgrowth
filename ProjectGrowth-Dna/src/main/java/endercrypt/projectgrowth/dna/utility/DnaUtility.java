package endercrypt.projectgrowth.dna.utility;


import endercrypt.library.commons.reject.ThrowRejectDueTo;
import endercrypt.projectgrowth.dna.stack.NumberStack;

import java.util.function.DoubleSupplier;


public class DnaUtility
{
	private DnaUtility()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static double weightedRandomNumber(double weight)
	{
		double value = Math.random() * Math.pow(1.0, weight);
		return 1.0 - Math.pow(value, 1.0 / weight);
	}
	
	public static int darwinMaths(double dividend, double divisor, DoubleSupplier supplier)
	{
		if (divisor == 0)
		{
			// TODO: handle divide by zero like its done in darwinbots
			return (int) (NumberStack.MAX_VALUE * Math.signum(dividend));
		}
		
		return (int) Math.round(supplier.getAsDouble());
	}
}
