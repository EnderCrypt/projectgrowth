package endercrypt.projectgrowth.dna.sysvar;


import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;


@SuppressWarnings("serial")
public class UnknownSysvarException extends DarwinCompileException
{
	public UnknownSysvarException(String message)
	{
		super(message);
	}
	
	public UnknownSysvarException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
