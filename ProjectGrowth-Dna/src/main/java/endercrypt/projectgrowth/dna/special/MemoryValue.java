package endercrypt.projectgrowth.dna.special;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;

import java.util.Optional;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class MemoryValue extends Dna
{
	public static MemoryValue random()
	{
		return new MemoryValue(Sysvar.randomActiveSysvar());
	}
	
	public static Optional<MemoryValue> parse(String text)
	{
		if (text.startsWith("*.") == false)
		{
			return Optional.empty();
		}
		
		String name = text.substring(2);
		Sysvar sysvar = Sysvar.lookup(name);
		MemoryValue value = new MemoryValue(sysvar);
		return Optional.of(value);
	}
	
	@NonNull
	private final Sysvar sysvar;
	
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		int value = memory.get(getSysvar());
		numberStack.push(value);
	}
	
	@Override
	public String getName()
	{
		return "*." + sysvar.toString();
	}
}
