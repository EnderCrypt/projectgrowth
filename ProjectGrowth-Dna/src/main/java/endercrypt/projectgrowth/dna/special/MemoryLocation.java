package endercrypt.projectgrowth.dna.special;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;
import endercrypt.projectgrowth.dna.sysvar.Sysvar;

import java.util.Optional;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class MemoryLocation extends Dna
{
	public static MemoryLocation random()
	{
		return new MemoryLocation(Sysvar.randomActiveSysvar());
	}
	
	public static Optional<MemoryLocation> parse(String text)
	{
		if (text.startsWith(".") == false)
		{
			return Optional.empty();
		}
		
		String name = text.substring(1);
		Sysvar sysvar = Sysvar.lookup(name);
		MemoryLocation value = new MemoryLocation(sysvar);
		return Optional.of(value);
	}
	
	@NonNull
	private final Sysvar sysvar;
	
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		numberStack.push(getSysvar().getAddress());
	}
	
	@Override
	public String getName()
	{
		return "." + getSysvar().toString();
	}
}
