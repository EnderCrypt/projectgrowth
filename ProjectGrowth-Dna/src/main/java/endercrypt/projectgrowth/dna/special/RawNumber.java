package endercrypt.projectgrowth.dna.special;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;

import java.util.Optional;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class RawNumber extends Dna
{
	public static RawNumber random()
	{
		return new RawNumber(Memory.randomDarwinNumber());
	}
	
	public static Optional<RawNumber> parse(String text)
	{
		return Ender.parse.toInteger(text)
			.map(RawNumber::new);
	}
	
	public final int value;
	
	@Override
	public void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		numberStack.push(value);
	}
	
	@Override
	public String getName()
	{
		return Integer.toString(value);
	}
}
