package endercrypt.projectgrowth.dna.visual;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.genome.Sequence;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Getter;


@Getter
public class GenomeVisualization implements Serializable
{
	private static final long serialVersionUID = -1137688235134558137L;
	
	/**
	 * 
	 */
	
	private static final int INITAL = 1_000;
	private static final int PUSHING = 100;
	
	public static GenomeVisualization of(Genome genome)
	{
		List<Integer> hashes = new ArrayList<>();
		for (Gene gene : genome)
		{
			hashes.addAll(generateHashes(gene.getCondition()));
			hashes.addAll(generateHashes(gene.getBody()));
		}
		
		int start = (int) hashes.stream()
			.mapToInt(i -> i * INITAL)
			.average()
			.orElse(0);
		
		int sum = hashes.stream()
			.mapToInt(i -> i * PUSHING)
			.sum();
		
		int MAX = INITAL + PUSHING;
		int index = Math.floorMod((start - sum), MAX);
		double valueHsb = index / (double) MAX;
		
		int nameHash = genome.hashCode();
		
		return new GenomeVisualization(valueHsb, nameHash);
	}
	
	private static List<Integer> generateHashes(Sequence sequence)
	{
		return sequence.stream()
			.map(GenomeVisualization::generateHash)
			.toList();
	}
	
	private static int generateHash(Dna dna)
	{
		String name = dna.getName();
		int result = Objects.hash(name);
		result = Math.abs(result);
		result = result % 100;
		return result;
	}
	
	private final double value;
	private final Color skinColor;
	private final String name;
	
	private GenomeVisualization(double valueHsb, int nameHash)
	{
		this.value = valueHsb;
		this.skinColor = Color.getHSBColor((float) valueHsb, 1f, 1f);
		this.name = ScientificNamesFactory.getRandom(nameHash, 2);
	}
}
