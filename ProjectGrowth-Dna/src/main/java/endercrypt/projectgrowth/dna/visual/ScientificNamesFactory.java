package endercrypt.projectgrowth.dna.visual;


import endercrypt.library.commons.SneakyThrow;
import endercrypt.library.commons.data.DataSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;


@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ScientificNamesFactory
{
	private static Object loadLock = new Object();
	private static Throwable exception;
	
	private static List<String> names;
	
	static
	{
		Thread thread = new Thread(new ReaderThread());
		thread.start();
	}
	
	public static String getRandom(int seed, int words)
	{
		Random random = new Random(seed);
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < words; i++)
		{
			builder.append(getRandom(random));
			if (i != words)
			{
				builder.append(" ");
			}
		}
		return builder.toString();
	}
	
	public static String getRandom(Random random)
	{
		while (names == null)
		{
			if (exception != null)
			{
				SneakyThrow.perform(exception);
			}
			synchronized (loadLock)
			{
				try
				{
					loadLock.wait();
				}
				catch (InterruptedException e)
				{
					SneakyThrow.perform(e);
				}
			}
		}
		int index = random.nextInt(names.size());
		return names.get(index);
	}
	
	private static class ReaderThread implements Runnable
	{
		@Override
		public void run()
		{
			Path filename = Path.of("./names.txt");
			try
			{
				names = DataSource.RESOURCES.read(filename).splitNewline().list();
			}
			catch (IOException e)
			{
				exception = e;
				log.error("Failed to read " + filename, e);
			}
			synchronized (loadLock)
			{
				loadLock.notifyAll();
			}
		}
	}
}
