package endercrypt.projectgrowth.dna.compiler;

@SuppressWarnings("serial")
public class DarwinCompileException extends RuntimeException
{
	public DarwinCompileException(String message)
	{
		super(message);
	}

	public DarwinCompileException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
