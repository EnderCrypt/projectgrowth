package endercrypt.projectgrowth.dna.compiler.parse.lexer;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.processors.BodyLexerStateProcessor;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.processors.ConditionLexerStateProcessor;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.processors.ExitedLexerStateProcessor;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.processors.OutsideLexerStateProcessor;

import lombok.NonNull;


public enum LexerState implements LexerStateProcessor
{
	OUTSIDE(new OutsideLexerStateProcessor()),
	CONDITION(new ConditionLexerStateProcessor()),
	BODY(new BodyLexerStateProcessor()),
	EXITED(new ExitedLexerStateProcessor());
	
	private final LexerStateProcessor processor;
	
	private LexerState(@NonNull LexerStateProcessor processor)
	{
		this.processor = processor;
	}
	
	@Override
	public void process(Lexer lexer, Dna dna)
	{
		processor.process(lexer, dna);
	}
}
