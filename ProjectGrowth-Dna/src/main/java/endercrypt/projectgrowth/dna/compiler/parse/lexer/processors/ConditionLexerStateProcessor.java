package endercrypt.projectgrowth.dna.compiler.parse.lexer.processors;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.Lexer;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerState;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerStateProcessor;
import endercrypt.projectgrowth.dna.operators.flow.Cond;
import endercrypt.projectgrowth.dna.operators.flow.End;
import endercrypt.projectgrowth.dna.operators.flow.Start;
import endercrypt.projectgrowth.dna.operators.flow.Stop;


public class ConditionLexerStateProcessor implements LexerStateProcessor
{
	@Override
	public void process(Lexer lexer, Dna dna)
	{
		LexerStateProcessor.forbid(dna, Cond.class, Stop.class, End.class);
		
		if (dna instanceof Start)
		{
			lexer.setState(LexerState.BODY);
			return;
		}
		
		lexer.getCondition().add(dna);
	}
}
