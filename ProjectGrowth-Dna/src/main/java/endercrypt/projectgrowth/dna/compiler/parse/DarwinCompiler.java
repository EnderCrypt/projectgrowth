package endercrypt.projectgrowth.dna.compiler.parse;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.abstraction.OperatorCollection;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.Lexer;
import endercrypt.projectgrowth.dna.compiler.parse.tokenizer.Tokenizer;
import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Genome;
import endercrypt.projectgrowth.dna.special.MemoryLocation;
import endercrypt.projectgrowth.dna.special.MemoryValue;
import endercrypt.projectgrowth.dna.special.RawNumber;

import java.util.List;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class DarwinCompiler
{
	private final OperatorCollection operatorCollection;
	
	public Genome compile(String code)
	{
		Tokenizer tokenizer = new Tokenizer();
		tokenizer.feed(code);
		tokenizer.finish();
		List<Dna> dnas = tokenizer.stream()
			.map(this::compileDna)
			.toList();
		
		Lexer parser = new Lexer();
		parser.feed(dnas);
		parser.finish();
		List<Gene> genes = parser.getResult();
		
		return new Genome(genes);
	}
	
	private Dna compileDna(String dna)
	{
		MemoryValue memoryValue = MemoryValue.parse(dna).orElse(null);
		if (memoryValue != null)
		{
			return memoryValue;
		}
		
		MemoryLocation memoryLocation = MemoryLocation.parse(dna).orElse(null);
		if (memoryLocation != null)
		{
			return memoryLocation;
		}
		
		RawNumber rawNumber = RawNumber.parse(dna).orElse(null);
		if (rawNumber != null)
		{
			return rawNumber;
		}
		
		return operatorCollection.get(dna);
	}
}
