package endercrypt.projectgrowth.dna.compiler.parse.lexer;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.parse.DarwinParser;
import endercrypt.projectgrowth.dna.genome.Gene;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Getter;


@Getter
public class Lexer extends DarwinParser<Dna, Gene>
{
	private List<Dna> condition = new ArrayList<>();
	private List<Dna> body = new ArrayList<>();
	private LexerState state = LexerState.OUTSIDE;
	
	public void feed(Iterable<Dna> dnas)
	{
		dnas.forEach(this::feed);
	}
	
	@Override
	public void feed(Dna dna)
	{
		state.process(this, dna);
	}
	
	public void setState(LexerState state)
	{
		this.state = Objects.requireNonNull(state, "state");
	}
}
