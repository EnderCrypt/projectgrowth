package endercrypt.projectgrowth.dna.compiler.parse.lexer.processors;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.Lexer;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerState;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerStateProcessor;
import endercrypt.projectgrowth.dna.operators.flow.Cond;
import endercrypt.projectgrowth.dna.operators.flow.End;


public class OutsideLexerStateProcessor implements LexerStateProcessor
{
	@Override
	public void process(Lexer lexer, Dna dna)
	{
		if (dna instanceof Cond)
		{
			lexer.setState(LexerState.CONDITION);
			return;
		}
		if (dna instanceof End)
		{
			lexer.setState(LexerState.EXITED);
			return;
		}
		LexerStateProcessor.errorUnexpected(dna);
	}
}
