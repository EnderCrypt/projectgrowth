package endercrypt.projectgrowth.dna.compiler.parse.lexer;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;


public interface LexerStateProcessor
{
	@SafeVarargs
	public static void forbid(Dna operator, Class<? extends Dna>... forbidden)
	{
		for (Class<? extends Dna> forbid : forbidden)
		{
			if (forbid.isAssignableFrom(operator.getClass()))
			{
				errorUnexpected(operator);
			}
		}
	}

	public static void errorUnexpected(Dna dna)
	{
		throw new DarwinCompileException("unexpected token: " + dna.getName());
	}

	public void process(Lexer lexer, Dna dna);
}
