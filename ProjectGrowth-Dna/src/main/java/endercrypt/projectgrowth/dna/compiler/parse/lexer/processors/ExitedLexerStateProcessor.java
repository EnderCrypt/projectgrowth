package endercrypt.projectgrowth.dna.compiler.parse.lexer.processors;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.Lexer;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerStateProcessor;


public class ExitedLexerStateProcessor implements LexerStateProcessor
{
	@Override
	public void process(Lexer lexer, Dna dna)
	{
		throw new DarwinCompileException("expected no more operators after end");
	}
}
