package endercrypt.projectgrowth.dna.compiler.parse.tokenizer;


import endercrypt.projectgrowth.dna.compiler.parse.DarwinParser;


public class Tokenizer extends DarwinParser<Character, String>
{
	private final StringBuilder builder = new StringBuilder();
	private boolean comment = false;
	
	public void feed(String text)
	{
		for (char character : text.toCharArray())
		{
			feed(character);
		}
	}
	
	@Override
	public void feed(Character character)
	{
		if (comment)
		{
			if (character == '\n')
			{
				comment = false;
			}
			return;
		}
		if (character == '\n' || character == '\t' || character == ' ')
		{
			finish();
			return;
		}
		if (character == '\'')
		{
			comment = true;
			return;
		}
		builder.append(character);
	}
	
	@Override
	public void finish()
	{
		if (builder.length() > 0)
		{
			add(builder.toString());
			builder.setLength(0);
		}
	}
}
