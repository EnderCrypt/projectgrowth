package endercrypt.projectgrowth.dna.compiler.parse.lexer.processors;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.Lexer;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerState;
import endercrypt.projectgrowth.dna.compiler.parse.lexer.LexerStateProcessor;
import endercrypt.projectgrowth.dna.genome.Gene;
import endercrypt.projectgrowth.dna.genome.Sequence;
import endercrypt.projectgrowth.dna.operators.flow.Cond;
import endercrypt.projectgrowth.dna.operators.flow.End;
import endercrypt.projectgrowth.dna.operators.flow.Start;
import endercrypt.projectgrowth.dna.operators.flow.Stop;


public class BodyLexerStateProcessor implements LexerStateProcessor
{
	@Override
	public void process(Lexer lexer, Dna dna)
	{
		LexerStateProcessor.forbid(dna, Cond.class, Start.class, End.class);
		
		if (dna instanceof Stop)
		{
			Sequence condition = new Sequence(lexer.getCondition());
			lexer.getCondition().clear();
			
			Sequence body = new Sequence(lexer.getBody());
			lexer.getBody().clear();
			
			Gene gene = new Gene(condition, body);
			lexer.add(gene);
			lexer.setState(LexerState.OUTSIDE);
			return;
		}
		
		lexer.getBody().add(dna);
	}
}
