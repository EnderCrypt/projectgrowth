package endercrypt.projectgrowth.dna.compiler.parse;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;


public abstract class DarwinParser<F, R>
{
	private final List<R> result = new ArrayList<>();
	
	public abstract void feed(F value);
	
	public final void add(R value)
	{
		result.add(value);
	}
	
	public void finish()
	{
		// do nothing
	}
	
	public final List<R> getResult()
	{
		return Collections.unmodifiableList(result);
	}
	
	public final Stream<R> stream()
	{
		return getResult().stream();
	}
}
