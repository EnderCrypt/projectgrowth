package endercrypt.projectgrowth.dna.management;


import endercrypt.projectgrowth.dna.abstraction.Operator;
import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;


public abstract class ControlFlowOperator extends Operator
{
	@Override
	public final void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack)
	{
		throw new DarwinCompileException("cant execute: " + getName());
	}
}
