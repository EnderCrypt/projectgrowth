package endercrypt.projectgrowth.dna.abstraction;


import endercrypt.projectgrowth.dna.management.NamedDna;

import lombok.Getter;


@Getter
public abstract class Operator extends Dna
{
	private static final long serialVersionUID = -1474056445843107334L;
	
	/**
	 *
	 */
	
	private final String name;
	
	public Operator()
	{
		// name
		NamedDna namedDna = getClass().getAnnotation(NamedDna.class);
		if (namedDna != null)
		{
			name = namedDna.value();
			return;
		}
		
		name = getClass().getSimpleName().toLowerCase();
	}
}
