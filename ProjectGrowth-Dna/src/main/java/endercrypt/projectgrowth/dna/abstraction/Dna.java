package endercrypt.projectgrowth.dna.abstraction;


import endercrypt.projectgrowth.dna.management.NewlineOperator;
import endercrypt.projectgrowth.dna.management.RefuseMutatation;
import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import javassist.Modifier;
import lombok.Getter;


@Getter
public abstract class Dna implements Serializable
{
	private static final long serialVersionUID = 1896151797631698527L;
	
	/**
	 *
	 */
	
	public static <T extends Dna> Optional<T> instance(Class<T> dnaClass) throws ReflectiveOperationException
	{
		if (Modifier.isAbstract(dnaClass.getModifiers()))
		{
			return Optional.empty();
		}
		try
		{
			return Optional.of(dnaClass.getConstructor().newInstance());
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			throw new ReflectiveOperationException("failed to instantiate " + dnaClass.getSimpleName(), e);
		}
	}
	
	private final boolean newline;
	private final boolean mutatable;
	
	public Dna()
	{
		// newline
		newline = getClass().isAnnotationPresent(NewlineOperator.class);
		
		// mutate
		mutatable = getClass().isAnnotationPresent(RefuseMutatation.class) == false;
	}
	
	public abstract String getName();
	
	public abstract void execute(Memory memory, NumberStack numberStack, BooleanStack booleanStack);
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + getName().hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if (this == object)
			return true;
		
		if (object instanceof Dna == false)
			return false;
		Dna other = (Dna) object;
		
		boolean thisOperator = this instanceof Operator;
		boolean otherOperator = other instanceof Operator;
		if (thisOperator ^ otherOperator)
			return false;
		
		if (thisOperator && otherOperator)
		{
			return this.getClass().equals(other.getClass());
		}
		
		if (getName().equals(other.getName()) == false)
			return false;
		
		return true;
	}
}
