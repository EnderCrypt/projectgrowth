package endercrypt.projectgrowth.dna.abstraction;


import endercrypt.library.commons.RandomEntry;
import endercrypt.projectgrowth.common.modules.ProjectGrowthModuleInfo;
import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;


public class OperatorCollection
{
	private final Map<String, Operator> operators = new HashMap<>();
	private final List<Operator> activeOperators = new ArrayList<>();
	
	public OperatorCollection()
	{
		loadOperators(ProjectGrowthModuleInfo.dna.getPackageString() + ".operators");
	}
	
	private void loadOperators(String packageString)
	{
		try
		{
			Reflections reflections = new Reflections(packageString);
			Set<Class<? extends Operator>> operatorClasses = reflections.getSubTypesOf(Operator.class);
			if (operatorClasses.isEmpty())
			{
				throw new DarwinCompileException("Failed to find any operator classes in " + packageString);
			}
			for (Class<? extends Operator> operatorClass : operatorClasses)
			{
				Dna.instance(operatorClass)
					.ifPresent(this::register);
			}
		}
		catch (ReflectiveOperationException e)
		{
			throw new DarwinCompileException("Failed to reflectively find operators in " + packageString, e);
		}
	}
	
	private void register(Operator operator)
	{
		String key = operator.getName().toLowerCase();
		if (operators.containsKey(key))
		{
			throw new IllegalArgumentException("operator " + operator.getName() + " already exists");
		}
		operators.put(key, operator);
		if (operator.isMutatable())
		{
			activeOperators.add(operator);
		}
	}
	
	public Operator createRandom()
	{
		return RandomEntry.from(activeOperators);
	}
	
	public Operator get(String operatorName)
	{
		Operator operator = operators.get(operatorName.toLowerCase());
		if (operator == null)
		{
			throw new UnknownOperatorException(operatorName);
		}
		return operator;
	}
	
	public boolean exists(String operatorName)
	{
		return operators.containsKey(operatorName);
	}
}
