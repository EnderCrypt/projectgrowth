package endercrypt.projectgrowth.dna.abstraction;


import endercrypt.projectgrowth.dna.compiler.DarwinCompileException;


@SuppressWarnings("serial")
public class UnknownOperatorException extends DarwinCompileException
{
	public UnknownOperatorException(String message)
	{
		super(message);
	}
	
	public UnknownOperatorException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
