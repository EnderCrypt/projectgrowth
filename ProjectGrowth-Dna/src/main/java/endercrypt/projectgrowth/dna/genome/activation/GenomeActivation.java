package endercrypt.projectgrowth.dna.genome.activation;


import endercrypt.projectgrowth.dna.genome.Genome;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class GenomeActivation implements Activation
{
	public static GenomeActivation of(Genome genome, ActivationContext context)
	{
		List<GeneActivation> activations = genome.stream()
			.map(gene -> GeneActivation.of(gene, context))
			.toList();
		
		double energyCost = activations.stream()
			.mapToDouble(GeneActivation::getEnergyCost)
			.sum();
		
		return new GenomeActivation(activations, energyCost);
	}
	
	private final List<GeneActivation> genes;
	private final double energyCost;
}
