package endercrypt.projectgrowth.dna.genome;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.genome.abstr.GeneticMaterialCollection;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(callSuper = false)
public class Sequence extends GeneticMaterialCollection<Sequence, Dna>
{
	private static final long serialVersionUID = -889607562526577978L;
	
	/**
	 *
	 */
	
	private final List<Dna> dnas;
	
	public Sequence(List<Dna> dnas)
	{
		if (dnas.contains(null))
		{
			throw new IllegalArgumentException("dna cannot be null");
		}
		
		this.dnas = List.copyOf(dnas);
	}
	
	@Override
	public Stream<Dna> stream()
	{
		return dnas.stream();
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		
		Iterator<Dna> iterator = dnas.iterator();
		while (iterator.hasNext())
		{
			Dna dna = iterator.next();
			builder.append(dna.getName());
			if (iterator.hasNext())
			{
				String separator = dna.isNewline() ? "\n" : " ";
				builder.append(separator);
			}
		}
		
		return builder.toString();
	}
}
