package endercrypt.projectgrowth.dna.genome.activation;


import endercrypt.projectgrowth.dna.genome.Gene;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class GeneActivation implements Activation
{
	public static GeneActivation of(Gene gene, ActivationContext context)
	{
		double energyCost = 0.0;
		
		context.getBooleanStack().clear();
		SequenceActivation conditionSequenceActivation = SequenceActivation.of(gene.getCondition(), context);
		energyCost += conditionSequenceActivation.getEnergyCost();
		
		boolean activated = context.getBooleanStack().pop();
		if (activated)
		{
			context.getBooleanStack().clear();
			SequenceActivation bodySequenceActivation = SequenceActivation.of(gene.getBody(), context);
			energyCost += bodySequenceActivation.getEnergyCost();
		}
		
		return new GeneActivation(gene, energyCost, activated);
	}
	
	private final Gene gene;
	private final double energyCost;
	private final boolean activated;
}
