package endercrypt.projectgrowth.dna.genome.activation;


import java.io.Serializable;


public interface Activation extends Serializable
{
	public double getEnergyCost();
}
