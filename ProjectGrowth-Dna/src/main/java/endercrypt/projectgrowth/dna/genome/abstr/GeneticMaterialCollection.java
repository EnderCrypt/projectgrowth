package endercrypt.projectgrowth.dna.genome.abstr;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import lombok.EqualsAndHashCode;


@EqualsAndHashCode
public abstract class GeneticMaterialCollection<S extends GeneticMaterialCollection<S, E>, E> implements GeneticMaterial<S, E>, Iterable<E>
{
	private static final long serialVersionUID = -7318560079574426025L;
	
	/**
	 *
	 */
	
	public abstract Stream<E> stream();
	
	@Override
	public final Iterator<E> iterator()
	{
		return stream().iterator();
	}
	
	public List<E> asList()
	{
		return new ArrayList<>(stream().toList());
	}
}
