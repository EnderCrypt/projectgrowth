package endercrypt.projectgrowth.dna.genome;


import endercrypt.library.commons.misc.Ender;
import endercrypt.projectgrowth.dna.genome.abstr.GeneticMaterial;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(callSuper = false)
public class Gene implements GeneticMaterial<Gene, Sequence>
{
	private static final long serialVersionUID = 7388104842105518732L;
	
	/**
	 *
	 */
	
	private final Sequence condition;
	private final Sequence body;
	
	public Gene(Sequence condition, Sequence body)
	{
		this.condition = condition;
		this.body = body;
	}
	
	@Override
	public String toString()
	{
		String conditionText = condition.toString();
		conditionText = Ender.text.tab(conditionText)
			.replace("\t", "  ");
		
		String bodyText = body.toString();
		bodyText = Ender.text.tab(bodyText)
			.replace("\t", "  ");
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("cond\n");
		builder.append(conditionText);
		builder.append("\n");
		
		builder.append("start\n");
		builder.append(bodyText);
		builder.append("\n");
		
		builder.append("stop");
		return builder.toString();
	}
}
