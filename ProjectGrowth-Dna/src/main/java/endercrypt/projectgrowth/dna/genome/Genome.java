package endercrypt.projectgrowth.dna.genome;


import endercrypt.projectgrowth.dna.genome.abstr.GeneticMaterialCollection;
import endercrypt.projectgrowth.dna.visual.GenomeVisualization;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Genome extends GeneticMaterialCollection<Genome, Gene>
{
	private static final long serialVersionUID = -3974605989165751281L;
	
	/**
	 *
	 */
	
	@EqualsAndHashCode.Include
	private final List<Gene> genes;
	
	@Getter
	private final GenomeVisualization visualization;
	
	public Genome(List<Gene> genes)
	{
		this.genes = List.copyOf(genes);
		this.visualization = GenomeVisualization.of(this);
	}
	
	@Override
	public Stream<Gene> stream()
	{
		return genes.stream();
	}
	
	@Override
	public String toString()
	{
		return stream()
			.map(Gene::toString)
			.collect(Collectors.joining("\n\n"))
			+ "\n\n"
			+ "end";
	}
}
