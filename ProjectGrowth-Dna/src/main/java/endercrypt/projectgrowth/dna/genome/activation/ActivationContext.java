package endercrypt.projectgrowth.dna.genome.activation;


import endercrypt.projectgrowth.dna.memory.Memory;
import endercrypt.projectgrowth.dna.stack.BooleanStack;
import endercrypt.projectgrowth.dna.stack.NumberStack;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class ActivationContext
{
	private final Memory memory;
	private final NumberStack numberStack = new NumberStack();
	private final BooleanStack booleanStack = new BooleanStack();
}
