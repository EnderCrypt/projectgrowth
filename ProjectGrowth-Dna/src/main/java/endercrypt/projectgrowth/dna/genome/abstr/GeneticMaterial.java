package endercrypt.projectgrowth.dna.genome.abstr;


import java.io.Serializable;


public interface GeneticMaterial<S extends GeneticMaterial<S, E>, E> extends Serializable
{
}
