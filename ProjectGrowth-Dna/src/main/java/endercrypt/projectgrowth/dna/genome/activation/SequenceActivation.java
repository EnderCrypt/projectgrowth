package endercrypt.projectgrowth.dna.genome.activation;


import endercrypt.projectgrowth.dna.abstraction.Dna;
import endercrypt.projectgrowth.dna.genome.Sequence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class SequenceActivation implements Activation
{
	private static final double ENERGY_COST_PER_DNA = 0.0025;
	
	public static SequenceActivation of(Sequence sequence, ActivationContext context)
	{
		double energyCost = 0.0;
		for (Dna dna : sequence.getDnas())
		{
			dna.execute(context.getMemory(), context.getNumberStack(), context.getBooleanStack());
			energyCost += ENERGY_COST_PER_DNA;
		}
		
		return new SequenceActivation(sequence, energyCost);
	}
	
	private final Sequence sequence;
	private final double energyCost;
}
