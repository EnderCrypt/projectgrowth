# Project Growth
A powerful simulation of evolution, powered by executable dna and mutations.

## License
Licensed under **AGPLv3** - This means the software is free, like freedom not free beer!
You have the right to use this software however you want, look at how it works, share it with others, and make changes to improve it.
If you make changes, you must share them with everyone else too, this helps build a community where we all benefit from each other's improvements.

Check out the LICENSE file for more details.
For a simplified explanation of why GPL, checkout [A Quick Guide to GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html)

## Screenshots
![Screenshot of the gui](https://endercrypt.com/content/ProjectGrowth/screenshots/console_1.png)
<br>
*The terminal console interface, allowing you to headlessly control the simulation*
<br>
<br>
![Screenshot of the gui](https://endercrypt.com/content/ProjectGrowth/screenshots/gui_1.png)
<br>
*The black circles are food, the red circles are microbes, the color of them is determined by its dna*
<br>
<br>
![Screenshot of the gui](https://endercrypt.com/content/ProjectGrowth/screenshots/gui_2.png)
<br>
*Information about the red bots, listing information like energy, procedural name and its DNA*
<br>

### Getting started
Launch ProjectGrowth.jar from inside a terminal, it'll launch as a captive interface with a paused simulation.
here you can issue commands, probalbly you'll wanna load in some bots from the bots directory `spawn 10 bots/simple.dna`
this will read the bots/simple.dna file and spawn in 10 bots in simulation.
there are 2 ways to start the simulation, if you wanna keep going in headless mode (terminal only), run the command `start`
If you want to witness the simulation, run the command `gui` and a window will popup, here you can use WASD to control the camera. `space` lets you resume/pause at anytime, with `.` to step the simulation 1 tick

### Origins
Project Growth was inspired heavily by [Darwinbots 2](http://wiki.darwinbots.com/w/Main_Page) an application written for windows which basically invented this system, however it was written in VBA (Visual Basic for Applications) which made the application very non-portable, suffering from bad performance and basically locked to windows.

### Goal
My goal with Project Growth is to make a fairly feature complete implementation of Darwinbots, in java which would make it very portable, easily moddable and most importantly, strong multi-threaded and able to run as a headless background service on a server for a longer period of time, this is simply something darwinbots isnt designed for.

### How does it work?
ProjectGrowth is all about microbes, microbes are very similar lifeforms presented as circles (you can think of them almost as single-celled lifeforms), they have a brain with memory slots and dna. The dna is a very fault tolerant executable code which executes every tick of the simulation, this lets the microbe read and write to its brain, different values.
some parts of the brain has numbers representing sensory input, such as from its 9 eyes.
while other parts of the brain represent output, such as directional movement
By reading from say, the eye memory, doing some conditional logic and storing values in its movement part of the brain we can exhibit intelligent behaviour.

### What about evolution?
When the simulation starts, you'll usually start by spawning in bots with some very simple dna, the file `bots/simple.dna` is a good starting point. these bots are practically lacking intelligence, they move at random and always try eating infront of them, with some very simple logic for when they should reproduce. and just like real evolution, at the reproduce stage some of the childs dna MAY get mutated resulting in different behaviour. usually one would need to run this simulation for atleast a few hours if not days to see improved intelligence.

## Requirements
- GNU/Linux (should work on other systems, but untested)
- JRE/JDK 21
- Terminal emulator

## Features
- Terminal based interface, controlled by commands
- Optional GUI
- First class multithreading

## Compiling

Compiling is a relatively straightforward process.
Execute the following commands one by one from the root directory of the project

```bash
mvn install
cd ProjectGrowth-Main
mvn package
```
A final jar will be generated at `./ProjectGrowth-Main/target/ProjectGrowth.jar` (from the root)

> :memo: **Note:** If your maven installation is not configured to use java 21, it may result in the following error<br>
> <br>
> java.lang.NoSuchFieldError: Class com.sun.tools.javac.tree.JCTree$JCImport does not have member field 'com.sun.tools.javac.tree.JCTree qualid' -> [Help 1]

## Running

```bash
java -jar ProjectGrowth.jar
```
Currently, there are no options, however any command parameter used will be run as a simulation command
